﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClipDownLoadAndImdCreation
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Process[] processlist = Process.GetProcesses();
            Process thisProc = Process.GetCurrentProcess();
            string processName = thisProc.ProcessName;
            Boolean checkStatus = false;
            foreach (System.Diagnostics.Process proc in processlist)
            {
               if ((processName == proc.ProcessName) && (thisProc.Id != proc.Id))
                {
                    checkStatus = true;
                }

            }

            if (checkStatus == false)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
            else
            {
                Application.Exit(); 
            }


        }
    }
}
