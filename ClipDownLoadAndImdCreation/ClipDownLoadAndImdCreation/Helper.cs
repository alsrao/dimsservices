﻿using System;
using System.IO;
using System.Text;
using System.Configuration;
using System.Runtime.Serialization;
using System.Net.Mail;
using System.Net;
using System.Collections.Generic;

namespace ClipDownLoadAndImdCreation
{
    public class Helper
    {
        public static void AddtoLogFile(string message)
        {
            var filename = @"\Log_" + DateTime.Now.ToString("MM-dd-yyyy") + ".txt";


            var value = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Logs\\";


            if (!Directory.Exists(value))
            {
                Directory.CreateDirectory(value);
            }

            var filepath = value + filename;

            if (!File.Exists(filepath))
            {
                var writer = File.CreateText(filepath);
                writer.Close();
            }

            using (var writer = new StreamWriter(filepath, true))
            {
                var sb = new StringBuilder();
                sb.Append(Environment.NewLine + "================================================================");
                sb.Append(Environment.NewLine + message);
                sb.Append(Environment.NewLine + "================================================================");
                writer.WriteLine(sb.ToString());
            }

        }

        public static string GetSetting(string settingName)
        {
            return ConfigurationManager.AppSettings[settingName] ?? "";
        }

        public static void SendMail(string filePath, string subJect, string body)
        {


            // Instantiate a new instance of MailMessage
            var mMailMessage = new MailMessage { From = new MailAddress(GetSetting("FromMail")) };
            var toMailIds = GetSetting("toMailIds");
            var bcc = GetSetting("bcc");
            var cc = GetSetting("cc");

            if (toMailIds.Contains(","))
            {
                foreach (var mailid in toMailIds.Split(','))
                {
                    mMailMessage.To.Add(new MailAddress(mailid));
                }

            }
            else
            {
                mMailMessage.To.Add(new MailAddress(toMailIds));
            }



            // Check if the bcc value is null or an empty string
            if (!string.IsNullOrEmpty(bcc))
            {
                // Set the Bcc address of the mail message
                mMailMessage.Bcc.Add(new MailAddress(bcc));
            }      // Check if the cc value is null or an empty value
            if (!string.IsNullOrEmpty(cc))
            {
                // Set the CC address of the mail message
                // mMailMessage.CC.Add(new MailAddress(cc));
                if (cc.Contains(","))
                {
                    foreach (var mailid in cc.Split(','))
                    {
                        mMailMessage.CC.Add(new MailAddress(mailid));
                    }

                }
                else
                {
                    mMailMessage.CC.Add(new MailAddress(cc));
                }
            } 
            
            // Set the subject of the mail message
            mMailMessage.Subject = subJect;
            // Set the body of the mail message
            mMailMessage.Body = body + "<br/><br/>" + filePath;

            // Set the format of the mail message body as HTML
            mMailMessage.IsBodyHtml = true;

            // Set the priority of the mail message to normal
            mMailMessage.Priority = MailPriority.Normal;

            // Instantiate a new instance of SmtpClient
            var mSmtpClient = new SmtpClient
            {
                Host = GetSetting("Host"),
                Port = Convert.ToInt32(GetSetting("Port"))
            };

            var frommail = Convert.ToString(GetSetting("FromMail"));
            var mailkey = Convert.ToString(GetSetting("MailKey"));
            mSmtpClient.UseDefaultCredentials = false;
            mSmtpClient.EnableSsl = Convert.ToBoolean(GetSetting("ssl"));
            mSmtpClient.Credentials = new NetworkCredential(frommail, mailkey);

            mSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            // Send the mail message
            try
            {
                mSmtpClient.Send(mMailMessage);
            }
            catch (Exception ex)
            {
                AddtoLogFile("Send Mail Error : " + ex.Message);
            }
        }


    }

    public class ClsClipStatus
    {
        [DataMember]
        public string MakeRequestId { get; set; }

        [DataMember]
        public string MediaFileStatus { get; set; }

        [DataMember]
        public string MediaFilePath { get; set; }

    }


    public class ClsLstCameraTrackAndDateTime
    {
        public List<CameraTrackAndDateTime> LstCameraTrackAndDateTime { get; set; }

    }

    public class CameraTrackAndDateTime
    {
        public string TrackId { get; set; }
        public DateTime StartDateTime { get; set; }

        public DateTime ENdDateTime { get; set; }


    }
}
