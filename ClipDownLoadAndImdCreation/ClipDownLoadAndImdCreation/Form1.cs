﻿using Insight.Shared.MetaContainer;
using System;
using System.IO;
using System.Windows.Forms;
using ClipDownLoadAndImdCreation.Model;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Bosch.VideoSDK.Device;
using Bosch.VideoSDK.MediaDatabase;
using System.Collections.Specialized;
using System.Threading;
using Newtonsoft.Json;
using System.Text;
using DVTel.API.Entities.Physical.Enums;
using DIMS.DVTelLibrary;
using DVTel.API.Entities.SystemObjects;
using DVTel.API;
using DVTel.API.Common;
using DVTel.SDK.Axiom.Export;
using DVTel.API.Entities.Physical;
using DVTel.SDK.Axiom.Base.Query;
using System.Net;

namespace ClipDownLoadAndImdCreation
{
    public partial class Form1 : Form
    {
        private readonly DBData _dbData = new DBData();
        AMSProcess amsProcess = new AMSProcess();
        private Bosch.VideoSDK.Device.DeviceConnectorClass deviceConnector = null;
        private Bosch.VideoSDK.Device.DeviceProxy proxy;

        private BoschMediaDatabaseBrowser mediaDb;
        private int tracksCompletelyLoaded;
        private Queue<int> trackToBeLoaded = new Queue<int>();

        public Dictionary<string, string> d_TrackList = new Dictionary<string, string>();
        public List<string> d_RecordList = new List<string>();
        public List<Bosch.VideoSDK.MediaDatabase.Track> m_tracks = null;
        private Bosch.VideoSDK.MediaDatabase.Track m_currentTrack = null;
        private Bosch.VideoSDK.MediaSession m_currentMediaSession = null;
        private Bosch.VideoSDK.MediaDatabase.PlaybackController m_currentPlaybackController = null;

        private Bosch.VideoSDK.MediaDatabase.MediaFileWriterClass m_mediaFileWriter = null;

        private ProgressBar m_progressBarRecordMediaFile = new ProgressBar();

        private VideoMode m_vmRecordVideoMode = VideoMode.vmPlayback;
        private int m_nRecordFileSizeKB = 0;
        private int m_mediaFileVideoTrackID = 0;
        private int m_mediaFileAudioTrackID = 0;

        string fileName, extn, localClipPathTemp1,tempWorkOrderId;
        int boschRecordIndex = 0;
        int boschRecordLength = 0;

        public enum ConnectionState
        {
            Disconnected,
            Connecting,
            Connected,
            Disconnecting,
            Failed
        };

        public enum VideoMode
        {
            vmLive = 0,
            vmPlayback = 1
        };

        /// <summary>
        /// Current connection state
        /// </summary>
        public ConnectionState State { get; set; }
        string reqStTime;
        string reqEnTime;

        DBData _clipData = new DBData();
        public static string resultId = null;
       
       
        #region MediaDb event handlers
        private void mediaDb_OnProgress(int progress)
        {
            float totalProgress = 0;
            if (mediaDb.State == MediaDatabaseBrowserState.TracksLoading || mediaDb.State == MediaDatabaseBrowserState.TracksLoaded)
            {
                // estimate loading tracks as 10%
                totalProgress = progress / 10;
            }
            else
            {
                // "divide" rest of progress bar (90%) between all tracks
                int tracksCount = mediaDb.Tracks.Count;

                if (tracksCount > 0)
                    totalProgress = 10 + 0.9f * (progress + tracksCompletelyLoaded * 100) / tracksCount;
                else
                    totalProgress = progress;
            }
            if ((int)totalProgress == 100)
            {
                tracksCompletelyLoaded = 0;
                d_TrackList = new Dictionary<string, string>();
                processBoschDownload();
            }
            Trace.TraceInformation("Total progress: {0}%", (int)totalProgress);
        }

        private void mediaDb_OnStateChanged(MediaDatabaseBrowserState state, string description)
        {

            if (state == MediaDatabaseBrowserState.TracksLoaded)
            {
                // prepare list of tracks for loading records
                trackToBeLoaded.Clear();
                foreach (var track in mediaDb.Tracks)
                {
                    trackToBeLoaded.Enqueue(track.TrackID);
                }
                // start load first
                LoadNextTrackRecords();
            }

            if (state == MediaDatabaseBrowserState.RecordsLoaded)
            {
                tracksCompletelyLoaded++;
                LoadNextTrackRecords();
            }
        }
        private void Tracks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                // add track nodes
                foreach (Bosch.VideoSDK.MediaDatabase.Track track in e.NewItems)
                {
                    string nodeKey = track.TrackID.ToString();
                    string displayName = string.Format("[Track #{0}] {1}", track.TrackID, track.Name);
                    if (!d_TrackList.ContainsKey(TrackToString(track)))
                    {
                        m_tracks.Add(track);
                        d_TrackList.Add(TrackToString(track), track.TrackID.ToString());
                    }
                }
            }
        }
        public string TrackToString(Bosch.VideoSDK.MediaDatabase.Track track)
        {
            if (track == null)
                return "";
            else
                return track.Name.Trim().ToUpper();
        }

        public ClsLstCameraTrackAndDateTime clsLstCameraTrackAndDateTime = new ClsLstCameraTrackAndDateTime
        {
            LstCameraTrackAndDateTime = new List<CameraTrackAndDateTime>()
        };
        private void Records_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                // add track records nodes
                foreach (Bosch.VideoSDK.MediaDatabase.SearchResult record in e.NewItems)
                {
                    var cameraTrackAndDateTime = new CameraTrackAndDateTime();
                    string parentNodeKey = record.TrackID.ToString();
                    string nodeKey = string.Format("{0}\\{1}", parentNodeKey, record.StartTime.UTC.Ticks);
                    string displayName = "[" + parentNodeKey + "]Record . " + record.StartTime.UTC + " - " + record.EndTime.UTC + " - " + record.Text;
                    d_RecordList.Add(displayName);
                    clsLstCameraTrackAndDateTime.LstCameraTrackAndDateTime.Add(
                        new CameraTrackAndDateTime { TrackId = parentNodeKey, StartDateTime = record.StartTime.UTC, ENdDateTime = record.EndTime.UTC });

                }
            }
        }
        #endregion

        #region Methods
        private void LoadMediaDatabase()
        {
            if (proxy.MediaDatabase == null)
            {
                Trace.TraceWarning("No media database found.");
                return;
            }
            // start loading 
            mediaDb.SearchTracks(proxy.MediaDatabase);//,FillTime(Convert.ToDateTime(reqStTime)), FillTime(Convert.ToDateTime(reqEnTime)));
        }
        private void LoadNextTrackRecords()
        {
            // Start loading records for found tracks from queue
            if (trackToBeLoaded.Count > 0)
            {
                int trackId = trackToBeLoaded.Dequeue();
                mediaDb.SearchTrackRecords(trackId);
            }
            else
            {
                // all tracks loaded
            }
        }
        #endregion

        public string Start_MediaFile_Writer(Track tracktemp, string stime, string etime, string workOrderId, string division)
        {
            try
            {
                m_currentTrack = tracktemp;
                m_currentMediaSession = m_currentTrack.GetMediaSession(m_currentPlaybackController);
                m_mediaFileWriter.NewFileCreated += new Bosch.VideoSDK.GCALib._IMediaFileWriterEvents_NewFileCreatedEventHandler(m_mediaFileWriter_NewFileCreated);
                m_mediaFileWriter.RecordingStopped += new Bosch.VideoSDK.GCALib._IMediaFileWriterEvents_RecordingStoppedEventHandler(m_mediaFileWriter_RecordingStopped);

                localClipPathTemp1 = @"\\MTAAXIOMS01\Users\Public\ClipDatabase\" + workOrderId + "\\";
                fileName = Path.Combine(localClipPathTemp1, workOrderId + "-" + Path.GetFileName(tracktemp.Name)) + ".mp4";
                Helper.AddtoLogFile(DateTime.Now + " :: " +(new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  File path : " + fileName);
                // make media file recording progress bar enabled
                m_vmRecordVideoMode = VideoMode.vmPlayback;
                m_mediaFileWriter.FileFormat = Bosch.VideoSDK.MediaDatabase.MediaFileFormatEnum.mffISOMP4;
                
                // Maximum size of media file(s)
                m_mediaFileWriter.FileSizeLimitKB = 10000000;
                m_nRecordFileSizeKB = m_mediaFileWriter.FileSizeLimitKB * 1;

                m_mediaFileWriter.FileSizeLimitKB = m_nRecordFileSizeKB;

                m_mediaFileWriter.RecordingStartTime = FillTime(Convert.ToDateTime(stime));// "2017-10-27T11:35:00"));
                m_mediaFileWriter.RecordingEndTime = FillTime(Convert.ToDateTime(etime));//"2017-10-27T11:45:00"));
                
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  AddStreamToMediaWriter:: " + m_mediaFileWriter.FileSizeLimitKB+" "+ m_mediaFileWriter.FileFormat+" "+ m_mediaFileWriter.MaximumNumberOfFiles);
                AddStreamToMediaWriter(Bosch.VideoSDK.MediaTypeEnum.mteVideo, m_currentMediaSession.GetVideoStream(), "Test");
                m_mediaFileWriter.StartRecording(fileName, "This file is created on " + DateTime.Now);
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " : " + ex.Message);
                return ex.Message;
            }
        }

        private Bosch.VideoSDK.MediaDatabase.Time64 FillTime(DateTime datetime)
        {
            Bosch.VideoSDK.MediaDatabase.Time64 t64 = new Bosch.VideoSDK.MediaDatabase.Time64();
            DateTime dt = new DateTime(
                datetime.Year,
                datetime.Month,
                datetime.Day,
                datetime.Hour,
                datetime.Minute,
                datetime.Second);

            t64.UTC = dt;

            return t64;
        }
        public void AddStreamToMediaWriter(
           Bosch.VideoSDK.MediaTypeEnum mediaType,
           Bosch.VideoSDK.DataStream stream,
           string url)
        {
            try
            {
                string trackName = "";
                int trackID = 1;

                if (mediaType == Bosch.VideoSDK.MediaTypeEnum.mteVideo)
                    trackID = ++m_mediaFileVideoTrackID;
                else
                    trackID = ++m_mediaFileAudioTrackID;

                trackName = "Sample Track " + trackID.ToString();

                m_mediaFileWriter.AddStream(
                    stream,
                    mediaType,
                    trackID,
                    trackName,
                    url,
                    0);


            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " : " + ex.Message);
            }
        }

        // This delegate enables asynchronous calls for updating
        // a messages ListBox control.
        delegate void RecordingStoppedThreadSafe(Bosch.VideoSDK.MediaDatabase.RecordingStoppedEnum Reason);

        public System.Windows.Forms.ListBox m_lbMessages = new System.Windows.Forms.ListBox();
        private void m_mediaFileWriter_RecordingStopped(Bosch.VideoSDK.MediaDatabase.RecordingStoppedEnum Reason)
        {
            try
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  m_mediaFileWriter_RecordingStopped event raised... and reason : " + Reason.ToString());
                // InvokeRequired required compares the thread ID of the
                // calling thread to the thread ID of the creating thread.
                // If these threads are different, it returns true.
                if (m_lbMessages.InvokeRequired)
                {
                    RecordingStoppedThreadSafe wrapper = new RecordingStoppedThreadSafe(m_mediaFileWriter_RecordingStopped);
                    System.Windows.Forms.Control c = new System.Windows.Forms.Control();
                    c.Invoke(wrapper, new object[] { Reason });
                }
                else
                {
                    string msg = "Media file recording stopped, Reason: " + Reason.ToString();

                    ///LogMessage("MainForm", "m_mediaFileWriter_RecordingStopped", msg);

                    m_lbMessages.Items.Add(DateTime.Now.ToString() + " " + msg);
                    if (m_lbMessages.Items.Count > 100)
                        m_lbMessages.Items.RemoveAt(0);
                    m_lbMessages.SelectedIndex = m_lbMessages.Items.Count - 1;

                    m_mediaFileVideoTrackID = 0;
                    m_mediaFileAudioTrackID = 0;
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  File recording stopped...");
                    Helper.AddtoLogFile("=============================================================================");

                    var response = BoschImdFileCreation(fileName, imdDestinationPathTemp1, tagsDictionary);
                    if (response == "SUCCESS")
                    {
                        string clipOutputFilePath = @"\\MTAAXIOMS01\ClipDatabase\" + boschClipData[boschRecordIndex].AxiomClipId + "\\";
                        var UpDateClipStatus1 = _dbData.UpDateClipStatus(boschClipData[boschRecordIndex].MakeRequestId, "4", @"\\MTAAXIOMS01\Users\Public\ClipDatabase\" + boschClipData[boschRecordIndex].AxiomClipId + "\\", clipOutputFilePath);
                        boschRecordIndex++;
                        if (boschRecordIndex < boschRecordLength)
                            BoschConnection(boschRecordIndex);
                        else
                            amsProcess.processAMSRecords();
                    }
                    else
                    {
                        var UpDateClipStatus = _dbData.UpDateClipStatus(boschClipData[boschRecordIndex].MakeRequestId, "3", null + boschClipData[boschRecordIndex].AxiomClipId + "\\", null);
                        boschRecordIndex++;
                        if (boschRecordIndex < boschRecordLength)
                            BoschConnection(boschRecordIndex);
                        else
                            amsProcess.processAMSRecords();
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " : " + ex.Message);
            }
        }

        // This delegate enables asynchronous calls for updating
        // a messages ListBox control.
        delegate void UpdateMessagesThreadSafe(string strFilename);

        private void m_mediaFileWriter_NewFileCreated(string strFilename)
        {
            try
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  m_mediaFileWriter_NewFileCreated event raised...");
                m_mediaFileWriter.Progress += new Bosch.VideoSDK.GCALib._IMediaFileWriterEvents_ProgressEventHandler(OnMFWProgress);
                // InvokeRequired required compares the thread ID of the
                // calling thread to the thread ID of the creating thread.
                // If these threads are different, it returns true.
                if (m_lbMessages.InvokeRequired)
                {
                    System.Windows.Forms.Control c = new System.Windows.Forms.Control();
                    UpdateMessagesThreadSafe wrapper = new UpdateMessagesThreadSafe(m_mediaFileWriter_NewFileCreated);
                    c.Invoke(wrapper, new object[] { strFilename });
                }
                else
                {
                    string msg = "New media file has been created: " + strFilename.ToString();

                    ///LogMessage("MainForm", "m_mediaFileWriter_NewFileCreated", msg);

                    m_lbMessages.Items.Add(DateTime.Now.ToString() + " " + msg);
                    if (m_lbMessages.Items.Count > 100)
                        m_lbMessages.Items.RemoveAt(0);
                    m_lbMessages.SelectedIndex = m_lbMessages.Items.Count - 1;
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  File creation started...");
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " : " + ex.Message);
            }
        }

        // This delegate enables asynchronous calls for setting
        // the text property on a Button control.
        delegate void SetProgressValueThreadSafe(int nTotalSec, int nElapsedSec, int KBWritten, Bosch.VideoSDK.MediaDatabase.Time64 tCurrentRecTime, int nErrorCount);

        private void OnMFWProgress(int nTotalSec, int nElapsedSec, int KBWritten, Bosch.VideoSDK.MediaDatabase.Time64 tCurrentRecTime, int nErrorCount)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (m_progressBarRecordMediaFile.InvokeRequired)
            {
                SetProgressValueThreadSafe wrapper = new SetProgressValueThreadSafe(OnMFWProgress);
                Invoke(wrapper, new object[] { nTotalSec, nElapsedSec, KBWritten, tCurrentRecTime, nErrorCount });
            }
            else
            {
                // select maximum value from the file size/recording interval
                double fFileSizePersantage = (double)KBWritten / m_nRecordFileSizeKB;
                double fRecdTimePersantage = 0.0;

                TimeSpan tMaximum = (m_mediaFileWriter.RecordingEndTime.UTC - m_mediaFileWriter.RecordingStartTime.UTC);
                TimeSpan tInterval = tCurrentRecTime.UTC - m_mediaFileWriter.RecordingStartTime.UTC;

                if (tInterval.TotalSeconds >= 0.0)
                {
                    fRecdTimePersantage = tInterval.TotalSeconds / tMaximum.TotalSeconds;
                    int percentage = (int)(11 + 0.9f * fRecdTimePersantage * 100);
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  fRecdTimePersantage " + percentage + " %");
                    if (percentage == 10)
                        _clipData.UpDatePercentage(tempWorkOrderId, percentage, 5);
                    else if (percentage == 25)
                        _clipData.UpDatePercentage(tempWorkOrderId, percentage, 5);
                    else if (percentage == 50)
                        _clipData.UpDatePercentage(tempWorkOrderId, percentage, 5);
                    else if (percentage == 75)
                        _clipData.UpDatePercentage(tempWorkOrderId, percentage, 5);
                    else if (percentage == 97)
                        _clipData.UpDatePercentage(tempWorkOrderId, percentage, 5);
                }

                if (fFileSizePersantage > fRecdTimePersantage)
                {
                    m_progressBarRecordMediaFile.Maximum = m_nRecordFileSizeKB;
                    m_progressBarRecordMediaFile.Value = KBWritten;                    
                }
                else
                {
                    m_progressBarRecordMediaFile.Maximum = (int)tMaximum.TotalSeconds;
                    m_progressBarRecordMediaFile.Value = (int)((tInterval.TotalSeconds < 0) ? 0 : tInterval.TotalSeconds);
                }
            }
        }

        public string ValidateTrack(string cameraName, string encoderName)
        {
            try
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  cameraName :: " + cameraName + " and encoderName :: " + encoderName + " trackObj :: " + d_TrackList.Count);
                string requestAttrEC = encoderName.Trim() + "/" + cameraName.Trim();
                string vrmTrackAttr = d_TrackList[requestAttrEC.ToUpper()];
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  TrackID :: " + vrmTrackAttr);
                return vrmTrackAttr;
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " : " + ex.Message);
                return null;
            }
        }

        public CameraTrackAndDateTime ValidateRecordDateTime(string trackId, DateTime clipstartDate, DateTime clipEndDate)
        {
            try
            {
                //Please verify this 
                Time64 st = FillTime(clipstartDate);
                Time64 et = FillTime(clipEndDate);

                Thread.Sleep(3000);
                var data = (from x in clsLstCameraTrackAndDateTime.LstCameraTrackAndDateTime
                            where x.TrackId == trackId && st.UTC >= x.StartDateTime && st.UTC <= x.ENdDateTime
                            && et.UTC > x.StartDateTime && et.UTC <= x.ENdDateTime
                            select x).FirstOrDefault();
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Record Date and time with given time range. StartDateTime :: " + data.StartDateTime + " ENdDateTime :: " + data.ENdDateTime);
                return data;
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " : " + ex.Message);
                return null;
            }
        }


        public Track GetTrackData(string trackId)
        {
            Track data = (from x in m_tracks where x.TrackID == Convert.ToInt32(trackId) select x).FirstOrDefault();
            return data;
        }

        
        public enum Status { AVAILABLE = 1, PENDING = 2, FAILED = 3 };
        public Form1()
        {
            InitializeComponent();
        }

        private void OnConnectResult(ConnectResultEnum connectresult, string url, DeviceProxy deviceProxy)
        {
            proxy = deviceProxy;

            switch (connectresult)
            {
                case ConnectResultEnum.creConnected:
                    Console.WriteLine("-------");
                    break;

                case ConnectResultEnum.creInitialized:
                    // The connection was successfully established.
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Bosch Connection Successfully established...");
                    LoadMediaDatabase();
                  
                    break;
                case ConnectResultEnum.creConnectionFailure:
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Bosch connection failure...");
                    boschRecordIndex++;
                    if (boschRecordIndex < boschRecordLength)
                        BoschConnection(boschRecordIndex);
                    else
                        amsProcess.processAMSRecords();
                    break;
                case ConnectResultEnum.creInvalidCredentials:
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Bosch InvalidCredentials...");
                    if (boschRecordIndex < boschRecordLength)
                        BoschConnection(boschRecordIndex);
                    else
                        amsProcess.processAMSRecords();
                    break;
                default:
                    proxy.Disconnect();
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Bosch proxy connectionState : " + proxy.ConnectionState);
                    boschRecordIndex++;
                    if (boschRecordIndex < boschRecordLength)
                        BoschConnection(boschRecordIndex);
                    else
                        amsProcess.processAMSRecords();
                    break;
            }
        }


        public void processBoschDownload()
        {
            try
            {
                if (boschRecordIndex < boschRecordLength)
                {
                    var boschObj = boschClipData[boschRecordIndex];
                    var trackId = boschObj.ClipRequestId;
                    tempWorkOrderId = boschObj.AxiomClipId;
                    if (trackId != null)
                    {
                        Track trackData = (from x in m_tracks where x.TrackID == Convert.ToInt32(trackId) select x).FirstOrDefault();// boschVRMProcess.GetTrackData(trackId.ToString());
                        if (trackData != null)
                        {
                            var response = Start_MediaFile_Writer(trackData, boschObj.StartTime, boschObj.EndTime, boschObj.AxiomClipId, boschObj.Division);
                            if (response == "SUCCESS")
                            {
                                axiomClipId = boschObj.AxiomClipId;
                                var returnData = BoschDownLoadAndImdCreation(boschObj, trackData.Name);

                                if (returnData != "Success")
                                {
                                    var UpDateClipStatus = _dbData.UpDateClipStatus(boschObj.MakeRequestId, "3", null + boschObj.AxiomClipId + "\\", null);
                                }
                            }
                            else
                            {
                                boschRecordIndex++;
                                if (boschRecordIndex < boschRecordLength)
                                    BoschConnection(boschRecordIndex);
                                else
                                    amsProcess.processAMSRecords();
                            }
                        }
                        else
                        {
                            boschRecordIndex++;
                            if (boschRecordIndex < boschRecordLength)
                                BoschConnection(boschRecordIndex);
                            else
                                amsProcess.processAMSRecords();
                        }
                    }
                }
                else
                {
                    amsProcess.processAMSRecords();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " : " + ex.Message);
            }
        }

        public void BoschConnection(int index)
        {
            try
            {
                int timeDifference = (int)(DateTime.Now - boschClipData[index].CreatedOn).TotalHours;
                if ((boschClipData[index].ClipStatus == 2 || boschClipData[index].ClipStatus == 3) && timeDifference > 48)
                {
                    String camEncccoder = null;
                    camEncccoder = boschClipData[index].BusNumber;
                    if(camEncccoder != null && camEncccoder.Contains("/"))
                    {
                        String[] camEcncod = camEncccoder.Split('/');
                        if (camEcncod.Length > 0)
                        {
                            camEncccoder = camEcncod[0];
                            if (camEncccoder != null && camEncccoder.Length > 34)
                            {
                                camEncccoder = camEncccoder.Substring(0, 35);
                            }
                        }
                    }
                    else if(camEncccoder != null)
                    {
                        camEncccoder = camEncccoder.Substring(0, 35);
                    }

                    using (var httpClient = new HttpClient())
                    {
                        httpClient.Timeout = TimeSpan.FromSeconds(60);
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var reqdate = boschClipData[index].CreatedOn.ToString("yyyy-MM-dd HH:mm:ss");
                        var m3DvrInterface = new M3_Dvr_Interface()
                        {
                            record_Status = "R",
                            Transaction_type = "NIN",
                            Incident_Type_code = "DVR",
                            Problem_code = "DVR8",
                            Division = boschClipData[index].RailNumber,
                            Equip_code = camEncccoder,
                            Incident_desc = boschClipData[index].IncidentDescription,
                            Rail_Bus_Ind = "R",
                            Dims_Id = boschClipData[index].AxiomClipId,
                            Request_Created_Date = reqdate
                        };

                        var json = JsonConvert.SerializeObject(m3DvrInterface);
                        Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  M3 Request:: " + json);
                        var postData = new StringContent(json, Encoding.UTF8, "application/json");
                        var response = httpClient.PostAsync("http://apiservices.metro.net/M3DvrInterfacedata", postData).Result;
                        try
                        {
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                var UpDateClipStatus = _dbData.UpDateClipStatus(boschClipData[index].MakeRequestId, "7", boschClipData[index].ClipFilePath, null);
                                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  M3 request placed successfull with Railnumber :: " + boschClipData[index].RailNumber);
                            }
                        }
                        catch (Exception ex)
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  M3 exception code :: " + response.IsSuccessStatusCode);
                        }
                    }
                    boschRecordIndex++;
                    if (boschRecordIndex < boschRecordLength)
                        BoschConnection(boschRecordIndex);
                    else
                        amsProcess.processAMSRecords();
                }
                else if (boschClipData[index].ClipStatus == 2) //process pending requests only
                {
                    deviceConnector = new DeviceConnectorClass();
                    deviceConnector.ConnectResult += OnConnectResult;
                    deviceConnector.DefaultTimeout = 500;

                    m_mediaFileWriter = new Bosch.VideoSDK.MediaDatabase.MediaFileWriterClass();
                    // create media database class
                    mediaDb = new BoschMediaDatabaseBrowser();
                    mediaDb.Tracks.CollectionChanged += Tracks_CollectionChanged;
                    mediaDb.Records.CollectionChanged += Records_CollectionChanged;
                    mediaDb.OnStateChanged += mediaDb_OnStateChanged;
                    mediaDb.OnProgress += mediaDb_OnProgress;
                    string url = "axiom@" + boschClipData[index].Division;
                    deviceConnector.ConnectAsync(url, "");

                    m_tracks = new List<Bosch.VideoSDK.MediaDatabase.Track>();

                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Bosch Server Connection :: " + url);
                }
                else
                {
                    boschRecordIndex++;
                    if (boschRecordIndex < boschRecordLength)
                        BoschConnection(boschRecordIndex);
                    else
                        amsProcess.processAMSRecords();
                }
             }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  BoschConnection:: " + ex.Message);
            }
        }
        /*public void BoschAxiomConnection(int index)
        {
            try
            {
                int timeDifference = (int)(DateTime.Now - boschClipData[index].CreatedOn).TotalHours;
                if (boschClipData[index].ClipStatus == 2 && timeDifference > 48)
                {
                    using (var httpClient = new HttpClient())
                    {
                        httpClient.Timeout = TimeSpan.FromSeconds(60);
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var reqdate = boschClipData[index].CreatedOn.ToString("yyyy-MM-dd HH:mm:ss");
                        var m3DvrInterface = new M3_Dvr_Interface()
                        {
                            record_Status = "R",
                            Transaction_type = "NIN",
                            Incident_Type_code = "DVR",
                            Problem_code = "DVR8",
                            Equip_code = boschClipData[index].RailNumber,
                            Division = boschClipData[index].Division,
                            Incident_desc = boschClipData[index].IncidentDescription,
                            Rail_Bus_Ind = "R",
                            Dims_Id = boschClipData[index].AxiomClipId,
                            Request_Created_Date = reqdate
                        };

                        var json = JsonConvert.SerializeObject(m3DvrInterface);
                        var postData = new StringContent(json, Encoding.UTF8, "application/json");
                        var response = httpClient.PostAsync("http://apiservices.metro.net/M3DvrInterfacedata", postData).Result;
                        try
                        {
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                var UpDateClipStatus = _dbData.UpDateClipStatus(boschClipData[index].MakeRequestId, "7", boschClipData[index].ClipFilePath, null);
                                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  M3 request placed successfull with Railnumber :: " + boschClipData[index].RailNumber);
                            }
                        }
                        catch (Exception ex)
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  M3 exception code :: " + response.IsSuccessStatusCode);
                        }
                    }
                }
                else
                {
                    deviceConnector = new DeviceConnectorClass();
                    deviceConnector.ConnectResult += OnConnectResult;
                    deviceConnector.DefaultTimeout = 500;

                    m_mediaFileWriter = new Bosch.VideoSDK.MediaDatabase.MediaFileWriterClass();
                    // create media database class
                    mediaDb = new BoschMediaDatabaseBrowser();
                    mediaDb.Tracks.CollectionChanged += Tracks_CollectionChanged;
                    mediaDb.Records.CollectionChanged += Records_CollectionChanged;
                    mediaDb.OnStateChanged += mediaDb_OnStateChanged;
                    mediaDb.OnProgress += mediaDb_OnProgress;
                    string url = "axiom@" + boschClipData[index].Division;
                    deviceConnector.ConnectAsync(url, "");

                    m_tracks = new List<Bosch.VideoSDK.MediaDatabase.Track>();

                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Bosch Server Connection :: " + url);
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  BoschConnection:: " + ex.Message);
            }
        }*/
        public List<SpGetBoschClipData_Result> boschClipData = new List<SpGetBoschClipData_Result>();
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                processDVTelData();
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in Form1_Load() " + ex.Message);
            }
            try
            {
                boschAndAMSCall();
                
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in Form1_Load() " + ex.Message);
            }
        }

        private string BoschImdFileCreation(string clipPath,string imdpath, Dictionary<string,string> tagsDictionary)
        {
            try
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  OnStart");
                var myMovieFile = new FileInfo(clipPath);
                string m3UFilePath = clipPath.Replace("mp4","m3u");
                var m3UFile = new FileInfo(m3UFilePath);
                var fileStart = clipStartTime;
                var fileDuration = clipDuration;
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  fileDuration:: " + fileDuration.TotalSeconds+ " fileStart:: " + fileStart);

                var imd = new ImdFile();
                var mm = imd.Multimedia.AddNew();
                
                if (tagsDictionary.ContainsKey("AREA"))
                {
                    mm.Area = tagsDictionary["AREA"];
                }
               
                if (tagsDictionary.ContainsKey("SOURCE"))
                {
                    mm.Source = tagsDictionary["SOURCE"];
                }
               

                if (tagsDictionary.ContainsKey("BRANDNAME"))
                {
                    mm.BrandName = tagsDictionary["BRANDNAME"];
                }               
                //Need to remove the three keys 1.AREA 2.SOURCE 3.BRANDNAME

                // Here we set up some information about the piece of media we're ingesting.
                if (tagsDictionary.ContainsKey("OWNER"))
                {
                    mm.Owners.AddNew(tagsDictionary["OWNER"]);
                }

                if (tagsDictionary.ContainsKey("TITLE"))
                {
                    mm.Title = tagsDictionary["TITLE"];
                }
                else
                    mm.Title = "Default Title";

                if (tagsDictionary.ContainsKey("DESCRIPTION"))
                {
                    mm.Description = tagsDictionary["DESCRIPTION"];
                }
                mm.StartAt = fileStart;
                mm.Duration = fileDuration;
                mm.AssetMedium = Medium.Video;
                mm.StartMethod = StartMethod.Known;
                mm.SystemRelation = SystemRelation.DirectTransfer;
                
                // This describes the file itself.
                var mf = mm.Files.AddNew(myMovieFile, true);
                mf.Importance = FileImportance.Primary;
                mf.StartTime = fileStart;
                mf.Duration = fileDuration;
                mf.FileSize = myMovieFile.Length;
                mf.FileMedium = Medium.Video;
                mf.OriginalName = Path.GetFileName(myMovieFile.Name);
                mf.CurrentName = Path.GetFileName(myMovieFile.Name);
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  " + mf.StartTime + " " + mf.Duration + " " + mf.FileSize + " " + mf.OriginalName + " " + mf.CurrentName);

                var mf1 = mm.Files.AddNew(m3UFile, true);
                mf1.Importance = FileImportance.Auxiliary;
                mf1.StartTime = fileStart;
                mf1.Duration = TimeSpan.FromSeconds(0);
                mf1.FileSize = m3UFile.Length;
                mf1.FileMedium = Medium.Other;
                mf1.OriginalName = Path.GetFileName(m3UFile.Name);
                mf1.CurrentName = Path.GetFileName(m3UFile.Name);
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  " + mf1.FileMedium + " "+mf1.Duration + " " + mf1.FileSize + " " + mf1.OriginalName + " " + mf1.CurrentName);

                // This adds a time zone to the file, so that local times can be determined.
                mm.TimeZones.AddCurrentTimeZone(TimeSource.DirectHandler, TimeRelation.Same, fileStart);

                Metadata md = mm.Metadata;

                foreach (var item in tagsDictionary)
                {                    
                    md.Tags.AddNew(item.Key, item.Value);
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Tags Adding:: " + item.Key+" "+ item.Value);
                }
               
                if (!Directory.Exists(imdpath))
                {
                    Directory.CreateDirectory(imdpath);
                }
                //string destinationImdFilePath = imdpath + @"\\" + axiomClipId + "-"+ Path.GetFileNameWithoutExtension(myMovieFile.Name) + ".imd";
                string destinationImdFilePath = imdpath + @"\\" + Path.GetFileNameWithoutExtension(myMovieFile.Name) + ".imd";
                imd.SaveToFile(new FileInfo(destinationImdFilePath), "Axiom xCell, Inc.", "7C31149EFA4260A73DE8F9A955FEF438ACF14ACA4528A4423338C070F9213F4D77DC4CFD754B19BA4854AA3D087F796944135AAE149E1212BBDB3612504064C2");

                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  File Created Successfully");

                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  end");
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                var UpDateClipStatus = _dbData.UpDateClipStatus(boschClipData[boschRecordIndex].MakeRequestId, "3", null + boschClipData[boschRecordIndex].AxiomClipId + "\\", null);
                boschRecordIndex++;
                if (boschRecordIndex < boschRecordLength)
                    BoschConnection(boschRecordIndex);
                else
                    amsProcess.processAMSRecords();
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() +  "Exception :: " + ex.Message);
                return ex.Message;
            }
        }
        
        Dictionary<string, string> tagsDictionary = null;
        string imdDestinationPathTemp1 = null;
        string axiomClipId = null;
        DateTime clipStartTime = DateTime.Now;
        TimeSpan clipDuration = TimeSpan.FromSeconds(0);

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Hide();
        }

        string boschfileName = null;
        private string BoschDownLoadAndImdCreation(SpGetBoschClipData_Result bosch,string fileName)
        {
            try
            {
                axiomClipId = bosch.AxiomClipId;
                clipStartTime = Convert.ToDateTime(bosch.StartTime);
                DateTime sdt = Convert.ToDateTime(bosch.StartTime);
                DateTime edt = Convert.ToDateTime(bosch.EndTime);
                clipDuration = edt.Subtract(sdt);
                
                tagsDictionary = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                boschfileName = fileName;
                string localClipPathTemp1 = @"\\MTAAXIOMS01\ClipDatabase\" + bosch.AxiomClipId + "\\";

                imdDestinationPathTemp1 = @"\\MTAAXIOMS01\ClipDatabase\" + bosch.AxiomClipId + "\\";

                if (!Directory.Exists(localClipPathTemp1))
                {
                    Directory.CreateDirectory(localClipPathTemp1);
                }


                if (!String.IsNullOrEmpty(bosch.TagsJson))
                {
                    try
                    {
                        var TagsData = XDocument.Parse(bosch.TagsJson);

                        if (TagsData.Root != null)
                        {
                            var tagResult =
                                TagsData.Root.Descendants("Tag")
                                    .Select(row => new
                                    {
                                        Name = row.Element("Name"),
                                        Value = row.Element("Value"),
                                    }).ToList();
                            try
                            {
                                
                            }
                            catch (Exception ex)
                            {
                                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Exception:" + ex.Message);
                            }
                            for (int i = 0; i < tagResult.Count; i++)
                            {
                                tagsDictionary.Add(tagResult[i].Name.Value.ToUpper(), tagResult[i].Value.Value);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " : " + ex.Message);
                        return "Error";
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private void boschAndAMSCall()
        {
            boschClipData = _dbData.GetBoschClipToCheckStatus();//Get records by avaliable status
            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Make Clip Request Records Size" + Convert.ToString(boschClipData.Count));
            boschRecordIndex = 0;
            if (boschClipData.Count > 0)
            {
                boschRecordLength = boschClipData.Count;
                BoschConnection(boschRecordIndex);
            }
            else
            {
                amsProcess.processAMSRecords();
            }
        }


        #region "DVTel Start"
        private readonly Type[] m_TypesOfInterest = { typeof(IConfigurationEntity) };
        private IReadonlyEntitiesCollection<IConfigurationEntity> m_Entities;
        public void processDVTelData()
        {
            var dvtelClipDataM3 = _dbData.GetDVTelClipToCheckStatus("TRUE");
            if (dvtelClipDataM3.Count > 0)
                farwordToM3(dvtelClipDataM3);
            var dvtelClipData = _dbData.GetDVTelClipToCheckStatus("FALSE");
            if (dvtelClipData.Count > 0)
            {
                string s_hostName = GetHostName;
                string s_userName = GetUserName;
                string s_password = GetPassword;

                LoginManager.InitDVTelSDK();
                try
                {
                    LoginManager.Instance.Login(s_hostName, s_userName, s_password);
                    IAdministrationAPI adminAPI = LoginManager.Instance.DvtelSystem.AdministrationAPI;
                    adminAPI.AddCachedEntityType(typeof(IConfigurationEntity), null);
                    System.Threading.Thread.Sleep(5000);
                    try
                    {
                        m_Entities = adminAPI.GetCachedEntitiesOfTypes(m_TypesOfInterest, null);
                    }
                    catch (Exception ex)
                    {
                        Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in processDVTelData() " + ex.Message);
                        adminAPI.AddCachedEntityType(typeof(IConfigurationEntity), null);
                        m_Entities = adminAPI.GetCachedEntitiesOfTypes(m_TypesOfInterest, null);
                    }
                    AddEntitiesToList(m_Entities, dvtelClipData);
                    LoginManager.Instance.Logoff();
                  //  LoginManager.Instance.Shutdown();
                }
                catch(Exception ex)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in processDVTelData() " + ex.Message);
                    LoginManager.Instance.Logoff();
                    //LoginManager.Instance.Shutdown();
                }
            }
        }
        private void farwordToM3(List<SpGetDVTelClipData_Result> dvtelClipData)
        {
            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in Form1.cs Make Clip Request Records Size" + Convert.ToString(dvtelClipData.Count));
            foreach (var dvtel in dvtelClipData)
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(60);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var reqdate = dvtel.CreatedOn.ToString("yyyy-MM-dd HH:mm:ss");
                    var m3DvrInterface = new M3_Dvr_Interface()
                    {
                        record_Status = "R",
                        Transaction_type = "NIN",
                        Incident_Type_code = "DVR",
                        Problem_code = "DVR6",
                        Equip_code = dvtel.BusNumber,
                        Division = dvtel.Division,
                        Incident_desc = dvtel.IncidentDescription,
                        Rail_Bus_Ind = "B",
                        Dims_Id = dvtel.AxiomClipId,
                        Request_Created_Date = reqdate
                    };

                    var json = JsonConvert.SerializeObject(m3DvrInterface);
                    var postData = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = httpClient.PostAsync("http://apiservices.metro.net/M3DvrInterfacedata", postData).Result;
                    try
                    {
                        response.EnsureSuccessStatusCode();
                        if (response.IsSuccessStatusCode)
                        {
                            var UpDateClipStatus = _dbData.UpDateClipStatus(dvtel.MakeRequestId, "7", dvtel.ClipFilePath, null);
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in dvtel Form1.cs " + dvtel.MakeRequestId + " status updated to CANCELLED " + json);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (response.StatusCode == HttpStatusCode.InternalServerError)
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in dvtel Form1.cs Status code : 500 " + ex.Message);
                        else
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in dvtel Form1.cs " + ex.Message);
                    }
                }
            }
        }
        IConfigurationEntity[] p_sceneEntity;
        public IConfigurationEntity[] SceneEntity
        {
            get { return p_sceneEntity; }
            set { p_sceneEntity = value; }
        }
        public static int MAX_QUERY_RESULTS = 150;
        private void AddEntitiesToList(IReadonlyEntitiesCollection<IConfigurationEntity> entities, List<SpGetDVTelClipData_Result> dvtelClipData)
        {
            p_sceneEntity = new IConfigurationEntity[1];  //new IConfigurationEntity[entities.Count];
           // var dvtelClipData = _dbData.GetDVTelClipToCheckStatus("FALSE");
            int i = 0;
            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in Form1.cs searching clips for export " + DateTime.Now.ToString());
            foreach (var dvt in dvtelClipData)
            {
                foreach (IConfigurationEntity scene in entities.Values)
                {
                    ListViewItem item = new ListViewItem(scene.Name);
                    item.Tag = scene;
                    if (scene.Name == dvt.BusNumber)
                    {
                        p_sceneEntity[0] = (IDisplayableEntity)item.Tag;
                        break;
                    }
                }
                IDisplayableEntity[] results = PerformQuery(p_sceneEntity);
                if (results != null)
                {
                    ExportClip(results, dvt, (i == dvtelClipData.Count));
                }
                else
                {
                    var UpDateClipStatus = _dbData.UpDateClipStatus(dvt.MakeRequestId, "3", dvt.ClipFilePath, null);
                }
            }
        }
        private IDisplayableEntity[] PerformQuery(IConfigurationEntity[] scenes)
        {
            IReadWriteEntitiesCollection<IRecordableSceneEntity> recordableScenes;
            if (scenes != null)
            {
                recordableScenes = DVTelObjectsFactory.Instance.CreateObject<IReadWriteEntitiesCollection<IRecordableSceneEntity>>();
                foreach (IConfigurationEntity entity in scenes)
                {
                    if (!(entity is IRecordableSceneEntity))
                    {
                        // TODO: add status report
                        return null;
                    }
                    recordableScenes.Add((IRecordableSceneEntity)entity);
                }
            }
            else
            {
                // Log saying "No entities exist. Aborting query"
                return null;
            }
            IDisplayableEntity[] results = null;
            AxiomQueryImpl objAxiomQueryImpl = new AxiomQueryImpl(); ;
            IReadonlyEntitiesCollection<IClipEntity> clips = AxiomQueryImpl.QueryClips(recordableScenes.AsReadOnly(), Reason, ProtectionLevel, From, To, (Initiators == null ? null : Initiators.AsReadOnly()), MAX_QUERY_RESULTS);
            if (clips != null)
            {
                results = new IClipEntity[clips.Count];
                clips.Values.CopyTo((IClipEntity[])results, 0);
            }

            return results;

        }
        /// <summary>
        /// Exports the searched clip according to chosen parameters
        /// </summary>
        private void ExportClip(IDisplayableEntity[] scenes, SpGetDVTelClipData_Result dvt, bool isExit)
        {
            IDisplayableEntity[] results = scenes;
            ExportImpl m_ExportImpl = new ExportImpl();

            if (results.Length > 0)
            {
                //check that we don't have more then one video clip to export
                string localClipExportPath = @"C:\Users\Public\ClipDatabase01\" + dvt.AxiomClipId + "\\";
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in Form1.cs starting to export " + DateTime.Now.ToString());
                if (!Directory.Exists(localClipExportPath))
                {
                    Directory.CreateDirectory(localClipExportPath);
                }

                if (Directory.Exists(localClipExportPath))
                {
                    m_ExportImpl.To = Convert.ToDateTime(dvt.StartTime);
                    m_ExportImpl.From = Convert.ToDateTime(dvt.EndTime);
                    m_ExportImpl.TargetFilePath = localClipExportPath;
                    m_ExportImpl.MakeRequestId = dvt.MakeRequestId;
                    m_ExportImpl.clipData = dvt;
                    m_ExportImpl.Export(results,
                                           true /*m_DurationCheckBox.Checked*/,
                                           true /*m_checkBoxTargetFileSize.Checked*/,
                                           false/* m_checkBoxTargetBaseName.Checked*/,
                                           false/* m_ExportOSDCheckBox.Checked*/,
                                           true/* m_InteleavedExportCheckBox.Checked*/);
                }
            }
            //if (m_ExportImpl.isdvtelEnd)
            //    boschAndAMSCall();
        }
        int p_reason = 479;
        public ArchivingReason Reason
        {
            get { return (ArchivingReason)p_reason; }
        }
        protected DateTime From
        {
            get
            {
                return DateTime.Now.AddMonths(-2);
            }
        }

        /// <summary>
        /// The time to end the query from.
        /// </summary>
        protected DateTime To
        {
            get
            {
                return DateTime.Now.AddMonths(3);
            }
        }
        public IReadWriteEntitiesCollection<IConfigurationEntity> Initiators
        {
            get
            {
                /*
                IReadWriteEntitiesCollection<IConfigurationEntity> initiators = null;
                if (m_hashInitiators.Contains(inititatorName))
                {
                    IUserEntity initiator = (IUserEntity)m_hashInitiators[inititatorName];
                    initiators = DVTelObjectsFactory.Instance.CreateObject<IReadWriteEntitiesCollection<IConfigurationEntity>>();
                    initiators.Add(initiator);
                }

                */
                return null;
            }
            //set { p_protectionLevel = value; }
        }
        /// <summary>
        /// Both =3
        ///Protected=1
        ///NotProtected=2
        /// </summary>
        int p_protectionLevel = 3;
        public ProtectionFilterEnum ProtectionLevel
        {
            get { return (ProtectionFilterEnum)p_protectionLevel; }
        }
        public string GetHostName
        {
            get
            {
                string hostName = Helper.GetSetting("DVTelHostName");
                return hostName;
            }
        }
        public string GetUserName
        {
            get
            {
                string userName = Helper.GetSetting("DVTelUserName");
                return userName;
            }
        }
        public string GetPassword
        {
            get
            {
                string password = Helper.GetSetting("DVTelPassword");
                return password;
            }
        }
        #endregion 
    }
}
