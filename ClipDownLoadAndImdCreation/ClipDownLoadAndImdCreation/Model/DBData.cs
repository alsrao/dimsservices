﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClipDownLoadAndImdCreation.Model
{
    public class DBData
    {
        public List<SpGetAMSClipData_Result> GetAmsClipToCheckStatus()
        {
            using (var db = new DBEntities())
            {
                try
                {
                    return db.SpGetAMSClipData().ToList();
                }
                catch (Exception ex)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DBData.cs " + ex.Message);
                    return null;
                }
            }
        }

        public int UpDateClipStatus(int makeClipMakeRequestId, string status, string sourceFilePath, string clipOutputFilePath)
        {
            using (var db = new DBEntities())
            {
                try
                {
                    var data = db.SpUpdateClipStatus(makeClipMakeRequestId, Convert.ToInt16(status), sourceFilePath, clipOutputFilePath);
                    return 1;
                }
                catch (Exception ex)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DBData.cs " + ex.Message);
                    return 0;
                }
            }
        }

        public List<SpGetBoschClipData_Result> GetBoschClipToCheckStatus()
        {
            using (var db = new DBEntities())
            {
                try
                {
                    return db.SpGetBoschClipData().ToList();
                }
                catch (Exception ex)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DBData.cs " + ex.Message);
                    return null;
                }
            }
        }

        public SpGetBoschServer_Result GetBoschServerRecord(string serverName)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpGetBoschServer(serverName).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DBData.cs " + ex.Message);
                return null;
            }
        }

        public int UpDatePercentage(string makeClipMakeRequestId,int percent, int status)
        {
            using (var db = new DBEntities())
            {
                try
                {
                    var data = db.SpUpdatePercentage(makeClipMakeRequestId, percent, status);
                    return 1;
                }
                catch (Exception ex)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DBData.cs " + ex.Message);
                    return 0;
                }
            }
        }
        public List<SpGetDVTelClipData_Result> GetDVTelClipToCheckStatus(string isM3Filter)
        {
            using (var db = new DBEntities())
            {
                try
                {
                    return db.SpGetDVTelClipData(isM3Filter).ToList();
                }
                catch (Exception ex)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DBData.cs " + ex.Message);
                    return null;
                }
            }
        }
    }
}
