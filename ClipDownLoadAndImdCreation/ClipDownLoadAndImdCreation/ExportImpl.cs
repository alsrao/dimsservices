using System;
using System.Collections.Generic;
using System.Drawing;
using DVTel.API;
using DVTel.API.Entities.AVT;
using DVTel.API.Entities.Physical;
using DVTel.API.Entities.Physical.Enums;
using DVTel.API.Entities.SystemObjects;
using DIMS.DVTelLibrary;
using ClipDownLoadAndImdCreation.Model;
using System.Xml.Linq;
using System.Linq;
using Insight.Shared.MetaContainer;
using System.IO;
using ClipDownLoadAndImdCreation;

//using DVTel.SDK.Samples.Base;

namespace DVTel.SDK.Axiom.Export
{
    public delegate void ExportProgressedEventHandler(IExportSessionEx exportSession, uint percentage);
    public delegate void ExportImplStartedEventHandler(IExportSessionEx exportSession);
    public delegate void ExportImplEndedEventHandler(IExportSessionEx exportSession, ExportResult result);
    internal interface IData
    {
        TimeSpan Duration { get; set; }

        /// <summary>
        /// The path to which the file will be written
        /// </summary>
        string TargetFilePath { get; set; }

        /// <summary>
        /// The target file base name.
        /// </summary>
        string TargetFileBaseName { get; set; }

        /// <summary>
        /// The VideoEncoderNames that will be used to encode Video stream. 
        /// </summary>
        string SelectedVideoEncoderName { get; set; }

        /// <summary>
        /// The AudioEncoderNames that will be used to encode Audio stream. 
        /// </summary>
        string SelectedAudioEncoderName { get; set; }

        DateTime From { get; set; }

        DateTime To { get; set; }

        int TargetFileSize { get; set; }

        TitlePositionEnum TitlePosition { get; set; }

        TitleAlignmentEnum TitleAlignment { get; set; }

        Font OverlayFont { get; set; }

        Color OverlayColor { get; set; }

        BackgroundModeEnum BackgroundMode { get; set; }
    }




    /// <summary>
    /// This sample demonstrates the usage of the Export API.
    /// It is compound of a GUI UserControl and an implementation class (this class).
    /// </summary>
    public class ExportImpl : IData
    {

        #region Events

        public event ExportProgressedEventHandler ExportProgressed;
        public event ExportImplStartedEventHandler ExportStarted;
        public event ExportImplEndedEventHandler ExportEnded;
        DBData _dbData = new DBData();

        #endregion Events

        #region Variables

        /// <summary>
        /// Names of CODECs existing on the user's machine
        /// </summary>
        private string[] m_VideoCodecList = null;
        private string[] m_AudioCodecList = null;

        /// <summary>
        /// A session ID is returned immediately at the beginning of each export session.
        /// This ID is later used in each event fired by the Export API.
        /// This Hashtable stores the session IDs and the exported clips related to them
        /// Key		= Guid	(the export session ID)
        /// Value	= IArchivedClipEntity (the clip)
        /// </summary>
        private readonly Dictionary<Guid, IExportSessionEx> m_ExportSessions = new Dictionary<Guid, IExportSessionEx>();

        private string m_DVTelContainerFilePath;
        private string m_SelectedVideoEncoderName = "DVTel Format";
        private string m_SelectedAudioEncoderName= "DVTel Format";
        private string m_TargetFilePath = @"C:\exports\Export Folder 8.0\ExpotPath";//@"C:\Axiom\Deva\22Mar2018\Code\DevTel.SDK.Axiom.Application\DevTelClipExport\ExportClips";
        private string m_TargetFileBaseName;
        private TimeSpan m_Duration = new TimeSpan(0, 0, 0, 20);
        private DateTime m_From;
        private DateTime m_To;
        private DateTime m_OfflineClipStartTime;
        private int m_nTargetFileSize= 4096;
        private TitlePositionEnum m_TitlePosition;
        private TitleAlignmentEnum m_TitleAlignment;
        private Font m_OverlayFont;
        private Color m_OverlayColor;
        private BackgroundModeEnum m_BackgroundMode;
        private readonly bool m_bTestEDBStartAndEndEvents = false;
        private IRecordableSceneEntity m_OfflineSceneEntity;
        public const string MP4_FILE_FORMAT = "MP4";

        #endregion Variables



        #region Properties


        public string DVTelFileFormat
        {
            get
            {
                string strDVTelFileFormat = null;
                try
                {

                    IDvtelSystemsManagerAPI manager = DvtelSystemsManagerProvider.Instance.DvtelSystemsManager;
                    // get the Export API
                    IExportAPI exportAPI = manager.GetOfflineAPI<IExportAPI>();

                    if (exportAPI != null)
                    {
                        strDVTelFileFormat = exportAPI.DVTelFileFormat;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("failed to retrieve DVTelFileFormat from export api. error {0}.", ex);
                }
                return strDVTelFileFormat;
            }
        }



        /// <summary>
        /// Target File Size (MB)
        /// </summary>
        public int TargetFileSize
        {
            get
            {
                return m_nTargetFileSize;
            }
            set
            {
                m_nTargetFileSize = value;
            }
        }

        /// <summary>
        /// The duration of the export
        /// </summary>
        public TimeSpan Duration
        {
            get
            {
                return m_Duration;
            }
            set
            {
                m_Duration = value;
            }
        }

        /// <summary>
        /// The path to export sub clip from.
        /// </summary>
        public string DVTelContainerFilePath
        {
            get
            {
                return m_DVTelContainerFilePath;
            }
            set
            {
                m_DVTelContainerFilePath = value;
            }
        }

        /// <summary>
        /// The path to which the file will be written
        /// </summary>
        public string TargetFilePath
        {
            get
            {
                return m_TargetFilePath;
            }
            set
            {
                m_TargetFilePath = value;
            }
        }
        //public DateTime clipStartTime
        //{
        //    get { return _clipStartTime; }
        //    set { _clipStartTime = value; }
        //}
        //public TimeSpan clipDuration
        //{
        //    get { return clipDuration; }
        //    set { clipDuration = value; }
        //}
        //public clipDuration 
        //DateTime _clipStartTime = DateTime.Now;
        //TimeSpan clipDuration = TimeSpan.FromSeconds(0);
        public bool _isLogoff = false;
        public bool IsLogoff
        {
            get { return _isLogoff; }
            set { _isLogoff = value; }
        }
        public int _makeRequestId;
        public int MakeRequestId
        {
            get { return _makeRequestId; }
            set { _makeRequestId = value; }
        }
        private bool _isdvtelEnd;
        public bool isdvtelEnd
        {
            get { return _isdvtelEnd; }
            set { _isdvtelEnd = value; }
        }
        SpGetDVTelClipData_Result _clipData;
        public SpGetDVTelClipData_Result clipData
        {
            get { return _clipData; }
            set { _clipData = value; }
        }
        /// <summary>
        /// The target file base name.
        /// </summary>
        public string TargetFileBaseName
        {
            get
            {
                return m_TargetFileBaseName;
            }
            set
            {
                m_TargetFileBaseName = value;
            }
        }
        /// <summary>
        /// The VideoEncoderNames that will be used to encode Video stream. 
        /// </summary>
        public string SelectedVideoEncoderName
        {
            get
            {
                return m_SelectedVideoEncoderName;
            }
            set
            {
                m_SelectedVideoEncoderName = value;
            }
        }

        /// <summary>
        /// The AudioEncoderNames that will be used to encode Audio stream. 
        /// </summary>
        public string SelectedAudioEncoderName
        {
            get
            {
                return m_SelectedAudioEncoderName;
            }
            set
            {
                m_SelectedAudioEncoderName = value;
            }
        }

        public DateTime From
        {
            get
            {
                return m_From;
            }
            set
            {
                m_From = value;
            }
        }


        public DateTime To
        {
            get
            {
                return m_To;
            }
            set
            {
                m_To = value;
            }
        }

        public DateTime OfflineClipStartTime
        {
            get
            {
                return m_OfflineClipStartTime;
            }
            set
            {
                m_OfflineClipStartTime = value;
            }
        }

        public IRecordableSceneEntity OfflineSceneEntity
        {
            get
            {
                return m_OfflineSceneEntity;
            }
            set
            {
                m_OfflineSceneEntity = value;
            }
        }

        public TitlePositionEnum TitlePosition
        {
            get
            {
                return m_TitlePosition;
            }
            set
            {
                m_TitlePosition = value;
            }
        }

        public TitleAlignmentEnum TitleAlignment
        {
            get
            {
                return m_TitleAlignment;
            }
            set
            {
                m_TitleAlignment = value;
            }
        }

        public Font OverlayFont
        {
            get
            {
                return m_OverlayFont;
            }
            set
            {
                m_OverlayFont = value;
            }
        }

        public Color OverlayColor
        {
            get
            {
                return m_OverlayColor;
            }
            set
            {
                m_OverlayColor = value;
            }
        }

        public BackgroundModeEnum BackgroundMode
        {
            get
            {
                return m_BackgroundMode;
            }
            set
            {
                m_BackgroundMode = value;
            }
        }

        #endregion Properties


        public ExportImpl()
        {
            BackgroundMode = BackgroundModeEnum.TRANSPARENT;
            OverlayFont = new Font("@Arial Unicode MS", (float)8.0);
            OverlayColor = new Color();
        }





        #region Public methods

        /// <summary>
        /// Uses the ExportAPI to get a list of existing Video encoders on the user's machine.
        /// </summary>
        /// <returns>an array of Video encoder's friendly names</returns>
        public string[] GetVideoEncoders()
        {
            try
            {
                IDvtelSystemsManagerAPI manager = DvtelSystemsManagerProvider.Instance.DvtelSystemsManager;
                // get the Export API of the given system
                IExportAPI exportAPI = manager.GetOfflineAPI<IExportAPI>();
                // use the API to get a list of CODECS names
                List<String> alVideoCODECS = exportAPI.LoadFiltersInVideoCompressorCategory();
                // store the list in a member array of strings
                m_VideoCodecList = new string[alVideoCODECS.Count];
                alVideoCODECS.CopyTo(m_VideoCodecList, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[ExportImpl.GetVideoEncoders] Export sample failed getting list of existing CODECs: " + ex);
            }
            return m_VideoCodecList;
        }

        /// <summary>
        /// Uses the ExportAPI to get a list of existing Audio encoders on the user's machine.
        /// </summary>
        /// <returns>an array of Video encoder's friendly names</returns>
        public string[] GetAudioEncoders()
        {
            try
            {
                IDvtelSystemsManagerAPI manager = DvtelSystemsManagerProvider.Instance.DvtelSystemsManager;
                // get the Export API of the given system
                IExportAPI exportAPI = manager.GetOfflineAPI<IExportAPI>();
                // use the API to get a list of CODECS names
                List<String> alAudioCODECS = exportAPI.LoadFiltersInVideoCompressorCategory();
                // store the list in a member array of strings
                m_AudioCodecList = new string[alAudioCODECS.Count];
                alAudioCODECS.CopyTo(m_AudioCodecList, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Export sample failed getting list of existing CODECs: " + ex);
            }
            return m_AudioCodecList;
        }


        /// <summary>
        /// Uses the Export API to export a clip according to the given arguments.
        /// </summary>
        public void ExportOffline(bool bExportByDuration, bool bTargetFileSize, bool bTargetFileBaseNameIsSet, bool bExportOSD)
        {          
            try
            {
                // get the Export API
                IDvtelSystemsManagerAPI manager = DvtelSystemsManagerProvider.Instance.DvtelSystemsManager;
                IExportAPI exportAPI = manager.GetOfflineAPI<IExportAPI>();

                IExportParametersEx exportParameters = exportAPI.CreateExportParametersEx(TargetFilePath);
                if (bTargetFileBaseNameIsSet)
                {
                    if ((TargetFileBaseName != null) && (TargetFileBaseName.Length > 0))
                    {
                        exportParameters.TargetFileBaseName = TargetFileBaseName;
                    }
                }
                if (bTargetFileSize)
                {
                    exportParameters.TargetFileSize = m_nTargetFileSize;
                }

                //If 'Export OSD' checkbox is checked - the OSD will be displayed 
                //on the exported clip also for non dvt formats.
                exportParameters.ShouldExportOSD = bExportOSD;

                TimeSpan duration = Duration;
                DateTime beginTime = OfflineClipStartTime;
                if (!bExportByDuration)
                {
                    duration = To - From;
                    beginTime = From;
                }

                string encoderName = null;

                if (OfflineSceneEntity is IVideoInSceneEntity)
                {
                    encoderName = SelectedVideoEncoderName;
                }
                if (OfflineSceneEntity is IAudioInSceneEntity)
                {
                    encoderName = SelectedAudioEncoderName;
                }

                if (encoderName != null)
                {
                    AddExportSourceInfoToExportParameters(encoderName, exportAPI, exportParameters, beginTime, duration);
                }
                else
                {
                    throw new Exception("Selected file scene is not a valid scene.");
                }

                IExportSessionEx exportSession = exportAPI.ExportArchive(exportParameters);

                if (!m_bTestEDBStartAndEndEvents)
                {
                    exportSession.ExportStartedEvent += OnExportStarted;
                    exportSession.ExportProgressedEvent += OnExportProgressed;
                    exportSession.ExportEndedEvent += OnExportEnded;
                }
                exportSession.StartExport();

                AddExportSession(exportSession);


            }
            catch (Exception ex)
            {
                Console.WriteLine("Export failed:\n" + ex);
            }
        }


        /// <summary>
        /// Uses the Export API to export a clip according to the given arguments.
        /// </summary>
        public void Export(IDisplayableEntity[] results, bool bExportByDuration, bool bTargetFileSize, bool bTargetFileBaseNameIsSet, bool bExportOSD, bool bInterleavedExport)
        {
            try
            {
                // get the DvtelSystem of the current clip
                IDvtelSystemId dvtelSystem = LoginManager.Instance.DvtelSystem;

                // get an Export API for the system that the clip belongs to
                IExportAPI exportAPI = dvtelSystem.GetAPI<IExportAPI>();

                IExportParametersEx exportParameters = null;
                if (bInterleavedExport)
                {
                    exportParameters = GetExportParameters(bTargetFileSize, bTargetFileBaseNameIsSet, bExportOSD, exportAPI);
                }
                foreach (IDisplayableEntity displayableEntity in results)
                {

                    if (!bInterleavedExport)
                    {
                        exportParameters = GetExportParameters(bTargetFileSize, bTargetFileBaseNameIsSet, bExportOSD, exportAPI);
                    }
                    // an archived clip can be exported both in DVTel and non-DVTel formats
                    if (displayableEntity is IArchivedClipEntity)
                    {
                        IArchivedClipEntity clip = displayableEntity as IArchivedClipEntity;

                        TimeSpan duration = Duration;
                        DateTime beginTime = clip.StartTime;
                        if (!bExportByDuration)
                        {
                            duration = To - From;
                            beginTime = From;
                        }

                        string encoderName = null;
                        if (clip.Scene is IVideoInSceneEntity)
                        {
                            encoderName = SelectedVideoEncoderName;
                        }
                        if (clip.Scene is IAudioInSceneEntity)
                        {
                            encoderName = SelectedAudioEncoderName;
                        }

                        if (encoderName != null)
                        {
                            AddExportSourceInfoToExportParameters(encoderName, exportAPI, exportParameters, clip, beginTime, duration);
                        }
                    }
                    // a stitched clip can only be exported in DVTel format
                    else if (displayableEntity is IStitchedCameraClipEntity)
                    {
                        IStitchedCameraClipEntity clip = displayableEntity as IStitchedCameraClipEntity;

                        TimeSpan duration = Duration;
                        if (!bExportByDuration)
                        {
                            duration = To - From;
                        }

                        exportParameters.AddExportSourceInfo(clip.StitchedCameraScene, clip.StartTime, duration, exportAPI.DVTelFileFormat, ExportFormatEnum.DVT, null, null);
                    }
                    if (!bInterleavedExport)
                    {
                        //If its not an Interleaved export we start each export separately with different parameters and in different sessions
                        StartExportAndAddToSessions(exportAPI, exportParameters);
                    }
                }
                if (bInterleavedExport)
                {
                    //In Interleaved export will happen just once
                    StartExportAndAddToSessions(exportAPI, exportParameters);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Export failed:\n" + ex);
            }
        }

        private void AddExportSourceInfoToExportParameters(string encoderName, IExportAPI exportAPI, IExportParametersEx exportParameters, IArchivedClipEntity clip, DateTime beginTime, TimeSpan duration)
        {
            if (encoderName.CompareTo(exportAPI.DVTelFileFormat) == 0)
            {
                exportParameters.AddExportSourceInfo(clip.Scene, beginTime, duration, exportAPI.DVTelFileFormat, ExportFormatEnum.DVT, null, null);
            }
            else if (encoderName.CompareTo(MP4_FILE_FORMAT) == 0)
            {
                exportParameters.AddExportSourceInfo(clip.Scene, beginTime, duration, exportAPI.DVTelFileFormat, ExportFormatEnum.MP4, null, null);
            }
            else
            {
                ITimeCodeOverlayParameters timeCodeOverlayParameters = exportParameters.CreateTimeCodeOverlayParameters(true, true);
                timeCodeOverlayParameters.SetTimeCodeOverlay(OverlayFont, OverlayColor,
                    TitleAlignment, TitlePosition,
                    BackgroundMode);
                exportParameters.AddExportSourceInfo(clip.Scene, beginTime, duration, encoderName, ExportFormatEnum.AVI, timeCodeOverlayParameters, null);
            }
        }

        private void AddExportSourceInfoToExportParameters(string encoderName, IExportAPI exportAPI, IExportParametersEx exportParameters, DateTime beginTime, TimeSpan duration)
        {
            List<string> sourceFileList = new List<string>();
            sourceFileList.Add(DVTelContainerFilePath);
            if (encoderName.CompareTo(exportAPI.DVTelFileFormat) == 0)
            {
                exportParameters.AddExportSourceInfo(sourceFileList, beginTime, duration, exportAPI.DVTelFileFormat, ExportFormatEnum.DVT, null, null);
            }
            else if (encoderName.CompareTo(MP4_FILE_FORMAT) == 0)
            {
                exportParameters.AddExportSourceInfo(sourceFileList, beginTime, duration, exportAPI.DVTelFileFormat, ExportFormatEnum.MP4, null, null);
            }
            else
            {
                ITimeCodeOverlayParameters timeCodeOverlayParameters = exportParameters.CreateTimeCodeOverlayParameters(true, true);
                timeCodeOverlayParameters.SetTimeCodeOverlay(OverlayFont, OverlayColor,
                    TitleAlignment, TitlePosition,
                    BackgroundMode);
                exportParameters.AddExportSourceInfo(sourceFileList, beginTime, duration, encoderName, ExportFormatEnum.AVI, timeCodeOverlayParameters, null);
            }
        }

        private void StartExportAndAddToSessions(IExportAPI exportAPI, IExportParametersEx exportParameters)
        {
            IExportSessionEx exportSession = exportAPI.ExportArchive(exportParameters);
           
            if (!m_bTestEDBStartAndEndEvents)
            {
                exportSession.ExportStartedEvent += OnExportStarted;
                exportSession.ExportProgressedEvent += OnExportProgressed;
                exportSession.ExportEndedEvent += OnExportEnded;
            }
            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ExportImpl.cs starting to export " + DateTime.Now.ToString());
            exportSession.StartExport();
            AddExportSession(exportSession);
            System.Threading.Thread.Sleep(5000);
        }
        private void DVTelIMDCreation(string fileNAme, string imdpath, Dictionary<string, string> tagsDictionary)
        {
            try
            {

                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  OnStart");

                var myMovieFile = new FileInfo(fileNAme);
                string m3UFilePath = fileNAme.Replace("mp4", "m3u");
                var m3UFile = new FileInfo(m3UFilePath);
                var fileStart = From;
                var fileDuration = Duration;
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  fileDuration:: " + fileDuration.TotalSeconds + " fileStart:: " + fileStart);

                var imd = new ImdFile();
                var mm = imd.Multimedia.AddNew();

                if (tagsDictionary.ContainsKey("AREA"))
                {
                    mm.Area = tagsDictionary["AREA"];
                }

                if (tagsDictionary.ContainsKey("SOURCE"))
                {
                    mm.Source = tagsDictionary["SOURCE"];
                }


                if (tagsDictionary.ContainsKey("BRANDNAME"))
                {
                    mm.BrandName = tagsDictionary["BRANDNAME"];
                }
                //Need to remove the three keys 1.AREA 2.SOURCE 3.BRANDNAME

                // Here we set up some information about the piece of media we're ingesting.
                if (tagsDictionary.ContainsKey("OWNER"))
                {
                    mm.Owners.AddNew(tagsDictionary["OWNER"]);
                }

                if (tagsDictionary.ContainsKey("TITLE"))
                {
                    mm.Title = tagsDictionary["TITLE"];
                }
                else
                    mm.Title = "Default Title";

                if (tagsDictionary.ContainsKey("DESCRIPTION"))
                {
                    mm.Description = tagsDictionary["DESCRIPTION"];
                }
                mm.StartAt = fileStart;
                mm.Duration = fileDuration;
                mm.AssetMedium = Medium.Video;
                mm.StartMethod = StartMethod.Known;
                mm.SystemRelation = SystemRelation.DirectTransfer;

                // This describes the file itself.
                var mf = mm.Files.AddNew(myMovieFile, true);
                mf.Importance = FileImportance.Primary;
                mf.StartTime = fileStart;
                mf.Duration = fileDuration;
                mf.FileSize = myMovieFile.Length;
                mf.FileMedium = Medium.Video;
                mf.OriginalName = Path.GetFileName(myMovieFile.Name);
                mf.CurrentName = Path.GetFileName(myMovieFile.Name);
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  " + mf.StartTime + " " + mf.Duration + " " + mf.FileSize + " " + mf.OriginalName + " " + mf.CurrentName);

                var mf1 = mm.Files.AddNew(m3UFile, true);
                mf1.Importance = FileImportance.Auxiliary;
                mf1.StartTime = fileStart;
                mf1.Duration = TimeSpan.FromSeconds(0);
                mf1.FileSize = m3UFile.Length;
                mf1.FileMedium = Medium.Other;
                mf1.OriginalName = Path.GetFileName(m3UFile.Name);
                mf1.CurrentName = Path.GetFileName(m3UFile.Name);
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  " + mf1.FileMedium + " " + mf1.Duration + " " + mf1.FileSize + " " + mf1.OriginalName + " " + mf1.CurrentName);

                // This adds a time zone to the file, so that local times can be determined.
                mm.TimeZones.AddCurrentTimeZone(TimeSource.DirectHandler, TimeRelation.Same, fileStart);

                Metadata md = mm.Metadata;

                foreach (var item in tagsDictionary)
                {
                    md.Tags.AddNew(item.Key, item.Value);
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  Tags Adding:: " + item.Key + " " + item.Value);
                }

                if (!Directory.Exists(imdpath))
                {
                    Directory.CreateDirectory(imdpath);
                }
                //string destinationImdFilePath = imdpath + @"\\" + axiomClipId + "-"+ Path.GetFileNameWithoutExtension(myMovieFile.Name) + ".imd";
                string destinationImdFilePath = imdpath + @"\\" + Path.GetFileNameWithoutExtension(myMovieFile.Name) + ".imd";
                imd.SaveToFile(new FileInfo(destinationImdFilePath), "Axiom xCell, Inc.", "7C31149EFA4260A73DE8F9A955FEF438ACF14ACA4528A4423338C070F9213F4D77DC4CFD754B19BA4854AA3D087F796944135AAE149E1212BBDB3612504064C2");

                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  File Created Successfully");

               // Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "  end");
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + "Exception :: " + ex.Message);
            }
        }

        private void DvTelIMDCreation(SpGetDVTelClipData_Result dvt, string TargetFilePath)
        {
            Dictionary<string, string> tagsDictionary = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            DateTime clipStartTime = Convert.ToDateTime(dvt.StartTime);
            DateTime sdt = Convert.ToDateTime(dvt.StartTime);
            DateTime edt = Convert.ToDateTime(dvt.EndTime);
            TimeSpan clipDuration = edt.Subtract(sdt);

            if (!String.IsNullOrEmpty(dvt.TagsJson))
            {
                try
                {
                    var TagsData = XDocument.Parse(dvt.TagsJson);

                    if (TagsData.Root != null)
                    {
                        var tagResult =
                            TagsData.Root.Descendants("Tag")
                                .Select(row => new
                                {
                                    Name = row.Element("Name"),
                                    Value = row.Element("Value"),
                                }).ToList();
                        try
                        {

                        }
                        catch (Exception ex)
                        {
                           // Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  Exception:" + ex.Message);
                        }
                        for (int i = 0; i < tagResult.Count; i++)
                        {
                            tagsDictionary.Add(tagResult[i].Name.Value.ToUpper(), tagResult[i].Value.Value);
                        }
                    }
                    // ImdFileCreation("pass downloaded clip file name", TargetFilePath, tagsDictionary, dvt.AxiomClipId, clipStartTime, clipDuration);
                //string fileNAme = Path.Combine(TargetFilePath, dvt.AxiomClipId + "-" + Path.GetFileName(dvt.BusNumber)) + ".dvt";
                    DirectoryInfo dirInfo = new DirectoryInfo(TargetFilePath);
                    FileInfo[] files = dirInfo.GetFiles("*.dvt");
                    List<string> lstFileNames = new List<string>();
                    foreach (FileInfo f in files)
                    {
                        lstFileNames.Add(f.FullName);
                    }
                    foreach (string fName in lstFileNames)
                    {
                        DVTelIMDCreation(fName, TargetFilePath, tagsDictionary);
                    }
                }
                catch (Exception ex)
                {
                    //Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  " + ex.Message);
                    //return "Error";
                }
            }
        }

        private void ImdFileCreation(string clipPath, string imdpath, Dictionary<string, string> tagsDictionary, string cxiomClipId, DateTime fileStart, TimeSpan fileDuration)
        {

            //Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  OnStart");
            var myMovieFile = new FileInfo(clipPath);
            //Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  AMS fileDuration:: " + fileDuration.TotalSeconds + " fileStart:: " + fileStart);

            var imd = new ImdFile();
            var mm = imd.Multimedia.AddNew();

            if (tagsDictionary.ContainsKey("AREA"))
            {
                mm.Area = tagsDictionary["AREA"];
            }

            if (tagsDictionary.ContainsKey("SOURCE"))
            {
                mm.Source = tagsDictionary["SOURCE"];
            }

            if (tagsDictionary.ContainsKey("BRANDNAME"))
            {
                mm.BrandName = tagsDictionary["BRANDNAME"];
            }

            // Here we set up some information about the piece of media we're ingesting.
            if (tagsDictionary.ContainsKey("OWNER"))
            {
                mm.Owners.AddNew(tagsDictionary["OWNER"]);
            }

            if (tagsDictionary.ContainsKey("TITLE"))
            {
                mm.Title = tagsDictionary["TITLE"];
            }
            else
                mm.Title = "Default Title";

            if (tagsDictionary.ContainsKey("DESCRIPTION"))
            {
                mm.Description = tagsDictionary["DESCRIPTION"];
            }

            mm.StartAt = fileStart;
            mm.Duration = fileDuration;
            mm.AssetMedium = Medium.Video;
            mm.StartMethod = StartMethod.Known;
            mm.SystemRelation = SystemRelation.DirectTransfer;

            // This describes the file itself.
            var mf = mm.Files.AddNew(myMovieFile, true);
            mf.Importance = FileImportance.Primary;
            mf.StartTime = fileStart;
            mf.Duration = fileDuration;
            mf.FileSize = myMovieFile.Length;
            mf.FileMedium = Medium.Video;
            mf.OriginalName = Path.GetFileName(myMovieFile.Name);
            //mf.CurrentName = Path.GetFileNameWithoutExtension(myMovieFile.Name) + ".imd";
            mf.CurrentName = Path.GetFileName(myMovieFile.Name);

            // This adds a time zone to the file, so that local times can be determined.
            mm.TimeZones.AddCurrentTimeZone(TimeSource.DirectHandler, TimeRelation.Same, fileStart);

            Metadata md = mm.Metadata;

            foreach (var item in tagsDictionary)
            {
                md.Tags.AddNew(item.Key, item.Value);
               // Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  Tags Adding:: " + item.Key + " " + item.Value);
            }
            if (!Directory.Exists(imdpath))
            {
                Directory.CreateDirectory(imdpath);
            }
            string destinationImdFilePath = imdpath + @"\\" + Path.GetFileNameWithoutExtension(myMovieFile.Name) + ".imd";
            imd.SaveToFile(new FileInfo(destinationImdFilePath), "Axiom xCell, Inc.", "7C31149EFA4260A73DE8F9A955FEF438ACF14ACA4528A4423338C070F9213F4D77DC4CFD754B19BA4854AA3D087F796944135AAE149E1212BBDB3612504064C2");

            //Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  File Created Successfully");

           // Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  end");
        }



        private IExportParametersEx GetExportParameters(bool bTargetFileSize, bool bTargetFileBaseNameIsSet, bool bExportOSD, IExportAPI exportAPI)
        {
            IExportParametersEx exportParameters = exportAPI.CreateExportParametersEx(TargetFilePath);

            if (bTargetFileBaseNameIsSet)
            {
                if ((TargetFileBaseName != null) && (TargetFileBaseName.Length > 0))
                {
                    exportParameters.TargetFileBaseName = TargetFileBaseName;
                }
            }
            if (bTargetFileSize)
            {
                exportParameters.TargetFileSize = m_nTargetFileSize;
            }

            //If 'Export OSD' checkbox is checked - the OSD will be displayed 
            //on the exported clip also for non dvt formats.
            exportParameters.ShouldExportOSD = bExportOSD;

            return exportParameters;
        }


        /// <summary>
        /// Subscribes to events of export progression
        /// </summary>
        /// <param name="dvtelSystem">The Federation (system) from which we would like to receive events</param>
        public void SubscribeToProgressEvents(IDvtelSystemId dvtelSystem)
        {
            // get the Export API
            //IDvtelSystemsManagerAPI manager = DvtelSystemsManagerProvider.Instance.DvtelSystemsManager;
            // get the Export API
            //IExportAPI exportAPI = manager.GetOfflineAPI<IExportAPI>();

            // use the Export API to get export progress events
            //exportAPI.ExportProgressed += OnExportProgressed;


            if (m_bTestEDBStartAndEndEvents)
            {
                //we register on edb export started and export ended events.
                RegisterToEDBExportEvent(dvtelSystem);
            }
        }





        /// <summary>
        /// Subscribes to events of export progression
        /// </summary>
        /// <param name="dvtelSystem">The Federation (system) from which we would like to receive events</param>
        public void UnSubscribeToProgressEvents(IDvtelSystemId dvtelSystem)
        {
            // get the Export API
            //IDvtelSystemsManagerAPI manager = DvtelSystemsManagerProvider.Instance.DvtelSystemsManager;
            // get the Export API
            //IExportAPI exportAPI = manager.GetOfflineAPI<IExportAPI>();

            // use the Export API to get export progress events
            //exportAPI.ExportProgressed -= OnExportProgressed;

            if (m_bTestEDBStartAndEndEvents)
            {
                //we register on edb export started and export ended events.
                UnRegisterEDBExportEvent(dvtelSystem);
            }
        }



        #endregion Public methods

        #region EDB Events Registration or Unregistration

        private void RegisterToEDBExportEvent(IDvtelSystemId dvtelSystem)
        {
            ICommonEventsFilterEntity ExportEventFilter = dvtelSystem.CreateEntity<ICommonEventsFilterEntity>();
            // give the filter an ID (any ID is fine)
            ExportEventFilter.InitializeAsNew(Guid.NewGuid());
            ExportEventFilter.Name = "RecordingClipExportedEventFilter";
            ExportEventFilter.EntityType = typeof(IRecordingClipExportedEventEntity);

            NotificationSubscriptionFlags flags = NotificationSubscriptionFlags.FromThisApplication | NotificationSubscriptionFlags.FromRemoteApplications;
            dvtelSystem.EventsAPI.SubscribeEx(ExportEventFilter, flags, EDBExportEventCallback);
        }

        private void UnRegisterEDBExportEvent(IDvtelSystemId dvtelSystem)
        {
            dvtelSystem.EventsAPI.Unsubscribe(new SingleNotificationCallbackHandler(EDBExportEventCallback));
        }

        #endregion EDB Events Registration or Unregistration

        #region Event Handlers


        protected void EDBExportEventCallback(IDvtelSystemId sender, IConfigurationEntity entity, IEventsApiArgs args)
        {
            try
            {
                if (entity is IDvtelEventEntity)
                {
                    IDvtelEventEntity eventEntity = (IDvtelEventEntity)entity;

                    //started
                    if (eventEntity.EntityInterface == typeof(IRecordingClipExportStartedEventEntity))
                    {
                        lock (m_ExportSessions)
                        {
                            Guid sessionId = ((IRecordingClipExportStartedEventEntity)eventEntity).SessionId;
                            // check if the arrived event belongs to a session we started
                            IExportSessionEx exportSession = m_ExportSessions[sessionId];
                            if (exportSession != null)
                            {
                                if (ExportStarted != null)
                                {
                                    ExportStarted(exportSession);
                                }
                            }
                        }
                    }
                    //ended
                    else if (eventEntity.EntityInterface == typeof(IRecordingClipExportEndedEventEntity))
                    {
                        lock (m_ExportSessions)
                        {
                            Guid sessionId = ((IRecordingClipExportEndedEventEntity)eventEntity).SessionId;
                            // check if the arrived event belongs to a session we started
                            IExportSessionEx exportSession = m_ExportSessions[sessionId];
                            if (exportSession != null)
                            {
                                if (ExportEnded != null)
                                {
                                    ExportResult result = ((IRecordingClipExportEndedEventEntity)eventEntity).Result;
                                    ExportEnded(exportSession, result);
                                }
                                RemoveExportSession(exportSession);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[ExportImpl.EDBExportEventCallback] Exception caught: {0}", ex);
            }
        }


        /// <summary>
        /// Stops export session.
        /// </summary>
        /// <param name="exportSessionGuid">Unique export session id.</param>
        /// <param name="dvtelSystem">The Federation (system) from which we would like to receive events.</param>
        public void AbortExport(Guid exportSessionGuid, IDvtelSystemId dvtelSystem)
        {
            // get the Export API
            lock (m_ExportSessions)
            {
                // check if the arrived event belongs to a session we started
                if (m_ExportSessions.ContainsKey(exportSessionGuid))
                {
                    m_ExportSessions[exportSessionGuid].AbortExport();
                }
            }
        }

        /// <summary>
        /// Each time a progression event arrives, fires the corresponding event
        /// </summary>
        private void OnExportProgressed(Guid sessionId, uint percentage)
        {
            lock (m_ExportSessions)
            {
                // check if the arrived event belongs to a session we started
                if (m_ExportSessions.ContainsKey(sessionId))
                {
                    if (ExportProgressed != null)
                    {
                        ExportProgressed(m_ExportSessions[sessionId], percentage);
                        try
                        {
                            int progressPercentage = Convert.ToInt32(percentage) - 5;
                            int fileStatus = 5;
                            if (progressPercentage < 95)
                                fileStatus = 5;
                            else
                                fileStatus = 1;
                            _dbData.UpDatePercentage(clipData.AxiomClipId, progressPercentage, fileStatus);
                        }
                        catch (Exception ex)
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ExportImpl.cs Exception :: " + ex.Message);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Each time a progression event arrives, fires the corresponding event
        /// </summary>
        private void OnExportEnded(Guid sessionId, ExportResult result)
        {
            lock (m_ExportSessions)
            {
                // check if the arrived event belongs to a session we started
                if (m_ExportSessions.ContainsKey(sessionId))
                {
                    //if (result.ToString().Equals("SUCCESS"))
                    {
                        Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ExportImpl.cs clip export is done " + DateTime.Now.ToString());
                        Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ExportImpl.cs updating clip request status to done/success 4 " + DateTime.Now.ToString());
                        var UpDateClipStatus1 = _dbData.UpDateClipStatus(MakeRequestId, "4", "", TargetFilePath);
                        Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ExportImpl.cs starting IMD file creation " + DateTime.Now.ToString());
                        DvTelIMDCreation(clipData, TargetFilePath);
                        Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ExportImpl.cs End IMD file generation " + DateTime.Now.ToString());
                    }
                    if (ExportEnded != null)
                    {
                        ExportEnded(m_ExportSessions[sessionId], result);
                    }
                    RemoveExportSession(m_ExportSessions[sessionId]);
                }
            }
        }

        /// <summary>
        /// Each time a progression event arrives, fires the corresponding event
        /// </summary>
        private void OnExportStarted(Guid sessionId)
        {
            lock (m_ExportSessions)
            {
                // check if the arrived event belongs to a session we started
                if (m_ExportSessions.ContainsKey(sessionId))
                {
                    if (ExportStarted != null)
                    {
                        ExportStarted(m_ExportSessions[sessionId]);
                    }
                }
            }
        }

        #endregion Event Handlers

        private void AddExportSession(IExportSessionEx exportSession)
        {
            lock (m_ExportSessions)
            {
                m_ExportSessions.Add(exportSession.Id, exportSession);
            }
        }


        private void RemoveExportSession(IExportSessionEx exportSession)
        {
            lock (m_ExportSessions)
            {
                if (!m_bTestEDBStartAndEndEvents)
                {
                    exportSession.ExportStartedEvent -= OnExportStarted;
                    exportSession.ExportProgressedEvent -= OnExportProgressed;
                    exportSession.ExportEndedEvent -= OnExportEnded;
                }
                m_ExportSessions.Remove(exportSession.Id);
            }
        }

    }
}
