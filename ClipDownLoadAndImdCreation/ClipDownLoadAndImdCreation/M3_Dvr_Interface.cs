﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ClipDownLoadAndImdCreation
{
    [DataContract]
    public class M3_Dvr_Interface
    {
        [DataMember]
        public string record_Status { get; set; }

        [DataMember]
        public string Transaction_type { get; set; }

        [DataMember]
        public string Incident_Type_code { get; set; }

        [DataMember]
        public string Problem_code { get; set; }

        [DataMember]
        public string Equip_code { get; set; }

        [DataMember]
        public string Division { get; set; }

        [DataMember]
        public string Incident_desc { get; set; }

        [DataMember]
        public string Rail_Bus_Ind { get; set; }

        [DataMember]
        public string Dims_Id { get; set; }

        [DataMember]
        public string Request_Created_Date { get; set; }

    }
}
