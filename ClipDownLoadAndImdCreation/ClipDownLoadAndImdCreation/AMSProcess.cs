﻿using ClipDownLoadAndImdCreation.Model;
using Insight.Shared.MetaContainer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ClipDownLoadAndImdCreation
{
    public class AMSProcess
    {
        DBData _dbData = new DBData();
        public void processAMSRecords()
        {
            try
            {
                var amsClipData = _dbData.GetAmsClipToCheckStatus();//Get records by avaliable status
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs Make Clip Request Records Size" + Convert.ToString(amsClipData.Count));
                foreach (var ams in amsClipData)
                {
                    int timeDifference = (int)(DateTime.Now - ams.CreatedOn).TotalHours;
                    if ((ams.ClipStatus == 2 || ams.ClipStatus == 3) && timeDifference > 48)
                    {
                        using (var httpClient = new HttpClient())
                        {
                            httpClient.Timeout = TimeSpan.FromSeconds(60);
                            httpClient.DefaultRequestHeaders.Accept.Clear();
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var reqdate = ams.CreatedOn.ToString("yyyy-MM-dd HH:mm:ss");
                            var m3DvrInterface = new M3_Dvr_Interface()
                            {
                                record_Status = "R",
                                Transaction_type = "NIN",
                                Incident_Type_code = "DVR",
                                Problem_code = "DVR6",
                                Equip_code = ams.BusNumber,
                                Division = ams.Division,
                                Incident_desc = ams.IncidentDescription,
                                Rail_Bus_Ind = "B",
                                Dims_Id = ams.AxiomClipId,
                                Request_Created_Date = reqdate
                            };

                            var json = JsonConvert.SerializeObject(m3DvrInterface);
                            var postData = new StringContent(json, Encoding.UTF8, "application/json");
                            var response = httpClient.PostAsync("http://apiservices.metro.net/M3DvrInterfacedata", postData).Result;
                            try
                            {
                                response.EnsureSuccessStatusCode();
                                if (response.IsSuccessStatusCode)
                                {
                                    var UpDateClipStatus = _dbData.UpDateClipStatus(ams.MakeRequestId, "7", ams.ClipFilePath, null);
                                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs "+ ams.MakeRequestId+" status updated to CANCELLED "+json);
                                }
                            }
                            catch (Exception ex)
                            {
                                if (response.StatusCode == HttpStatusCode.InternalServerError)
                                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs Status code : 500 " + ex.Message);
                                else
                                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs " + ex.Message);
                            }
                        }
                    }
                    else if(ams.ClipStatus != 3) //process pending and downloading requests only
                    {
                        //1. Get the clip staus 
                        var checkAMSStatus = CheckAMSStatus(ams);

                        //2. if available please update the status and path to the database
                        if (checkAMSStatus.MediaFileStatus == "1")
                        {
                            //3. download the clip and convert the imd file
                            var returnData = DownLoadAndImdCreation(ams);
                            if (returnData == "Success")
                            {
                                string clipOutputFilePath = @"\\MTAAXIOMS01\ClipDatabase\" + ams.AxiomClipId + "\\";
                                var UpDateClipStatus1 = _dbData.UpDateClipStatus(ams.MakeRequestId, "4", checkAMSStatus.MediaFilePath, clipOutputFilePath);
                            }
                            else
                            {
                                var UpDateClipStatus = _dbData.UpDateClipStatus(ams.MakeRequestId, "3", checkAMSStatus.MediaFilePath, null);
                            }
                        }
                        else
                        {
                            var UpDateClipStatus = _dbData.UpDateClipStatus(ams.MakeRequestId, checkAMSStatus.MediaFileStatus, checkAMSStatus.MediaFilePath, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs " + ex.Message);
            }
           System.Environment.Exit(1);
        }

        private string callProgressData(string mediaFileId,string systemId)
        {
            string progress = string.Empty;
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(Helper.GetSetting("AMSUrl"));
                httpClient.Timeout = TimeSpan.FromSeconds(60);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = httpClient.GetAsync("manage-archive-tasks.php?func=progress&media_file_id=" + mediaFileId + "&media_file_sys_id=" + systemId).Result;

                if (!response.IsSuccessStatusCode)
                {

                }
                var responseData = XDocument.Parse(response.Content.ReadAsStringAsync().Result);                
                if (responseData.Root != null)
                {
                    var result =
                          responseData.Root.Descendants("archiveProgress")
                              .Select(row => new
                              {
                                  Progress = (string)row.Element("progress"),
                                  current_filesize = (string)row.Element("current_filesize"),
                                  expected_filesize = (string)row.Element("expected_filesize"),
                              }).FirstOrDefault();

                    progress = result.Progress;

                }
            }
            return progress;
        }

        private ClsClipStatus CheckAMSStatus(SpGetAMSClipData_Result ams)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(Helper.GetSetting("AMSUrl"));
                    httpClient.Timeout = TimeSpan.FromSeconds(60);
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = httpClient.GetAsync("manage-media-data.php?func=list&location_id=" + Convert.ToString(ams.LocationId)).Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        return new ClsClipStatus
                        {
                            MakeRequestId = Convert.ToString(ams.MakeRequestId),
                            MediaFileStatus = Convert.ToString(3),
                            MediaFilePath = "Unable to get the status"
                        };
                    }
                    var responseData = XDocument.Parse(response.Content.ReadAsStringAsync().Result);
                    if (responseData.Root != null)
                    {
                        var result =
                           responseData.Root.Descendants("archive_media_file")
                               .Where(row =>
                               {
                                   var xElement = row.Element("media_file_id");
                                   var xElementServerId = row.Element("server_id");
                                   return ((xElement != null && Convert.ToInt64(xElement.Value) == ams.ClipRequestId) &&
                                   (xElementServerId != null && Convert.ToInt64(xElementServerId.Value) == ams.ServerId));
                               })
                               .Select(row => new
                               {
                                   MediaFilePath = (string)row.Element("media_file_path"),
                                   MediaFileStatus = (string)row.Element("media_file_status"),
                               }).FirstOrDefault();

                        string responseStatus = result.MediaFileStatus.ToUpper();

                        int fileStatus = 0;
                        if (responseStatus == "AVAILABLE" || responseStatus == "AVAILABLE-PROTECTED")
                        {
                            fileStatus = 1;
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  AVAILABLE:: " + ams.MakeRequestId + " " + result.MediaFilePath);
                            return new ClsClipStatus
                            {
                                MakeRequestId = Convert.ToString(ams.MakeRequestId),
                                MediaFileStatus = Convert.ToString(fileStatus),
                                MediaFilePath = result.MediaFilePath
                            };
                        }

                        if (responseStatus == "WAITING")
                        {
                            if (ams.DownloadPercentage == 0)
                                fileStatus = 2;
                        }

                        if (responseStatus == "UPLOADING")
                        {
                            var progress = callProgressData(Convert.ToString(ams.ClipRequestId), ams.SystemId);
                            int progressPercentage = Convert.ToInt32(progress) - 5;
                            //Update the progress to the database
                            _dbData.UpDatePercentage(ams.AxiomClipId, progressPercentage, 5);
                            if(progressPercentage < 95)//95
                                fileStatus = 5;
                            else
                                fileStatus = 1;
                        }

                        if ((responseStatus == "FAILED" || responseStatus == "CANCELLED"))
                            fileStatus = 3;

                        if (responseStatus == "DELETED")
                            fileStatus = 3;

                        return new ClsClipStatus
                        {
                            MakeRequestId = Convert.ToString(ams.MakeRequestId),
                            MediaFileStatus = Convert.ToString(fileStatus),
                            MediaFilePath = result.MediaFilePath

                        };
                    }
                    return new ClsClipStatus
                    {
                        MakeRequestId = Convert.ToString(ams.MakeRequestId),
                        MediaFileStatus = "3",
                        MediaFilePath = ""
                    };
                }
            }
            catch (Exception ex)
            {
                return new ClsClipStatus
                {
                    MakeRequestId = Convert.ToString(ams.MakeRequestId),
                    MediaFileStatus = "FAILED",
                    MediaFilePath = "Server error. Try again."
                };
            }
        }
        private string DownLoadAndImdCreation(SpGetAMSClipData_Result ams)
        {
            var division = ams.Division.ToString().PadLeft(2, '0');
            var sourcePathTemp1 = "";
            if (division == "05" || division == "13")
                sourcePathTemp1 = @"\\MTANASDIV" + division + "\\div" + division + "\\" + ams.ClipFilePath + "\\";
            else
                sourcePathTemp1 = @"\\MTANASDIV" + division + "\\Div" + division + "Video\\" + ams.ClipFilePath + "\\";
            
            string localClipPathTemp1 = @"\\MTAAXIOMS01\Users\Public\ClipDatabase\" + ams.AxiomClipId + "\\";
            string imdDestinationPathTemp1 = @"\\MTAAXIOMS01\Users\Public\ClipDatabase\" + ams.AxiomClipId + "\\";
           
            List<string> listFileNames = new List<string>();
            try
            {
                foreach (var d in System.IO.Directory.GetDirectories(sourcePathTemp1))
                {
                    var dirName = new DirectoryInfo(d).Name.ToString();
                    if (!Directory.Exists(localClipPathTemp1))
                    {
                        try
                        {
                            Directory.CreateDirectory(localClipPathTemp1);
                        }
                        catch (Exception ex)
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  " + ex.Message);
                        }
                    }
                    if (dirName.Contains("DIR"))
                    {
                        try
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  File Copy Started " + DateTime.Now.ToString());
                            foreach (var file in Directory.GetFiles(sourcePathTemp1 + @"\" + dirName, "*.PAR"))
                            {
                                using (var inputStream = File.Open(file, FileMode.Open, FileAccess.Read))
                                {
                                    if (File.Exists(Path.Combine(localClipPathTemp1, ams.AxiomClipId + "-" + Path.GetFileName(file))))
                                    {

                                        FileInfo finfo = new FileInfo(Path.Combine(localClipPathTemp1, ams.AxiomClipId + "-" + Path.GetFileName(file)));
                                        FileInfo sourceinfo = new FileInfo(Path.Combine(sourcePathTemp1, dirName +"\\" +Path.GetFileName(file)));
                                        int sourceLength = 0;
                                        sourceLength = (int)sourceinfo.Length;
                                        int totalLength = (int)finfo.Length;
                                        int RemaingDatalength = 0;
                                        RemaingDatalength = sourceLength - totalLength;
                                        int appLength = 0;
                                        int Remaining = 0;
                                        int bufferLength = 12488;
                                        byte[] buffer = new byte[bufferLength];
                                        int pointer = 1;
                                        FileStream fsa = new FileStream(Path.Combine(localClipPathTemp1, ams.AxiomClipId + "-" + Path.GetFileName(file)), FileMode.Append);
                                        while (pointer != 0)
                                        {
                                            if (finfo.Length >= sourceinfo.Length)
                                                break;
                                            inputStream.Position = (long)totalLength;
                                            inputStream.Read(buffer, 0, bufferLength);
                                            totalLength += bufferLength;
                                            appLength += bufferLength;
                                            fsa.Write(buffer, 0, bufferLength);
                                            Remaining = RemaingDatalength - appLength;
                                            if (Remaining < bufferLength)
                                            {
                                                byte[] buff = new byte[Remaining];
                                                inputStream.Read(buff, 0, Remaining);
                                                fsa.Write(buff, 0, Remaining);
                                                break;
                                            }
                                        }
                                        inputStream.Close();
                                        fsa.Close();

                                    }
                                    else
                                    {
                                        using (var outputStream = File.Open(Path.Combine(localClipPathTemp1, ams.AxiomClipId + "-" + Path.GetFileName(file)), FileMode.Create))
                                        {
                                            var bufferRead = -1;
                                            var bufferLength = 4096;
                                            var buffer = new byte[bufferLength];

                                            while ((bufferRead = inputStream.Read(buffer, 0, bufferLength)) > 0)
                                            {
                                                outputStream.Write(buffer, 0, bufferRead);
                                            }
                                        }
                                    }
                                }
                                listFileNames.Add(Path.Combine(localClipPathTemp1, ams.AxiomClipId + "-" + Path.GetFileName(file)));
                            }
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  File Copy Ended " + DateTime.Now.ToString());
                        }
                        catch (Exception ex)
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  Error:" + ex.Message);
                            //Helper.SendMail(sourcePathTemp1, "Error while copying the source file", "Unable to copy the source file");
                            return "Error";
                        }
                    }
                }
                
                // From NAS to Central server
                foreach (var d in System.IO.Directory.GetDirectories(sourcePathTemp1))
                {
                    var dirName = new DirectoryInfo(d).Name.ToString();
                    string centralServer = @"\\Metroshare01\dvr\Videos\Div" + division+"\\"+ams.ClipFilePath;
                    if (!Directory.Exists(centralServer))
                    {
                        try
                        {
                            Directory.CreateDirectory(centralServer);
                        }
                        catch (Exception ex)
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  " + ex.Message);
                        }
                    }
                    if (dirName.Contains("DIR"))
                    {
                        try
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  File Copy Started " + DateTime.Now.ToString());
                            foreach (var file in Directory.GetFiles(sourcePathTemp1 + @"\" + dirName, "*.PAR"))
                            {
                                using (var inputStream = File.Open(file, FileMode.Open, FileAccess.Read))
                                {                                    
                                    if (File.Exists(Path.Combine(centralServer, Path.GetFileName(file))))
                                    {

                                        FileInfo finfo = new FileInfo(Path.Combine(centralServer, Path.GetFileName(file)));
                                        FileInfo sourceinfo = new FileInfo(Path.Combine(sourcePathTemp1, dirName + "\\" + Path.GetFileName(file)));
                                        int sourceLength = 0;
                                        sourceLength = (int)sourceinfo.Length;
                                        int totalLength = (int)finfo.Length;
                                        int RemaingDatalength = 0;
                                        RemaingDatalength = sourceLength - totalLength;
                                        int appLength = 0;
                                        int Remaining = 0;
                                        int bufferLength = 12488;
                                        byte[] buffer = new byte[bufferLength];
                                        int pointer = 1;
                                        FileStream fsa = new FileStream(Path.Combine(centralServer, Path.GetFileName(file)), FileMode.Append);
                                        while (pointer != 0)
                                        {
                                            if (finfo.Length >= sourceinfo.Length)
                                                break;
                                            inputStream.Position = (long)totalLength;
                                            inputStream.Read(buffer, 0, bufferLength);
                                            totalLength += bufferLength;
                                            appLength += bufferLength;
                                            fsa.Write(buffer, 0, bufferLength);
                                            Remaining = RemaingDatalength - appLength;
                                            if (Remaining < bufferLength)
                                            {
                                                byte[] buff = new byte[Remaining];
                                                inputStream.Read(buff, 0, Remaining);
                                                fsa.Write(buff, 0, Remaining);
                                                break;
                                            }
                                        }
                                        inputStream.Close();
                                        fsa.Close();

                                    }
                                    else
                                    {
                                        using (var outputStream = File.Open(Path.Combine(centralServer, Path.GetFileName(file)), FileMode.Create))
                                        {
                                            var bufferRead = -1;
                                            var bufferLength = 4096;
                                            var buffer = new byte[bufferLength];

                                            while ((bufferRead = inputStream.Read(buffer, 0, bufferLength)) > 0)
                                            {
                                                outputStream.Write(buffer, 0, bufferRead);
                                            }
                                        }
                                    }
                                }
                                
                            }
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  File Copy Ended " + DateTime.Now.ToString());
                        }
                        catch (Exception ex)
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  Error:" + ex.Message);
                            //Helper.SendMail(sourcePathTemp1, "Error while copying the source file", "Unable to copy the source file");
                            return "Error";
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException uax)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  UnauthorizedAccessException:" + uax.Message);
                //Helper.SendMail(sourcePathTemp1, "Unauthorized Access Exception", "Unable to copy the source file");
                return "Error";
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  Exception:" + ex.Message);
                //Helper.SendMail(sourcePathTemp1, "Unauthorized Access Exception", "Unable to copy the source file");
                return "Error";
            }
            
            Dictionary<string, string> tagsDictionary = new Dictionary<string, string>();
            DateTime clipStartTime = Convert.ToDateTime(ams.StartTime);
            DateTime sdt = Convert.ToDateTime(ams.StartTime);
            DateTime edt = Convert.ToDateTime(ams.EndTime);
            TimeSpan clipDuration = edt.Subtract(sdt);
            
            if (!String.IsNullOrEmpty(ams.TagsJson))
            {
                try
                {
                    var TagsData = XDocument.Parse(ams.TagsJson);

                    if (TagsData.Root != null)
                    {
                        var tagResult =
                            TagsData.Root.Descendants("Tag")
                                .Select(row => new
                                {
                                    Name = row.Element("Name"),
                                    Value = row.Element("Value"),
                                }).ToList();
                        try
                        {
                          
                        }
                        catch (Exception ex)
                        {
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  Exception:" + ex.Message);
                        }
                        for (int i = 0; i < tagResult.Count; i++)
                        {
                            tagsDictionary.Add(tagResult[i].Name.Value.ToUpper(), tagResult[i].Value.Value);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  " + ex.Message);
                    return "Error";
                }
            }

            foreach (string fileName in listFileNames)
            {
                ImdFileCreation(fileName, imdDestinationPathTemp1, tagsDictionary, ams.AxiomClipId, clipStartTime, clipDuration);
            }
            return "Success";
        }

        private void ImdFileCreation(string clipPath, string imdpath, Dictionary<string, string> tagsDictionary,string cxiomClipId, DateTime fileStart, TimeSpan fileDuration)
        {

            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  OnStart");
            var myMovieFile = new FileInfo(clipPath);
            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  AMS fileDuration:: " + fileDuration.TotalSeconds + " fileStart:: " + fileStart);

            var imd = new ImdFile();
            var mm = imd.Multimedia.AddNew();

            if (tagsDictionary.ContainsKey("AREA"))
            {
                mm.Area = tagsDictionary["AREA"];
            }           

            if (tagsDictionary.ContainsKey("SOURCE"))
            {
                mm.Source = tagsDictionary["SOURCE"];
            }          

            if (tagsDictionary.ContainsKey("BRANDNAME"))
            {
                mm.BrandName = tagsDictionary["BRANDNAME"];
            }
            
            // Here we set up some information about the piece of media we're ingesting.
            if (tagsDictionary.ContainsKey("OWNER"))
            {
                mm.Owners.AddNew(tagsDictionary["OWNER"]);
            }
            
            if (tagsDictionary.ContainsKey("TITLE"))
            {
                mm.Title = tagsDictionary["TITLE"];
            }
            else
                mm.Title = "Default Title";

            if (tagsDictionary.ContainsKey("DESCRIPTION"))
            {
                mm.Description = tagsDictionary["DESCRIPTION"];
            }
            
            mm.StartAt = fileStart;
            mm.Duration = fileDuration;
            mm.AssetMedium = Medium.Video;
            mm.StartMethod = StartMethod.Known;
            mm.SystemRelation = SystemRelation.DirectTransfer;

            // This describes the file itself.
            var mf = mm.Files.AddNew(myMovieFile, true);
            mf.Importance = FileImportance.Primary;
            mf.StartTime = fileStart;
            mf.Duration = fileDuration;
            mf.FileSize = myMovieFile.Length;
            mf.FileMedium = Medium.Video;
            mf.OriginalName = Path.GetFileName(myMovieFile.Name);
            //mf.CurrentName = Path.GetFileNameWithoutExtension(myMovieFile.Name) + ".imd";
            mf.CurrentName = Path.GetFileName(myMovieFile.Name);

            // This adds a time zone to the file, so that local times can be determined.
            mm.TimeZones.AddCurrentTimeZone(TimeSource.DirectHandler, TimeRelation.Same, fileStart);

            Metadata md = mm.Metadata;

            foreach (var item in tagsDictionary)
            {
                md.Tags.AddNew(item.Key, item.Value);
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  Tags Adding:: " + item.Key + " " + item.Value);
            }
            if (!Directory.Exists(imdpath))
            {
                Directory.CreateDirectory(imdpath);
            }
            string destinationImdFilePath = imdpath + @"\\" + Path.GetFileNameWithoutExtension(myMovieFile.Name) + ".imd";
            imd.SaveToFile(new FileInfo(destinationImdFilePath), "Axiom xCell, Inc.", "7C31149EFA4260A73DE8F9A955FEF438ACF14ACA4528A4423338C070F9213F4D77DC4CFD754B19BA4854AA3D087F796944135AAE149E1212BBDB3612504064C2");

            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  File Created Successfully");

            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSProcess.cs  end");
        }
    }
}
