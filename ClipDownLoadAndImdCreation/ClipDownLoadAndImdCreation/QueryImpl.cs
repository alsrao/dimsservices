using System;
using DVTel.API;
using DVTel.API.Common;
using DVTel.API.Entities.Incidents;
using DVTel.API.Entities.Physical;
using DVTel.API.Entities.Physical.Enums;
using DVTel.API.Entities.SystemObjects;

namespace DVTel.SDK.Axiom.Base.Query
{
    /// <summary>
    /// This sample demonstrates executing of queries using the Recording API and the Incidents API.
    /// It is compound of a GUI UserControl and an implementation class (this class).
    /// </summary>
    public class AxiomQueryImpl
    {
        #region Methods

        /// <summary>
        /// Query for archived clips using the Recording API
        /// </summary>
        /// <param name="scenes">the scenes participating in the query. Value can not be null</param>
        /// <param name="reasons">the reasons that the clips were recorded. Null = any</param>
        /// <param name="protectionFilter">the protection level of the desired clips: Locked / Unlocked / Both</param>
        /// <param name="from">time to start searching from</param>
        /// <param name="to">time to end searching at</param>
        /// <param name="initiators">The entities who initiated the creation of the desired clips. Null = any</param>
        /// <param name="maxResults">maximum number of results to return</param>
        /// <returns>an array of clips or null</returns>
        public static IReadonlyEntitiesCollection<IClipEntity> QueryClips(
            IReadonlyEntitiesCollection<IRecordableSceneEntity> scenes,
            ArchivingReason reasons,
            ProtectionFilterEnum protectionFilter,
            DateTime from,
            DateTime to,
            IReadonlyEntitiesCollection<IConfigurationEntity> initiators,
            int maxResults)
        {
            IReadonlyEntitiesCollection<IClipEntity> results = null;
            if (scenes != null && scenes.Count > 0)
            {
                try
                {
                    // Since we only demonstrate a single federation system, we can
                    // assume that all selected entities hold the same IDvtelSystemId.
                    // so we will simply get the IDvtelSystemId object from one of the scenes.
                    IDvtelSystemId dvtelSystem = scenes.Values[0].DvtelSystem;

                    // get a Recording API that belongs to our specific federation (system)
                    IRecordingAPI recordingAPI = dvtelSystem.GetAPI<IRecordingAPI>();

                    // create a filter for the query
                    QueryClipsFilter filter = new QueryClipsFilter(scenes, from, to, maxResults);
                    filter.ArchivingReasons = reasons;
                    filter.Initiators = initiators;
                    filter.ProtectionFilter = protectionFilter;

                    bool cropped;

                    // perform the query
                    results = recordingAPI.QueryClips(filter, out cropped);

                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed querying clips: " + e);
                }
            }
            return results;
        }


        /// <summary>
        /// Query for Incidents using the IncidentsAPI
        /// </summary>
        /// <param name="scenes">the scenes participating in the query</param>
        /// <param name="incidentName">incident name</param>
        /// <param name="from">time to start looking from</param>
        /// <param name="to">time to end looking at</param>
        /// <param name="maxResults">maximum number of results to return</param>
        /// <returns>an array of incidents or null</returns>
        public static IIncidentEntity[] QueryIncidents(
            IReadWriteEntitiesCollection<IConfigurationEntity> scenes,
            string incidentName,
            DateTime from,
            DateTime to,
            int maxResults)
        {
            IIncidentEntity[] results = null;
            if (scenes != null && scenes.Count > 0)
            {
                // create a filter entity for the query:
                IIncidentsCommonFilterEntity filter = Filter(scenes, incidentName, from, to, maxResults);

                IReadonlyEntitiesCollection<IIncidentEntity> gcResults = null;
                try
                {
                    // Since we only demonstrate a single federation system, we can
                    // assume that all selected entities hold the same IDvtelSystemId.
                    // so we will simply get the IDvtelSystemId object from one of the scenes.
                    IDvtelSystemId dvtelSystem = scenes.Values[0].DvtelSystem;

                    // get an Incidents API that belongs to our specific federation (system)
                    IIncidentsAPI incidentsAPI = dvtelSystem.GetAPI<IIncidentsAPI>();

                    // perform the query
                    bool partialResults;
                    gcResults = incidentsAPI.QueryIncidents(filter, null, out partialResults);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed querying incidents: " + e);
                }

                // convert the query results back to an array:

                if (gcResults != null)
                {
                    results = new IIncidentEntity[gcResults.Count];
                    int i = 0;
                    foreach (IIncidentEntity entity in gcResults.Values)
                    {
                        results[i] = entity;
                        i++;
                    }
                }
            }
            return results;
        }

        /// <summary>
        /// Query for Bookmarks using the Recording API
        /// </summary>
        /// <param name="scenes">the scenes participating in the query</param>
        /// <param name="description">description</param>
        /// <param name="from">time to start looking from</param>
        /// <param name="to">time to end looking at</param>
        /// <param name="maxResults">maximum number of results to return</param>
        /// <returns>an array of bookmarks or null</returns>
        public static IReadonlyEntitiesCollection<ILoggerBookmarkEntity> QueryBookmarks(
            IReadonlyEntitiesCollection<IRecordableSceneEntity> scenes,
            string description,
            DateTime from,
            DateTime to,
            int maxResults)
        {
            IReadonlyEntitiesCollection<ILoggerBookmarkEntity> results = null;
            if (scenes != null && scenes.Count > 0)
            {
                try
                {
                    bool resultsCropped;

                    // Since we only demonstrate a single federation system, we can
                    // assume that all selected entities hold the same IDvtelSystemId.
                    // so we will simply get the IDvtelSystemId object from one of the scenes.
                    IDvtelSystemId dvtelSystem = scenes.Values[0].DvtelSystem;

                    // get a Recording API that belongs to our specific federation (system)
                    IRecordingAPI recordingAPI = dvtelSystem.GetAPI<IRecordingAPI>();

                    // create a filter for the query
                    QueryBookmarksFilter filter = new QueryBookmarksFilter(scenes, from, to, maxResults);
                    filter.Description = description;

                    // perform the query
                    results = recordingAPI.QueryBookmark(filter, out resultsCropped);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed querying clips: " + e);
                }
            }
            return results;
        }


        /// <summary>
        /// Query for motion records using the Recording API
        /// </summary>
        /// <param name="scenes">the scenes participating in the query. Value can not be null</param>
        /// <param name="from">time to start looking from</param>
        /// <param name="to">time to end looking at</param>
        /// <param name="resolution">interval between sequential motion samples</param>
        /// <param name="lowerBound">return only motion values above this bound</param>
        /// <param name="upperBound">return only motion valued below this bound</param>
        /// <param name="maxResults">maximum number of results to return</param>
        /// <returns>an array of motion records or null</returns>
        public static IReadonlyEntitiesCollection<IArchivedEnergySampleEntity> QueryMotion(
            IReadonlyEntitiesCollection<IRecordableSceneEntity> scenes,
            DateTime from,
            DateTime to,
            MOTION_ENERGY_DB_RESOLUTIONS resolution,
            byte lowerBound,
            byte upperBound,
            int maxResults)
        {
            IReadonlyEntitiesCollection<IArchivedEnergySampleEntity> results = null;
            if (scenes != null && scenes.Count > 0)
            {
                try
                {
                    bool resultsCropped;

                    // Since we only demonstrate a single federation system, we can
                    // assume that all selected entities hold the same IDvtelSystemId.
                    // so we will simply get the IDvtelSystemId object from one of the scenes.
                    IDvtelSystemId dvtelSystem = scenes.Values[0].DvtelSystem;

                    // get a Recording API that belongs to our specific federation (system)
                    IRecordingAPI recordingAPI = dvtelSystem.GetAPI<IRecordingAPI>();

                    // create a filter for the query
                    QueryMotionFilter filter = new QueryMotionFilter(scenes, from, to, maxResults, resolution, lowerBound, upperBound);

                    // perform the query
                    results = recordingAPI.QueryMotionRows(filter, out resultsCropped);

                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed querying clips: " + e);
                }
            }
            return results;
        }


        /// <summary>
        /// This private method creates a filter entity for the incidents query according to the given arguments
        /// </summary>
        /// <param name="scenes">A collection of Scenes participating in the query.</param>
        /// <param name="incidentName">The name of the incident.</param>
        /// <param name="from">time to start looking from</param>
        /// <param name="to">time to end looking at</param>
        /// <param name="maxResults">maximum number of results to return</param>
        /// <returns>the created filter</returns>
        private static IIncidentsCommonFilterEntity Filter(
            IReadWriteEntitiesCollection<IConfigurationEntity> scenes,
            string incidentName,
            DateTime from,
            DateTime to,
            int maxResults)
        {
            // get the Administration API
            IAdministrationAPI adminAPI = scenes[0].DvtelSystem.AdministrationAPI;
            // use the Administration API to create the filter entity
            IIncidentsCommonFilterEntity filter = adminAPI.CreateEntity<IIncidentsCommonFilterEntity>();
            // initialize it with a new ID
            filter.InitializeAsNew(Guid.NewGuid());
            // fill the filter's parameters:
            filter.MaxResults = maxResults;
            filter.StartDate = from;
            filter.EndDate = to;
            filter.Scenes.AddCollection(scenes);

            filter.IncidentName = incidentName;	// if not specified: any incident name
            return filter;
        }


        #endregion Methods

        #region Properties

        /// <summary>
        /// The collection of all the archiving reasons.
        /// </summary>
        public static object[] ArchivingReasons
        {
            get
            {
                object[] reasons = null;
                Array values = Enum.GetValues(typeof(ArchivingReason));
                if (values != null && values.Length > 0)
                {
                    reasons = new object[values.Length];
                    values.CopyTo(reasons, 0);
                }
                return reasons;
            }
        }

        public static object[] ProtectionLevels
        {
            get
            {
                object[] protectionLevels = null;
                Array values = Enum.GetValues(typeof(ProtectionFilterEnum));
                if (values != null && values.Length > 0)
                {
                    protectionLevels = new object[values.Length];
                    values.CopyTo(protectionLevels, 0);
                }
                return protectionLevels;
            }
        }

        /// <summary>
        /// The collection of all the motion resolutions.
        /// </summary>
        public static object[] MotionResolutions
        {
            get
            {
                object[] resolutions = null;
                Array values = Enum.GetValues(typeof(MOTION_ENERGY_DB_RESOLUTIONS));
                if (values != null && values.Length > 0)
                {
                    resolutions = new object[values.Length];
                    values.CopyTo(resolutions, 0);
                }
                return resolutions;
            }
        }


        #endregion Properties

        public static IReadonlyEntitiesCollection<ISceneGeoLocationEntity> QueryLocation(IReadonlyEntitiesCollection<IRecordableSceneEntity> scenes, DateTime fromTime, DateTime toTime, double westLatitude, double eastLatitude, double northLongitude, double southLongitude, int maxQueryResults)
        {
            IReadonlyEntitiesCollection<ISceneGeoLocationEntity> results = null;
            if (scenes != null && scenes.Count > 0)
            {
                try
                {
                    bool resultsCropped;

                    IDvtelSystemId dvtelSystem = scenes.Values[0].DvtelSystem;

                    IRecordingAPI recordingAPI = dvtelSystem.GetAPI<IRecordingAPI>();


                    IReadonlyEntitiesCollection<IRecordableSceneEntity> s = DVTelObjectsFactory.Instance.CreateObject<IReadonlyEntitiesCollection<IRecordableSceneEntity>>(); ;
                    // create a filter for the query
                    double latPOI = (northLongitude + southLongitude)/2;
                    double lonPOI = (westLatitude + eastLatitude) / 2;

                    QueryLocationFilter filter = new QueryLocationFilter(s, fromTime, toTime, maxQueryResults, westLatitude, eastLatitude, northLongitude, southLongitude, latPOI, lonPOI);


                    // perform the query
                    results = recordingAPI.QueryLocation(filter, out resultsCropped);
                    var a = results[0].Name;

                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed querying clips: " + e);
                }
            }
            return results;
        }
    }
}
