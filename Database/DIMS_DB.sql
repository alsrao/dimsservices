/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/
USE [master]
GO
/****** Object:  Database [DIMS]    Script Date: 12/15/2017 1:25:43 AM ******/
CREATE DATABASE [DIMS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DIMS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\DIMS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DIMS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\DIMS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [DIMS] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DIMS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DIMS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DIMS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DIMS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DIMS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DIMS] SET ARITHABORT OFF 
GO
ALTER DATABASE [DIMS] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [DIMS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DIMS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DIMS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DIMS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DIMS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DIMS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DIMS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DIMS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DIMS] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DIMS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DIMS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DIMS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DIMS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DIMS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DIMS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DIMS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DIMS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DIMS] SET  MULTI_USER 
GO
ALTER DATABASE [DIMS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DIMS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DIMS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DIMS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DIMS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DIMS] SET QUERY_STORE = OFF
GO
USE [DIMS]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [DIMS]
GO
/****** Object:  Table [dbo].[AMSLocations]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMSLocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [bigint] NULL,
	[SystemId] [varchar](500) NULL,
	[DivisionName] [varchar](800) NULL,
	[DivisionIP] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AMSServers]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMSServers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[serverId] [bigint] NULL,
	[serverName] [varchar](500) NULL,
	[serverAddress] [varchar](800) NULL,
	[serverPort] [bigint] NULL,
	[serverDynamicIp] [varchar](500) NULL,
	[systemId] [varchar](500) NULL,
	[sysId] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AMSSystems]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMSSystems](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[systemId] [varchar](500) NULL,
	[systemName] [varchar](800) NULL,
	[sysId] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BoschServers]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoschServers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Location] [varchar](500) NULL,
	[LocationAddress] [varchar](800) NULL,
	[StorageType] [varchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblMakeClip]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblMakeClip](
	[MakeRequestId] [int] IDENTITY(1,1) NOT NULL,
	[AxiomClipId] [varchar](50) NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[ClipRequestId] [int] NULL,
	[LocationId] [bigint] NULL,
	[SiteName] [varchar](150) NULL,
	[StartTime] [varchar](150) NOT NULL,
	[EndTime] [varchar](150) NOT NULL,
	[SegLen] [varchar](50) NULL,
	[Username] [varchar](100) NULL,
	[Password] [varchar](50) NULL,
	[HQEnabled] [varchar](50) NULL,
	[PreTime] [int] NULL,
	[PostTime] [int] NULL,
	[Camera] [bit] NULL,
	[Geofence] [varchar](50) NULL,
	[ServerId] [bigint] NULL,
	[SystemId] [varchar](300) NULL,
	[ProviderId] [int] NOT NULL,
	[ClipStatus] [int] NULL,
	[ClipFilePath] [varchar](500) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[BusNumber] [varchar](500) NULL,
	[Division] [varchar](500) NULL,
	[TagsJson] [varchar](max) NULL,
	[clipOutputFilePath] [varchar](500) NULL,
	[DownloadPercentage] [int] NULL,
 CONSTRAINT [PK_TblClipData_1] PRIMARY KEY CLUSTERED 
(
	[MakeRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblProvider]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblProvider](
	[ProviderId] [int] IDENTITY(1,1) NOT NULL,
	[ProviderName] [varchar](150) NOT NULL,
	[ImdLocation] [varchar](max) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_TblProvider] PRIMARY KEY CLUSTERED 
(
	[ProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblStatus]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblStatus](
	[Id] [int] NOT NULL,
	[statusDescription] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SpExistClip]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpExistClip]
@BusNumber VARCHAR(500),
@Division VARCHAR(500),
@StartTime VARCHAR(150),
@EndTime VARCHAR(150)

as
begin
Select * from TblMakeClip where  BusNumber=@BusNumber and Division=@Division and StartTime=@StartTime and EndTime=@EndTime;
end
GO
/****** Object:  StoredProcedure [dbo].[SpGetAMSClipData]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetAMSClipData]    
AS    
    
SELECT * FROM TblMakeClip  where ProviderId = 2 and  ClipStatus=2 or ClipStatus = 5 and   MakeRequestId<>1039
--ClipRequestId in (1,2,12) and ClipStatus=1
--ClipRequestId=118 
--ClipRequestId in (79,102)  
/****** Object:  StoredProcedure [dbo].[SpInsMakeClip]    Script Date: 8/17/2017 12:30:38 AM ******/    
SET ANSI_NULLS ON  
  
GO
/****** Object:  StoredProcedure [dbo].[SpGetBoschClipData]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetBoschClipData]    
AS    
    
SELECT * FROM TblMakeClip  where ProviderId = 3 and  ClipStatus=2
--  and CreatedOn >= convert(datetime, '2017-11-09 03:25:16.747')
SET ANSI_NULLS ON 

GO
/****** Object:  StoredProcedure [dbo].[SpGetBoschServer]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetBoschServer]
@Location VARCHAR(500)
AS

SELECT * FROM BoschServers WHERE UPPER(Location) = @Location
GO
/****** Object:  StoredProcedure [dbo].[SpGetClipData]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetClipData]
@WorkOrderId int
AS

SELECT * FROM TblMakeClip WHERE ClipRequestId = @WorkOrderId
GO
/****** Object:  StoredProcedure [dbo].[SpGetClipRequestId]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetClipRequestId]
@AxiomClipIdClipRequestId VARCHAR(50)
AS

SELECT ClipRequestId FROM TblMakeClip WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpInsMakeClip]    Script Date: 8/17/2017 12:30:38 AM ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[SpGetLocationId]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetLocationId]
@AxiomClipIdClipRequestId VARCHAR(50)
AS

SELECT LocationId FROM TblMakeClip WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpInsMakeClip]    Script Date: 8/17/2017 12:30:38 AM ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[SpGetLocationsRecord]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetLocationsRecord]
@DivisionIp VARCHAR(50)
AS

SELECT * FROM AMSLocations WHERE DivisionIP = @DivisionIp
GO
/****** Object:  StoredProcedure [dbo].[SpGetRecord]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetRecord]
@AxiomClipIdClipRequestId VARCHAR(50)
AS

SELECT * FROM TblMakeClip WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpInsMakeClip]    Script Date: 8/17/2017 12:30:38 AM ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[SpGetServerRecord]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetServerRecord]
@serverName VARCHAR(500)
AS

SELECT * FROM AMSServers WHERE serverName = @serverName

GO
/****** Object:  StoredProcedure [dbo].[SpGetSystemRecord]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpGetSystemRecord]
@systemId varchar(500)
as
begin

select * from AMSSystems where systemId = @systemId;
end
GO
/****** Object:  StoredProcedure [dbo].[SpInsLocations]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInsLocations]
@LocationId BIGINT,
@SystemId VARCHAR(500),
@DivisionName VARCHAR(800),
@DivisionIP VARCHAR(500)
AS

INSERT INTO AMSLocations VALUES(@LocationId,@SystemId,@DivisionName,@DivisionIP)

SELECT @DivisionIP DivisionIP
GO
/****** Object:  StoredProcedure [dbo].[SpInsMakeClip]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInsMakeClip]
@AxiomClipRequestId VARCHAR(50),
@Type VARCHAR(100)
,@ClipRequestId INT
,@LocationId BIGINT
,@SiteName VARCHAR(150)
,@StartTime VARCHAR(150)
,@EndTime VARCHAR(150)
,@SegLen VARCHAR(50)
,@Username VARCHAR(100)
,@Password VARCHAR(50)
,@HQEnabled VARCHAR(50)
,@PreTime INT
,@PostTime INT
,@Camera  BIT
,@Geofence VARCHAR(50)
,@ServerId BIGINT
,@SystemId VARCHAR(300)
,@ProviderId INT
,@ClipStatus int
,@ClipFilePath VARCHAR(500)
,@BusNumber VARCHAR(500)
,@Division VARCHAR(500)
,@TagsJson VARCHAR(MAX)
,@clipOutputFilePath VARCHAR(500)
,@DownloadPercentage INT

AS
BEGIN

   INSERT INTO TblMakeClip
   SELECT @AxiomClipRequestId,@Type,@ClipRequestId,@LocationId,@SiteName,@StartTime,@EndTime,@SegLen,@Username,@Password,@HQEnabled,@PreTime,
   @PostTime,@Camera,@Geofence,@ServerId,@SystemId,@ProviderId,@ClipStatus,@ClipFilePath,1,GETDATE(),1,GETDATE(),@BusNumber,@Division,@TagsJson,@clipOutputFilePath,@DownloadPercentage
END
GO
/****** Object:  StoredProcedure [dbo].[SpInsSevers]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInsSevers]
@serverId BIGINT,
@serverName VARCHAR(500),
@serverAddress VARCHAR(800),
@serverPort BIGINT,
@serverDynamicIp VARCHAR(500),
@systemId VARCHAR(500),
@sysId VARCHAR(500)
AS

INSERT INTO AMSServers VALUES(@serverId,@serverName,@serverAddress,@serverPort,@serverDynamicIp,@systemId,@sysId)

GO
/****** Object:  StoredProcedure [dbo].[SpInsSystems]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpInsSystems]
@systemId varchar(500),
@systemName varchar(800),
@sysId varchar(500)
as
begin

insert into AMSSystems values(@systemId,@systemName,@sysId);
end
GO
/****** Object:  StoredProcedure [dbo].[SpUpdateClipStatus]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SpUpdateClipStatus]  
@MakeRequestId INT  
,@ClipStatus INT  
,@SourceFilePath VARCHAR(500)  
,@clipOutputFilePath VARCHAR(500)  
AS  
  
  UPDATE TblMakeClip SET ClipStatus=@ClipStatus,ClipFilePath=@SourceFilePath,clipOutputFilePath=@clipOutputFilePath 
  ,UpdatedOn = GETDATE()
  WHERE @MakeRequestId=MakeRequestId
GO
/****** Object:  StoredProcedure [dbo].[SpUpdateFilePath]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpUpdateFilePath]
@AxiomClipIdClipRequestId VARCHAR(50),
@ClipFilePath VARCHAR(500)
AS

UPDATE TblMakeClip SET ClipFilePath = @ClipFilePath WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpGetRecord]    Script Date: 8/28/2017 7:56:27 AM ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[SpUpdateLocationData]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpUpdateLocationData]
@LocationId BIGINT,
@SystemId VARCHAR(500),
@DivisionName VARCHAR(800),
@DivisionIP VARCHAR(500)
AS

UPDATE AMSLocations SET LocationId = @LocationId,SystemId = @SystemId,DivisionName = @DivisionName WHERE DivisionIP = @DivisionIP
GO
/****** Object:  StoredProcedure [dbo].[SpUpdatePercentage]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpUpdatePercentage]
@AxiomClipIdClipRequestId VARCHAR(50),
@DownloadPercentage INT,
@ClipStatus INT
AS

UPDATE TblMakeClip SET ClipStatus = @ClipStatus, DownloadPercentage = @DownloadPercentage WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpGetRecord]    Script Date: 8/28/2017 7:56:27 AM ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[SpUpdateServerRecord]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpUpdateServerRecord]
@serverId BIGINT,
@serverName VARCHAR(500),
@serverAddress VARCHAR(800),
@serverPort BIGINT,
@serverDynamicIp VARCHAR(500),
@systemId VARCHAR(500),
@sysId VARCHAR(500)
AS

UPDATE AMSServers SET serverAddress = @serverAddress, serverPort = @serverPort, serverDynamicIp = @serverDynamicIp, systemId = @systemId, sysId = @sysId WHERE serverId = @serverId and serverName = @serverName


GO
/****** Object:  StoredProcedure [dbo].[SpUpdateSystemRecord]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpUpdateSystemRecord]
@systemId VARCHAR(500),
@systemName VARCHAR(800),
@sysId VARCHAR(500)
AS

UPDATE AMSSystems SET sysId = @sysId WHERE systemId = @systemId and systemName = @systemName


GO
USE [master]
GO
ALTER DATABASE [DIMS] SET  READ_WRITE 
GO
