/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ProviderId]
      ,[ProviderName]
      ,[ImdLocation]
      ,[CreatedBy]
      ,[CreatedOn]
      ,[UpdatedBy]
      ,[UpdatedOn]
  FROM [DIMS].[dbo].[TblProvider]




  CREATE TABLE [dbo].[TblProvider](
	[ProviderId] [int] IDENTITY(1,1) NOT NULL,
	[ProviderName] [varchar](150) NOT NULL,
	[ImdLocation] [varchar](max) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_TblProvider] PRIMARY KEY CLUSTERED 
(
	[ProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblStatus]    Script Date: 12/15/2017 1:25:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  insert into TblProvider(ProviderName,ImdLocation,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn) values('APOLLO','\\MTAAXIOMS01\ClipDatabase',1,'2017-08-17 05:04:10.063',1,'2017-08-17 05:04:10.063');

  insert into TblProvider(ProviderName,ImdLocation,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn) values('AMS','\\MTAAXIOMS01\ClipDatabase',1,'2017-08-23 02:53:15.113',1,'2017-08-23 02:53:15.113');

  insert into TblProvider(ProviderName,ImdLocation,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn) values('BOSCH','\\MTAAXIOMS01\ClipDatabase',1,'2017-11-06 05:21:11.633',1,'2017-11-06 05:21:11.633');

  insert into TblProvider(ProviderName,ImdLocation,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn) values('DVTEL','\\MTAAXIOMS01\ClipDatabase',1,'2018-04-26 05:21:11.633',1,'2018-04-26 05:21:11.633');