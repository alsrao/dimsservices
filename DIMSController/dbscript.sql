
/****** Object:  Table [dbo].[TblMakeClip]    Script Date: 8/17/2017 12:30:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblMakeClip](
	[MakeRequestId] [int] IDENTITY(1,1) NOT NULL,
	[AxiomClipId] [varchar](50) NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[ClipRequestId] [int] NULL,
	[LocationId] [bigint] NULL,
	[SiteName] [varchar](150) NULL,
	[StartTime] [varchar](150) NOT NULL,
	[EndTime] [varchar](150) NOT NULL,
	[SegLen] [varchar](50) NULL,
	[Username] [varchar](100) NULL,
	[Password] [varchar](50) NULL,
	[HQEnabled] [varchar](50) NULL,
	[PreTime] [int] NULL,
	[PostTime] [int] NULL,
	[Camera] [bit] NULL,
	[Geofence] [varchar](50) NULL,
	[ServerId] [bigint] NULL,
	[SystemId] [varchar](300) NULL,
	[ProviderId] [int] NOT NULL,
	[ClipStatus] [int] NOT NULL,
	[ClipDownLoadStatus] [bit] NULL,
	[ClipFilePath] [varchar](500) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_TblClipData_1] PRIMARY KEY CLUSTERED 
(
	[MakeRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TblProvider]    Script Date: 8/17/2017 12:30:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblProvider](
	[ProviderId] [int] IDENTITY(1,1) NOT NULL,
	[ProviderName] [varchar](150) NOT NULL,
	[ImdLocation] [varchar](max) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_TblProvider] PRIMARY KEY CLUSTERED 
(
	[ProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AMSLocations]    Script Date: 04-Sep-17 03:52:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AMSLocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [bigint] NULL,
	[SystemId] [varchar](500) NULL,
	[DivisionName] [varchar](800) NULL,
	[DivisionIP] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AMSServers]    Script Date: 08-Sep-17 12:36:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AMSServers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[serverId] [bigint] NULL,
	[serverName] [varchar](500) NULL,
	[serverAddress] [varchar](800) NULL,
	[serverPort] [bigint] NULL,
	[serverDynamicIp] [varchar](500) NULL,
	[systemId] [varchar](500) NULL,
	[sysId] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SpInsSevers]    Script Date: 04-Sep-17 03:52:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInsSevers]
@serverId BIGINT,
@serverName VARCHAR(500),
@serverAddress VARCHAR(800),
@serverPort BIGINT,
@serverDynamicIp VARCHAR(500),
@systemId VARCHAR(500),
@sysId VARCHAR(500)
AS

INSERT INTO AMSServers VALUES(@serverId,@serverName,@serverAddress,@serverPort,@serverDynamicIp,@systemId,@sysId)

/****** Object:  StoredProcedure [dbo].[SpGetServerRecord]    Script Date: 04-Sep-17 03:52:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetServerRecord]
@serverId BIGINT
AS

SELECT * FROM AMSServers WHERE serverId = @serverId

/****** Object:  StoredProcedure [dbo].[SpInsLocations]    Script Date: 04-Sep-17 03:52:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInsLocations]
@LocationId BIGINT,
@SystemId VARCHAR(500),
@DivisionName VARCHAR(800),
@DivisionIP VARCHAR(500)
AS

INSERT INTO AMSLocations VALUES(@LocationId,@SystemId,@DivisionName,@DivisionIP)

/****** Object:  StoredProcedure [dbo].[SpUpdateLocationData]    Script Date: 04-Sep-17 03:52:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpUpdateLocationData]
@LocationId BIGINT,
@SystemId VARCHAR(500),
@DivisionName VARCHAR(800),
@DivisionIP VARCHAR(500)
AS

UPDATE AMSLocations SET LocationId = @LocationId,SystemId = @SystemId,DivisionName = @DivisionName WHERE DivisionIP = @DivisionIP
/****** Object:  StoredProcedure [dbo].[SpGetLocationsRecord]    Script Date: 04-Sep-17 03:52:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetLocationsRecord]
@DivisionIp VARCHAR(50)
AS

SELECT * FROM AMSLocations WHERE DivisionIP = @DivisionIp

/****** Object:  StoredProcedure [dbo].[SpGetClipRequestId]    Script Date: 8/17/2017 12:30:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[SpGetClipRequestId]    Script Date: 8/17/2017 7:56:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetClipRequestId]
@AxiomClipIdClipRequestId VARCHAR(50)
AS

SELECT ClipRequestId FROM TblMakeClip WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpGetLocationId]    Script Date: 8/17/2017 7:56:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetLocationId]
@AxiomClipIdClipRequestId VARCHAR(50)
AS

SELECT LocationId FROM TblMakeClip WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpUpdateFilePath]    Script Date: 8/28/2017 7:56:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpUpdateFilePath]
@AxiomClipIdClipRequestId VARCHAR(50),
@ClipFilePath VARCHAR(500)
AS

UPDATE TblMakeClip SET ClipFilePath = @ClipFilePath WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpGetRecord]    Script Date: 8/28/2017 7:56:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGetRecord]
@AxiomClipIdClipRequestId VARCHAR(50)
AS

SELECT * FROM TblMakeClip WHERE AxiomClipId = @AxiomClipIdClipRequestId
/****** Object:  StoredProcedure [dbo].[SpInsMakeClip]    Script Date: 8/17/2017 12:30:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInsMakeClip]
@Type VARCHAR(100)
,@ClipRequestId INT
,@LocationId BIGINT
,@SiteName VARCHAR(150)
,@StartTime VARCHAR(150)
,@EndTime VARCHAR(150)
,@SegLen VARCHAR(50)
,@Username VARCHAR(100)
,@Password VARCHAR(50)
,@HQEnabled VARCHAR(50)
,@PreTime INT
,@PostTime INT
,@Camera  BIT
,@Geofence VARCHAR(50)
,@ServerId BIGINT
,@SystemId VARCHAR(300)
,@ProviderId INT
,@ClipStatus BIT
,@ClipFilePath VARCHAR(500)

AS
BEGIN

   DECLARE @AxiomClipRequestId VARCHAR(50) = REPLACE(replace(replace(replace(CONVERT(VARCHAR(50), GETDATE(), 121), '-', ''), ' ', ''), ':', ''), '.', '')

   INSERT INTO TblMakeClip
   SELECT @AxiomClipRequestId,@Type,@ClipRequestId,@LocationId,@SiteName,@StartTime,@EndTime,@SegLen,@Username,@Password,@HQEnabled,@PreTime,
   @PostTime,@Camera,@Geofence,@ServerId,@SystemId,@ProviderId,@ClipStatus,0,@ClipFilePath,1,GETDATE(),1,GETDATE()

   SELECT @AxiomClipRequestId AxiomClipRequestId
END