using System;
using DVTel.API;
using DVTel.API.Common;
using DVTel.Common.AssemblyLoader;


namespace DIMS.DVTelLibrary
{
	/// <summary>
	/// Describes the event to be fired by this manager when logged-in to a DVTel system.
	/// </summary>
	public delegate void LoggedInHandler(string stLoggedInDirectory, Exception exFailure);
	
	/// <summary>
	/// Describes the event to be fired by this manager when logged-out from a DVTel system.
	/// </summary>
	public delegate void LoggedOutHandler(IDvtelSystemId loggedOutDvtelSystemId);

	/// <summary>
	/// Static utility that provides Login into DVTel systems,
	/// to be used by any SDK applications in need of such services.
	/// </summary>
	public class LoginManager
	{
		#region Static Data Members
		
		private static LoginManager ms_LoginManager = null;
		private static object		ms_Monitor = new object();
		
		#endregion Static Data Members
		
		#region Event Members
		
		/// <summary>
		/// The event to be fired by this manager when logged-in to a DVTel system.
		/// </summary>
		public event LoggedInHandler	LoggedIn;
		/// <summary>
		/// The event to be fired by this manager when logged-out from a DVTel system.
		/// </summary>
		public event LoggedOutHandler	LoggedOut;

		#endregion Event Members
		
		#region Data Members

		/// <summary>
		/// The reference to the DVTel system given by the Directory server when logged-in.
		/// </summary>
		private		IDvtelSystemId	m_DvtelSystem;						
		
		/// <summary>
		/// The name of the Directory server.
		/// </summary>
		protected	string			m_stDirectoryServer;
		
		/// <summary>
		/// The username to login with. 
		/// </summary>
		protected   string			m_stUsername;

		/// <summary>
		/// The password to login with.
		/// </summary>
		protected   string			m_stPassword;

		#endregion Data Members

		#region Properties

		/// <summary>
		/// Gets the reference to the logged-in DVTel system.
		/// </summary>
		public IDvtelSystemId DvtelSystem
		{
			get {return m_DvtelSystem;}
		}

		#endregion Properties
		
		#region Singleton
				
		/// <summary>
		/// Protected constructor.
		/// <note>When deriving from this class, please register your APIs in the constructor..</note>
		/// </summary>
		protected LoginManager()
		{
			// Declare the basic API types for which any application requires implementations 
			Type[] types = new Type[2];
			types[0] = typeof(IAdministrationAPI);
			types[1] = typeof(IEventsAPI);
			
			// Register to the event fired by the DVTel system upon logout
			DvtelSystemsManagerProvider.Instance.DvtelSystemsManager.DvtelSystemWasLoggedOut += new DvtelSystemWasLoggedOutHandler(OnDvtelSystemWasLoggedOut);
		}
		
		/// <summary>
		/// Gets the only instance of this class.
		/// </summary>
		public static LoginManager Instance
		{
			get
			{
				lock (ms_Monitor)
				{
					if (ms_LoginManager == null)
					{
						ms_LoginManager = new LoginManager();
					}
					return ms_LoginManager;
				}
			}
		}		
		
		#endregion Singleton
		
		#region Public Methods
		
		/// <summary>
		/// Loads the required assemblies into the SDK application domain.
		/// <note> 
		/// Any SDK application needs to use DVTel assemblies. Since the concrete 
		/// assemblies are not referenced directly, this method allows to load them. 
		/// Must be called before any usage of DVTel code or entities.
		/// </note> 
		/// </summary>
		public static void InitDVTelSDK()
		{
			InitDVTelSDK(false);
		}

	    private static bool? ms_isPartOfSampleBrowser = null;

        /// <summary>
        /// Loads the required assemblies into the SDK application domain.
        /// <note> 
        /// Any SDK application needs to use DVTel assemblies. Since the concrete 
        /// assemblies are not referenced directly, this method allows to load them. 
        /// Must be called before any usage of DVTel code or entities.
        /// </note> 
        /// </summary>
        public static void InitDVTelSDK(bool isPartOfSampleBrowser)
        {
            if (ms_isPartOfSampleBrowser == null)
            {
                DVTelInit.InitApplication();
                ms_isPartOfSampleBrowser = isPartOfSampleBrowser;
            }
        }		

		
		/// <summary>
		/// Performs the actual login to the DVTel system with the specified parameters.
		/// </summary>
		/// <param name="stDirectory">The hostname or IP of the Directory server.</param>
		/// <param name="stUsername">The username to login with.</param>
		/// <param name="stPassword">The password to login with.</param>
		public void Login(string stDirectory,string stUsername, string stPassword)
		{		
			try
			{
                // check if login was already done for this server with these credentials
                // note that we do not handle a situation of multi federation login, thus it 
                // will always return the last logged in server
                if (DvtelSystemsManagerProvider.Instance.DvtelSystemsManager.IsDvtelSystemLoggedIn(stDirectory,stUsername,stPassword))
                {
                    if (LoggedIn != null)
                    {
                        LoggedIn(m_stDirectoryServer, null);
                    }
                    return;
                }

				// Set login details members
				m_stDirectoryServer = stDirectory;
				m_stUsername = stUsername;
				m_stPassword = stPassword;

				// Register to the event fired by the DVTel system upon login
				DvtelSystemsManagerProvider.Instance.DvtelSystemsManager.DvtelSystemWasLoggedIn += new DvtelSystemWasLoggedInHandler(OnDvtelSystemWasLoggedIn);

//				IDvtelSdkApplicationEntity loginEntity = (IDvtelSdkApplicationEntity)DvtelSystemsManagerProvider.Instance.DvtelSystemsManager.CreateHandshakeOnlyEntity(typeof(IDvtelSdkApplicationEntity));
//				loginEntity.Id = Guid.NewGuid();

				// Perform the actual login itself
				DvtelSystemsManagerProvider.Instance.DvtelSystemsManager.Login(stDirectory,stUsername,stPassword, new LoginCacheRequest(false/*WaitForCacheInitialized*/));
			}
			catch (Exception ex)
			{
				if(ex.GetType().IsAssignableFrom(typeof(IDVTelException)))
				{
					System.Console.WriteLine(((IDVTelException)ex).UserMessage);
				}
				else
				{
					System.Console.WriteLine(string.Format("Login to Directory has failed! {0}",ex.Message));
				}

				// Fire the local event to any listeners with parameters indicating failure 
				if (LoggedIn != null)
				{
					LoggedIn(null, ex);
				}
			}		
		}		

		/// <summary>
		/// Performs logout from the DVTel system.
		/// </summary>
		public void Logoff()
		{
			if (m_DvtelSystem != null)
			{
				DvtelSystemsManagerProvider.Instance.DvtelSystemsManager.Logout(m_DvtelSystem);
			}
		}		
		
		/// <summary>
		/// Closes the DVTel application.
		/// </summary>
		public void Shutdown()
		{
            if (ms_isPartOfSampleBrowser == false)
            {
                DvtelSystemsManagerProvider.Instance.DvtelSystemsManager.Shutdown();
            }
		}	
		
		#endregion Public Methods
		
		#region Event Handlers
		
		/// <summary>
		/// Callback for handling the event fired by the DVTel system upon logout. 
		/// </summary>
		private void OnDvtelSystemWasLoggedOut(IDvtelSystemId dvtelSystem)
		{
			// Fire the local event
			if (LoggedOut != null)
			{
				LoggedOut(dvtelSystem);
			}
		}
		
		/// <summary>
		/// Callback for handling the event fired by the DVTel system upon login. 
		/// </summary>
		/// <param name="dvtelSystem">The reference to the DVTel system.</param>
		private void OnDvtelSystemWasLoggedIn(IDvtelSystemId dvtelSystem)
		{
			// Unregister to the event fired by the DVTel system upon login,
			// since we are already handling it and we don't want to get it twice.
			DvtelSystemsManagerProvider.Instance.DvtelSystemsManager.DvtelSystemWasLoggedIn -= new DvtelSystemWasLoggedInHandler(OnDvtelSystemWasLoggedIn);
			
			// Set the member 
			m_DvtelSystem = dvtelSystem;

			// Fire the local event to any listeners with parameters indicating success 
			if (LoggedIn != null)
			{
				LoggedIn(m_stDirectoryServer, null);
			}
		}
		
		#endregion Event Handlers
		
	}
}
