﻿using DVTel.API;
using DVTel.API.Common;
using DVTel.API.Entities.Physical;
using DVTel.API.Entities.Physical.Enums;
using DVTel.API.Entities.SystemObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace DIMS.DVTelLibrary
{
    public class DVTelProcess
    {
        //C:\Windows\Microsoft.NET\assembly\GAC_MSIL\DVTel.Common.AssemblyLoader\v4.0_7.0.0.0__921d584f8084f93e\DVTel.Common.AssemblyLoader.dll
        /// <summary>
        /// Describes the method to be invoked in order to login to a DVTel system.
        /// </summary>
        public delegate void loginDelegate(string hostname, string user, string password);
        public CacheCallback AllEntitiesCachedEvent;
        /// <summary>
        /// just to make sure we are in GUI thread
        /// </summary>
        private delegate void ShowLoginFormDelegate();
        /// <summary>
        /// this is the maximum amount of results the archiver will return when we run a query
        /// </summary>
        public static int MAX_QUERY_RESULTS = 150;

        /// <summary>
        /// Types of entities that will be displayed in the tree
        /// </summary>
        private readonly Type[] m_TypesOfInterest = { typeof(IConfigurationEntity) };

        private IReadonlyEntitiesCollection<IConfigurationEntity> m_Entities;
        private readonly Hashtable m_hashResults;   // key = IDisplayableEntity, value = ListViewItem

        private readonly System.Timers.Timer m_timer;

        private readonly string m_stTitle;
        /// <summary>
		/// The method to invoke in order to perform login.
		/// </summary>
		//private loginDelegate m_loginDelegate;
        private string s_hostName;
        private string s_userName;
        private string s_password;
        string c_name = "Roof Helipad 1 .238";
        AxiomQueryImpl objAxiomQueryImpl;
        IConfigurationEntity[] p_sceneEntity;

        public DVTelProcess(string hostname, string username, string password,string cameraName)
        {
            s_hostName = hostname;
            s_userName = username;
            s_password = password;
            c_name = cameraName;
            objAxiomQueryImpl = new AxiomQueryImpl();
            
        }
        
        public List<object> ConnectAndValidate(string startTime, string endTime)
        {
            List<object> values = new List<object>();
            try
            {
                LoginManager.InitDVTelSDK();
                LoginManager.Instance.Login(s_hostName, s_userName, s_password);
                IAdministrationAPI adminAPI = LoginManager.Instance.DvtelSystem.AdministrationAPI;
                adminAPI.AddCachedEntityType(typeof(IConfigurationEntity), null);
                System.Threading.Thread.Sleep(5000);
                try
                {
                    m_Entities = adminAPI.GetCachedEntitiesOfTypes(m_TypesOfInterest, null);
                }
                catch (Exception ex)
                {
                    adminAPI.AddCachedEntityType(typeof(IConfigurationEntity), null);
                    m_Entities = adminAPI.GetCachedEntitiesOfTypes(m_TypesOfInterest, null);
                }
                IDisplayableEntity[] obj = AddClipEntitiesToList(m_Entities);
                values.Add(obj.Length > 0);
            }
            catch (Exception ex)
            {
                values.Add(ex);
                // return ex;
            }
            return values;
        }
      
        public List<object> QueryDVTelClips()
        {
            List<object> values = new List<object>();
            try
            {
                LoginManager.InitDVTelSDK();
                loginDelegate m_loginDelegate = new loginDelegate(Initialize);
                LoginManager.Instance.Login(s_hostName, s_userName, s_password);
                IAdministrationAPI adminAPI = LoginManager.Instance.DvtelSystem.AdministrationAPI;
                adminAPI.AddCachedEntityType(typeof(IConfigurationEntity), null);

                m_Entities = adminAPI.GetCachedEntitiesOfTypes(m_TypesOfInterest, null);
                IDisplayableEntity[] obj = AddClipEntitiesToList(m_Entities);
                values.Add(obj.Length > 0);
            }
            catch (Exception ex)
            {
                values.Add(ex);
               // return ex;
            }
            return values;
        }
        public void logOff()
        {
            //Logout();
        }
        
        /// <summary>
        /// Starts an asynchronous login attempt to the Directory server
        /// </summary>
        /// <param name="hostname">The server name to which we try to login</param>
        /// <param name="password"></param>
        /// <param name="user"></param>
        private void Initialize(string hostname, string user, string password)
        {
            //UpdateStatus("Connecting");
            (new loginDelegate(AsynInitialize)).BeginInvoke(hostname, user, password, null, null);
            // Progressing = true;
        }
        /// <summary>
        /// Uses the LoginManager from SDK.Samples.Base to perform a login attempt.
        /// </summary>
        /// <param name="hostname">The hostname of the Directory server to which we would like to login</param>
        /// <param name="password"></param>
        /// <param name="user"></param>
        private void AsynInitialize(string hostname, string user, string password)
        {
            try
            {

                LoginManager.Instance.LoggedIn += LoggedIn;
                LoginManager.Instance.Login(hostname, user, password);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        /// <summary>
        /// Called by the Login manager when we are logged in.
        /// Adds all relevant entities to the cache
        /// </summary>
        /// <param name="stLoggedInDirectory"></param>
        /// <param name="exFailure"></param>
        private void LoggedIn(string stLoggedInDirectory, Exception exFailure)
        {
            LoginManager.Instance.LoggedIn -= LoggedIn;
            // success
            if (exFailure == null)
            {
                IAdministrationAPI adminAPI = LoginManager.Instance.DvtelSystem.AdministrationAPI;
                adminAPI.AddCachedEntityType(typeof(IConfigurationEntity), null);
                //Here we cache all trees - physical, logical and system settings.
               // adminAPI.CacheAllTrees(AllEntitiesCached);

                //IAdministrationAPI adminAPI = LoginManager.Instance.DvtelSystem.AdministrationAPI;
                m_Entities = adminAPI.GetCachedEntitiesOfTypes(m_TypesOfInterest, null);
                AddEntitiesToList(m_Entities);
            }
            // failure - try again
            else
            {
                /// Progressing = false;
                if (exFailure.GetType().IsAssignableFrom(typeof(IDVTelException)))
                {
                    LoginFailed(((IDVTelException)exFailure).UserMessage);
                }
                else
                {
                    LoginFailed(string.Format("Login to Directory has failed! {0}", exFailure.Message));
                }
            }
        }

        delegate void LoginFailedCallback(string errorMessage);
        private void LoginFailed(string errorMessage)
        {
            LoginFailedCallback obj = new LoginFailedCallback(LoginFailed);//, new object[] { errorMessage });
            obj.Invoke(errorMessage);

            //if (InvokeRequired)
            //{
            //    Invoke(new LoginFailedCallback(LoginFailed), new object[] { errorMessage });
            //}
            //else
            //{
            //  //  MessageBox.Show(this, errorMessage);

            //}
        }

        /// <summary>
        /// Called when all entities are cached and ready to use
        /// Asynchronously retrieves the entities from the Directory
        /// </summary>
        private void AllEntitiesCached(IDvtelSystemId sender, Exception success, ref bool shouldRetry)
        {
            if (success == null)
            {
                Thread  myThread1 = new Thread(new ThreadStart(SetNavigationTree));
                myThread1.Start();
                //Invoke(new ThreadStart(SetNavigationTree), null);

            }
            else
            {
                Console.WriteLine(success.ToString());
                shouldRetry = true;
            }
            if (AllEntitiesCachedEvent != null)
            {
                AllEntitiesCachedEvent(sender, success, ref shouldRetry);
            }
        }

        /// <summary>
        /// Retrieves all entities from the Directory.
        /// Registers to receive any changes in the entities.
        /// </summary>
        private void SetNavigationTree()
        {
            // m_tabControl.Enabled = true;
            try
            {
                IAdministrationAPI adminAPI = LoginManager.Instance.DvtelSystem.AdministrationAPI;
                m_Entities = adminAPI.GetCachedEntitiesOfTypes(m_TypesOfInterest, null);
                AddEntitiesToList(m_Entities);
            }
            catch (Exception ex)
            {
                Console.WriteLine("SetNavigationTree: " + ex.Message, ex);
            }
        }


        /// <summary>
        /// Adds a collection of entities to the tree
        /// </summary>
        /// <param name="entities">Generic collection of entities</param>
        private void AddEntitiesToList(IReadonlyEntitiesCollection<IConfigurationEntity> entities)
        {
            p_sceneEntity = new IConfigurationEntity[1]; 
            foreach (IConfigurationEntity scene in entities.Values)
            {
                if (scene.Name == c_name)// "TEST Bosch 192.168.1.223 () - 177")
                {
                    p_sceneEntity[0] = (IDisplayableEntity)scene;
                    break;
                }
                // i++;
            }
            IDisplayableEntity[] results = PerformQuery(p_sceneEntity);
            LoginManager.Instance.Logoff();

            //ExportClip(results);

        }
        private IDisplayableEntity[] AddClipEntitiesToList(IReadonlyEntitiesCollection<IConfigurationEntity> entities)
        {
            p_sceneEntity = new IConfigurationEntity[1];  
            foreach (IConfigurationEntity scene in entities.Values)
            {

                if (scene.Name == c_name)// "TEST Bosch 192.168.1.223 () - 177")
                {
                    p_sceneEntity[0] = (IDisplayableEntity)scene;
                    break;
                }
            }
            IDisplayableEntity[] results = PerformQuery(p_sceneEntity);
            LoginManager.Instance.Logoff();
            return results;
        }
        private IDisplayableEntity[] PerformQuery(IConfigurationEntity[] scenes)
        {
            IReadWriteEntitiesCollection<IRecordableSceneEntity> recordableScenes;
            if (scenes != null)
            {
                recordableScenes = DVTelObjectsFactory.Instance.CreateObject<IReadWriteEntitiesCollection<IRecordableSceneEntity>>();
                foreach (IConfigurationEntity entity in scenes)
                {
                    if (!(entity is IRecordableSceneEntity))
                    {
                        // TODO: add status report
                        // return null;
                        return new IDisplayableEntity[0];
                    }
                    recordableScenes.Add((IRecordableSceneEntity)entity);
                }
            }
            else
            {
                // Log saying "No entities exist. Aborting query"
                //return null;
                return new IDisplayableEntity[0];
            }
            IDisplayableEntity[] results = null;
            IReadonlyEntitiesCollection<IClipEntity> clips = AxiomQueryImpl.QueryClips(recordableScenes.AsReadOnly(), Reason, ProtectionLevel, From, To, (Initiators == null ? null : Initiators.AsReadOnly()), MAX_QUERY_RESULTS);
            if (clips != null)
            {
                results = new IClipEntity[clips.Count];
                clips.Values.CopyTo((IClipEntity[])results, 0);
            }
            else
                results = new IClipEntity[clips.Count];
            return results;

        }

         

        //initiators
        ///// Initiator,All initiators,AegisWksnt1,Metro ITSv
        ///// metro,System Administrator, MCM Test,General Services
        /////Security,metroadmin,Bahram Chaudhry
        /// <summary>
        /// None,Schedule,Alm,Manual,Event,Custom,Internal
        /// MotionDetection,Redundant,Edge,All=479
        /// </summary>
        int p_reason = 479;
        public ArchivingReason Reason
        {
            get { return (ArchivingReason)p_reason; }
        }

        /// <summary>
        /// Both =3
        ///Protected=1
        ///NotProtected=2
        /// </summary>
        int p_protectionLevel = 3;
        public ProtectionFilterEnum ProtectionLevel
        {
            get { return (ProtectionFilterEnum)p_protectionLevel; }
        }
        public IReadWriteEntitiesCollection<IConfigurationEntity> Initiators
        {
            get
            {
                /*
                IReadWriteEntitiesCollection<IConfigurationEntity> initiators = null;
                if (m_hashInitiators.Contains(inititatorName))
                {
                    IUserEntity initiator = (IUserEntity)m_hashInitiators[inititatorName];
                    initiators = DVTelObjectsFactory.Instance.CreateObject<IReadWriteEntitiesCollection<IConfigurationEntity>>();
                    initiators.Add(initiator);
                }

                */
                return null;
            }
            //set { p_protectionLevel = value; }
        }
        /// <summary>
		/// The time to start the query from.
		/// </summary>
		protected DateTime From
        {
            get
            {
                return DateTime.Now.AddMonths(-2);
            }
        }

        /// <summary>
        /// The time to end the query from.
        /// </summary>
        protected DateTime To
        {
            get
            {
                return DateTime.Now.AddMonths(3);
            }
        }
    }
    public class AxiomQueryImpl
    {
        /// <summary>
        /// Query for archived clips using the Recording API
        /// </summary>
        /// <param name="scenes">the scenes participating in the query. Value can not be null</param>
        /// <param name="reasons">the reasons that the clips were recorded. Null = any</param>
        /// <param name="protectionFilter">the protection level of the desired clips: Locked / Unlocked / Both</param>
        /// <param name="from">time to start searching from</param>
        /// <param name="to">time to end searching at</param>
        /// <param name="initiators">The entities who initiated the creation of the desired clips. Null = any</param>
        /// <param name="maxResults">maximum number of results to return</param>
        /// <returns>an array of clips or null</returns>
        public static IReadonlyEntitiesCollection<IClipEntity> QueryClips(
            IReadonlyEntitiesCollection<IRecordableSceneEntity> scenes,
            ArchivingReason reasons,
            ProtectionFilterEnum protectionFilter,
            DateTime from,
            DateTime to,
            IReadonlyEntitiesCollection<IConfigurationEntity> initiators,
            int maxResults)
        {
            IReadonlyEntitiesCollection<IClipEntity> results = null;
            if (scenes != null && scenes.Count > 0)
            {
                try
                {
                    // Since we only demonstrate a single federation system, we can
                    // assume that all selected entities hold the same IDvtelSystemId.
                    // so we will simply get the IDvtelSystemId object from one of the scenes.
                    IDvtelSystemId dvtelSystem = scenes.Values[0].DvtelSystem;

                    // get a Recording API that belongs to our specific federation (system)
                    IRecordingAPI recordingAPI = dvtelSystem.GetAPI<IRecordingAPI>();

                    // create a filter for the query
                    QueryClipsFilter filter = new QueryClipsFilter(scenes, from, to, maxResults);
                    filter.ArchivingReasons = reasons;
                    filter.Initiators = initiators;
                    filter.ProtectionFilter = protectionFilter;

                    bool cropped;

                    // perform the query
                    results = recordingAPI.QueryClips(filter, out cropped);

                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed querying clips: " + e);
                }
            }
            return results;
        }
    }
}
