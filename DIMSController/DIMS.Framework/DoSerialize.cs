﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DIMS.Framework
{
    public static partial class Helper
    {
       
       
        public static void SerializeToXml<T>(T obj, string fileName)
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            var ser = new XmlSerializer(typeof(T));
            //Create a FileStream object connected to the target file   
            var fileStream = new FileStream(fileName, FileMode.Create);
            ser.Serialize(fileStream, obj, ns);
            fileStream.Close();

        }
       
        public static string SerializeToXml<T>(T obj)
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var output = new StringWriter(new StringBuilder());
            var ser = new XmlSerializer(typeof(T));
            ser.Serialize(output, obj, ns);
            return output.ToString();

        }


    }
}
