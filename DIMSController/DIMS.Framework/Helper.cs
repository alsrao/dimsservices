﻿using System;
using System.IO;
using System.Text;
using System.Web.Configuration;
using System.Web;


namespace DIMS.Framework
{
    public static partial class Helper
    {
        public static string GetSetting(string setting)
        {
            return WebConfigurationManager.AppSettings[setting] ?? "";
        }

        public static void AddtoLogFile(string message)
        {
            var filename = @"\\Log_" + DateTime.Now.ToString("MM-dd-yyyy") + ".txt";


            //var value = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Logs\\";
            var value = System.Web.Hosting.HostingEnvironment.MapPath("~") + "\\Logs";


            if (!Directory.Exists(value))
            {
                Directory.CreateDirectory(value);
            }

            var filepath = value + filename;

            if (!File.Exists(filepath))
            {
                var writer = File.CreateText(filepath);
                writer.Close();
            }

            using (var writer = new StreamWriter(filepath, true))
            {
                var sb = new StringBuilder();
                sb.Append(Environment.NewLine + "================================================================");
                sb.Append(Environment.NewLine + message);
                writer.WriteLine(sb.ToString());
            }

        }


        public static void AddTrackstoLogFile(string message)
        {
            var filename = @"\Tracks_" + DateTime.Now.ToString("MM-dd-yyyy") + ".txt";


            var value = System.Web.Hosting.HostingEnvironment.MapPath("~") + "\\Logs\\"; //Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Logs\\";


            if (!Directory.Exists(value))
            {
                Directory.CreateDirectory(value);
            }

            var filepath = value + filename;
            /*if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }
            var writer1 = File.CreateText(filepath);
            writer1.Close();*/

            if (!File.Exists(filepath))
            {
                var writer = File.CreateText(filepath);
                writer.Close();
            }

            using (var writer = new StreamWriter(filepath, true))
            {
                var sb = new StringBuilder();
                sb.Append(Environment.NewLine + message);
                writer.WriteLine(sb.ToString());
            }

        }

        public static void AddRecordstoLogFile(string message)
        {
            var filename = @"\Records_" + DateTime.Now.ToString("MM-dd-yyyy") + ".txt";


            var value = System.Web.Hosting.HostingEnvironment.MapPath("~") + "\\Logs\\"; //Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Logs\\";


            if (!Directory.Exists(value))
            {
                Directory.CreateDirectory(value);
            }

            var filepath = value + filename;
            /*if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }
            var writer1 = File.CreateText(filepath);
            writer1.Close();*/

            if (!File.Exists(filepath))
            {
                var writer = File.CreateText(filepath);
                writer.Close();
            }

            using (var writer = new StreamWriter(filepath, true))
            {
                var sb = new StringBuilder();
                sb.Append(Environment.NewLine + message);
                writer.WriteLine(sb.ToString());
            }

        }
    }
}

