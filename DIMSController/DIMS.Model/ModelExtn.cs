﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DIMS.Model
{
    [DataContract]
    public class ClsClipRequest
    {
        [DataMember]
        public string Type { get; set; }

        //[DataMember]
        //public string SiteName { get; set; }

        //[DataMember]
        //public List<string> SiteNameList { get; set; }

        [DataMember]
        public string StartTime { get; set; }

        [DataMember]
        public string EndTime { get; set; }

        [DataMember]
        public string Password { get; set; }

        //[DataMember]
        //public string SegLen { get; set; }

        //public bool HqEnabled { get; set; }

        [DataMember]
        public int PreTime { get; set; }

        [DataMember]
        public int PostTime { get; set; }

        //[DataMember]
        //public List<bool> Camera { get; set; }

        //[DataMember]
        //public object Geofence { get; set; }

        [DataMember]
        public int Server_Id { get; set; }

        [DataMember]
        public int Location_Id { get; set; }

        [DataMember]
        public string System_Id { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public Tags Tags { get; set; }

        [DataMember]
        public string Division { get; set; }

        [DataMember]
        public string BusNumber { get; set; }

        [DataMember]
        public string WorkOrderId { get; set; }

        public string TagsJson { get; set; }

        [DataMember]
        public FACILITIESFIXED FacilitiesFixed { get; set; }

        [DataMember]
        public string IncidentDescription { get; set; }

        [DataMember]
        public bool M3 { get; set; }

        [DataMember]
        public string RailCarNumber { get; set; }

    }

    [DataContract]
    public class Tags
    {
        [DataMember]
        public List<Tag> Information { get; set; }

        //[DataMember]
        //public GPS Gps { get; set; }

    }

    [DataContract]
    public class Tag
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class GPS
    {
        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }
    }

    [DataContract]
    public class FACILITIESFIXED
    {
        [DataMember]
        public string Camera { get; set; }

        [DataMember]
        public string Encoder { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string NetworkAddress { get; set; }

        [DataMember]
        public string Number { get; set; }


        //[DataMember]
        //public string RailCarNumber { get; set; }
    }

    [DataContract]
    public class ClsClipResponse
    {

        [DataMember]
        public string Status { get; set; }


        [DataMember]
        public string WorkOrderId { get; set; }

        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public class ClipResponse
    {
        [DataMember]
        public int ClipRequestID { get; set; }

        [DataMember]
        public string status { get; set; }

        [DataMember]
        public string SiteName { get; set; }
    }

    [DataContract]
    public class ClsLstClipStatus : ClsClipResponse
    {
        [DataMember]
        public List<ClipStatus> LstClipStatus { get; set; }

    }

    [DataContract]
    public class ClipStatus
    {
        [DataMember]
        public int ClipID { get; set; }

        [DataMember]
        public object GCRID { get; set; }

        [DataMember]
        public int ClipRequestID { get; set; }

        [DataMember]
        public string SiteName { get; set; }

        [DataMember]
        public string Status { get; set; }
    }

    [DataContract]
    public class ClsClipStatus
    {
        [DataMember]
        public string WorkOrderId { get; set; }

        //[DataMember]
        //public string SiteName { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Description { get; set; }

    }


    [DataContract]
    public class ClsClipMetadata : ClsClipResponse
    {
        [DataMember]
        public string ClipName { get; set; }

        
        [DataMember]
        public string ClipStartTime { get; set; }

        [DataMember]
        public long ClipLengthInSeconds { get; set; }

        [DataMember]
        public long FileSize { get; set; }

        //[DataMember]
        //public string ClipType { get; set; }
    }

    public class ClsClipMetadataInternal : ClsClipResponse
    {
        [DataMember]
        public string ClipName { get; set; }

        [DataMember]
        public int Cameras { get; set; }

        [DataMember]
        public string ClipStartTime { get; set; }

        [DataMember]
        public int ClipLengthInSeconds { get; set; }

        [DataMember]
        public int FileSize { get; set; }

        [DataMember]
        public string ClipType { get; set; }
    }

    [DataContract]
    public class NetVuArchiveTasks
    {
        [DataMember]
        public Result result { get; set; }

        [DataMember]
        public Error error { get; set; }
    }

    [DataContract]
    public class Result
    {

        [DataMember]
        public string resultMessage { get; set; }

        [DataMember]
        public string resultDetails { get; set; }

    }

    [DataContract]
    public class Error
    {
        [DataMember]
        public string errorMessage { get; set; }

        [DataMember]
        public string errorDetails { get; set; }

    }

    [DataContract]
    public class NetVuMediaData
    {
        [DataMember]
        public List<ArchiveMediaFile> archive_media_file { get; set; }
    }

    [DataContract]
    public class ArchiveMediaFile
    {

        [DataMember]
        public int media_file_id { get; set; }

        [DataMember]
        public int media_location_id { get; set; }

        [DataMember]
        public string media_file_content_type { get; set; }

        [DataMember]
        public string media_file_path { get; set; }

        [DataMember]
        public string media_file_channels { get; set; }

        [DataMember]
        public string media_file_start { get; set; }

        [DataMember]
        public string media_file_end { get; set; }

        [DataMember]
        public int media_file_time_offset { get; set; }

        [DataMember]
        public int media_file_video_expiry { get; set; }

        [DataMember]
        public int media_file_remove_date { get; set; }

        [DataMember]
        public string media_file_status { get; set; }

        [DataMember]
        public string media_file_source { get; set; }

        [DataMember]
        public string media_file_added { get; set; }

        [DataMember]
        public string media_file_site_id { get; set; }

        [DataMember]
        public string media_file_notes { get; set; }

        [DataMember]
        public string media_file_mirror { get; set; }

        [DataMember]
        public string own_media { get; set; }

        [DataMember]
        public int server_id { get; set; }

        [DataMember]
        public string media_file_sys_id { get; set; }
    }

    [DataContract]
    public class NetVuArchiveLocations
    {
        [DataMember]
        public List<ArchiveLocation> archiveLocation { get; set;}
    }

    [DataContract]
    public class ArchiveLocation
    {
        [DataMember]
        public int media_location_id { get; set; }

        [DataMember]
        public string media_location_sys_id { get; set; }

        [DataMember]
        public string media_location_type { get; set; }

        [DataMember]
        public string media_location_name { get; set; }

        [DataMember]
        public string media_location_path { get; set; }

        [DataMember]
        public Distributed_info distributed_info { get; set; }
    }

    [DataContract]
    public class Distributed_info
    {
        [DataMember]
        public string distributed_config_name { get; set; }

        [DataMember]
        public string distributed_config_address { get; set; }

        [DataMember]
        public string own_location { get; set; }
    }

    [DataContract]
    public class NetVuAssets
    {
        [DataMember]
        public List<AssetServer> assetServer { get; set; }
        [DataMember]
        public List<AssetSystem> assetSystem { get; set; }
    }

    [DataContract]
    public class AssetServer
    {
        [DataMember]
        public int server_id { get; set; }

        [DataMember]
        public string server_name { get; set; }

        [DataMember]
        public string server_address { get; set; }

        [DataMember]
        public string server_address_secondary { get; set; }

        [DataMember]
        public int server_port { get; set; }

        [DataMember]
        public string server_https { get; set; }

        [DataMember]
        public string server_created { get; set; }

        [DataMember]
        public string server_expired { get; set; }

        [DataMember]
        public string server_cammask { get; set; }

        [DataMember]
        public string server_dynamic_ip { get; set; }

        [DataMember]
        public string system_id { get; set; }

        [DataMember]
        public string server_max_concurrent { get; set; }

        [DataMember]
        public int server_timeout { get; set; }

        [DataMember]
        public long server_par_size { get; set; }

        [DataMember]
        public string server_generation { get; set; }

        [DataMember]
        public string server_mobile { get; set; }

        [DataMember]
        public string server_mobile_ref { get; set; }

        [DataMember]
        public string server_notes { get; set; }

        [DataMember]
        public string server_par_type { get; set; }

        [DataMember]
        public string server_maintenance { get; set; }

        [DataMember]
        public string server_type { get; set; }

        [DataMember]
        public string server_filemode { get; set; }

        [DataMember]
        public string _sys_id { get; set; }

        [DataMember]
        public string _uts { get; set; }
    }

    [DataContract]
    public class AssetSystem
    {
        [DataMember]
        public string system_id { get; set; }

        [DataMember]
        public string system_name { get; set; }

        [DataMember]
        public string _sys_id { get; set; }
    }

    public class ClsLstCameraTrackAndDateTime
    {
        public List<CameraTrackAndDateTime> LstCameraTrackAndDateTime { get; set; }
        
    }

    public class CameraTrackAndDateTime
    {
        public string TrackId { get; set; }
       public DateTime StartDateTime  { get; set; }

        public DateTime ENdDateTime { get; set; }


    }

    [DataContract]
    public class M3_Dvr_Interface
    {
        [DataMember]
        public string record_Status { get; set; }

        [DataMember]
        public string Transaction_type { get; set; }

        [DataMember]
        public string Incident_Type_code { get; set; }

        [DataMember]
        public string Problem_code { get; set; }

        [DataMember]
        public string Equip_code { get; set; }

        [DataMember]
        public string Division { get; set; }

        [DataMember]
        public string Incident_desc { get; set; }

        [DataMember]
        public string Rail_Bus_Ind { get; set; }

        [DataMember]
        public string Dims_Id { get; set; }

        [DataMember]
        public string Request_Created_Date { get; set; }
    }
}
