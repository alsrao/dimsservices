﻿using System.Web.Http;
using DIMS.Model;
using DIMS.Business;
using System;
using DIMS.Data;
using DIMS.Framework;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace DIMSServices.Controllers
{
    public class ClipController : ApiController
    {

        private readonly ApolloBusiness _businessApollo = new ApolloBusiness();

        private readonly AMSBusiness _businessAMS = new AMSBusiness();

        private readonly BoschBusiness _businessBosch = new BoschBusiness();
        private readonly DVTelBusiness _businessDVTel = new DVTelBusiness();

        ClipData _clipData = new ClipData();

        [HttpPost]
        public ClsClipResponse MakeRequest(ClsClipRequest clipRequest)
        { 
            if (clipRequest != null)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipController.cs  MakeRequest:: " + Helper.SerializeToXml(clipRequest));
                if (clipRequest.Type != null)
                {
                    if (clipRequest.Type == "TRAINS-FIXED")
                    {
                        if ((String.IsNullOrEmpty(clipRequest.RailCarNumber)) || (String.IsNullOrEmpty(clipRequest.StartTime)) || (String.IsNullOrEmpty(clipRequest.EndTime)) || (String.IsNullOrEmpty(clipRequest.Division)))
                        {
                            return new ClsClipResponse
                            {
                                Status = "FAILED",
                                WorkOrderId = null,
                                Description = "Invalid input. Mandatory fields Division or Rail Car Number or StartTime or EndTime are missing."
                            };
                        }
                        return _businessApollo.ClipRequest(clipRequest);
                    }
                    else if (clipRequest.Type == "BUS-NOTFIXED")
                    {
                        if ((String.IsNullOrEmpty(clipRequest.BusNumber)) || (String.IsNullOrEmpty(clipRequest.StartTime)) || (String.IsNullOrEmpty(clipRequest.EndTime)))
                        {
                            return new ClsClipResponse
                            {
                                Status = "FAILED",
                                WorkOrderId = null,
                                Description = "Invalid input. Mandatory fields Bus Number or StartTime or EndTime are missing."
                            };
                        }
                        return _businessAMS.ClipRequest(clipRequest);
                    }
                    else if (clipRequest.Type == "FACILITIES–FIXED")
                    {
                        if ((clipRequest.FacilitiesFixed == null) || (String.IsNullOrEmpty(clipRequest.StartTime)) || (String.IsNullOrEmpty(clipRequest.EndTime)))
                        {
                            return new ClsClipResponse
                            {
                                Status = "FAILED",
                                WorkOrderId = null,
                                Description = "Invalid input. Mandatory fields FacilitiesFixed or StartTime or EndTime are missing."
                            };
                        }

                        var facilitiesFixedData = Helper.SerializeToXml(clipRequest.FacilitiesFixed);

                        XmlSerializer xs = new XmlSerializer(typeof(FACILITIESFIXED));
                        FACILITIESFIXED facilitiesFixedobj = (FACILITIESFIXED)xs.Deserialize(new StringReader(facilitiesFixedData));

                        if (facilitiesFixedobj != null && (facilitiesFixedobj.Location == "null" || facilitiesFixedobj.Location == null))
                        {
                            return _businessDVTel.ClipRequest(clipRequest);
                        }
                        else
                        {
                            return _businessBosch.ClipRequest(clipRequest);
                        }
                    }
                    else
                    {
                        return new ClsClipResponse
                        {
                            Status = "FAILED",
                            WorkOrderId = null,
                            Description = "Invalid type provided. Use valid type from reference guide"
                        };
                    }
                }
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = null,
                    Description = "Invalid type provided. Use valid type from reference guide"
                };
            }
            return new ClsClipResponse
            {
                Status = "FAILED",
                WorkOrderId = null,
                Description = "Invalid input.Mandatory fields are missing."
            };
        }

        [HttpGet]
        public ClsClipStatus GetStatus(string workOrderId)
        {
            if (!String.IsNullOrEmpty(workOrderId))
            {
                var type = _clipData.GetRecord(workOrderId.ToString());
                if (type != null)
                {
                    if (type.Type == "TRAINS-FIXED")
                        return _businessApollo.ClipStatus(workOrderId);
                    else if (type.Type == "BUS-NOTFIXED")
                        return _businessAMS.ClipStatus(workOrderId);
                    else if (type.Type == "FACILITIES–FIXED")
                        return _businessBosch.ClipStatus(workOrderId);
                    else
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "FAILED",
                            Description = "Invalid type provided. Use valid type from reference guide"
                        };
                }
                else
                {
                    return new ClsClipStatus
                    {
                        WorkOrderId = workOrderId,
                        Status = "FAILED",
                        Description = "Invalid workOrderId"
                    };
                }
            }
            return new ClsClipStatus
            {
                WorkOrderId = workOrderId,
                Status = "FAILED",
                Description = "Invalid workOrderId"
            };

        }

        [HttpGet]
        public ClsClipMetadata GetMetadata(string workOrderId)
        {
            if (!String.IsNullOrEmpty(workOrderId))
            {
                var type = _clipData.GetRecord(workOrderId.ToString());
                if (type != null)
                {
                    if (type.Type == "TRAINS-FIXED")
                        return _businessApollo.ClipMetadata(workOrderId);
                    else if (type.Type == "BUS-NOTFIXED")
                        return _businessAMS.ClipMetadata(workOrderId);
                    else if (type.Type == "FACILITIES–FIXED")
                        return _businessBosch.ClipMetadata(workOrderId);
                    else
                        return new ClsClipMetadata
                        {
                            WorkOrderId = workOrderId,
                            Status = "FAILED",
                            Description = "Invalid type provided. Use valid type from reference guide"
                        };
                }
                else
                {
                    return new ClsClipMetadata
                    {
                        WorkOrderId = workOrderId,
                        Status = "FAILED",
                        Description = "Invalid workOrderId"
                    };
                }
            }
            return new ClsClipMetadata
            {
                WorkOrderId = workOrderId,
                Status = "FAILED",
                Description = "Invalid workOrderId"
            };

        }

        [HttpGet]
        public ClsClipResponse DownLoad(string workOrderId)
        {
            if (!String.IsNullOrEmpty(workOrderId))
            {
                var type = _clipData.GetRecord(workOrderId.ToString());
                if (type != null)
                {
                    if (type.Type == "TRAINS-FIXED")
                        return _businessApollo.ClipDownLoad(workOrderId);
                    else if (type.Type == "BUS-NOTFIXED")
                        return _businessAMS.ClipDownLoad(workOrderId);
                    else if (type.Type == "FACILITIES–FIXED")
                        return _businessBosch.ClipDownLoad(workOrderId);
                    else
                        return new ClsClipResponse
                        {
                            WorkOrderId = workOrderId,
                            Status = "FAILED",
                            Description = "Invalid type provided. Use valid type from reference guide"
                        };
                }
                else
                {
                    return new ClsClipResponse
                    {
                        WorkOrderId = workOrderId,
                        Status = "FAILED",
                        Description = "Invalid workOrderId"
                    };
                }
            }
            return new ClsClipResponse
            {
                WorkOrderId = workOrderId,
                Status = "FAILED",
                Description = "Invalid workOrderId"
            };
            
        }
    }
}
