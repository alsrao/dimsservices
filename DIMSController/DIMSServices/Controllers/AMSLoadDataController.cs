﻿using DIMS.Business;
using DIMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DIMSServices.Controllers
{
    public class AMSLoadDataController : ApiController
    {
        private readonly LoadDataBusiness _businessLoadData = new LoadDataBusiness();
        

        [HttpGet]
        public void LoadAMSServers()
        {
            _businessLoadData.LoadServers();
        }
         
        [HttpGet]
        public void LoadAMSMediaLocations()
        {
            _businessLoadData.LoadMediaLocations();
        }

        [HttpGet]
        public void LoadAMSSystems()
        {
            _businessLoadData.LoadSystems();
        }
    }
}
