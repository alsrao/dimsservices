﻿using Bosch.VideoSDK.Device;
using DIMS.Data;
using DIMS.Framework;
using DIMS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DIMS.Business
{
    public class BoschBusiness
    {
        ClipData _clipData = new ClipData();

        public enum Status { M3REQUEST = 7, CANCELLED = 6, DOWNLOADING = 5, AVAILABLE = 4, PENDING = 2, FAILED = 3 };

        public ClsClipResponse ClipRequest(ClsClipRequest clipRequest)
        {
            try
            {
                string workOrdId = clipRequest.WorkOrderId.ToString().Trim();
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  workOrderId:: " + workOrdId);
                if (!String.IsNullOrEmpty(workOrdId))
                {
                    var facilitiesFixedData = Helper.SerializeToXml(clipRequest.FacilitiesFixed);

                    XmlSerializer xs = new XmlSerializer(typeof(FACILITIESFIXED));
                    FACILITIESFIXED facilitiesFixedobj = (FACILITIESFIXED)xs.Deserialize(new StringReader(facilitiesFixedData));

                    if (String.IsNullOrEmpty(clipRequest.IncidentDescription) || String.IsNullOrEmpty(clipRequest.IncidentDescription.Trim()))
                        clipRequest.IncidentDescription = "No description available";

                    var workOrderId = _clipData.GetRecord(workOrdId);
                    if (workOrderId == null)
                    {
                        String boschLocation = null;
                        if (facilitiesFixedobj.Location.Contains("Division"))
                        {
                            boschLocation = facilitiesFixedobj.Location.Replace("Division", " ");
                            boschLocation = boschLocation.Trim();
                        }
                        else if (facilitiesFixedobj.Location.Contains("division"))
                        {
                            boschLocation = facilitiesFixedobj.Location.Replace("division", " ");
                            boschLocation = boschLocation.Trim();
                        }
                        else if (facilitiesFixedobj.Location.Contains("DIVISION"))
                        {
                            boschLocation = facilitiesFixedobj.Location.Replace("DIVISION", " ");
                            boschLocation = boschLocation.Trim();
                        }

                        String camEncoder = facilitiesFixedobj.Camera;
                        if(camEncoder != null && camEncoder.Length > 34)
                             camEncoder = camEncoder.Substring(0, 35);
                        //String camEncoder = facilitiesFixedobj.Camera + "/" + facilitiesFixedobj.Encoder;
                        //if m3 api call success - 6 , if failed staus -2

                        if (clipRequest.M3 == true)
                        {
                            using (var httpClient = new HttpClient())
                            {
                                httpClient.Timeout = TimeSpan.FromSeconds(60);
                                httpClient.DefaultRequestHeaders.Accept.Clear();
                                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                var reqdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");//clipRequest.CreatedOn.ToString("yyyy-MM-dd HH:mm:ss");
                                var m3DvrInterface = new M3_Dvr_Interface()
                                {
                                    record_Status = "R",
                                    Transaction_type = "NIN",
                                    Incident_Type_code = "DVR",
                                    Problem_code = "DVR8",
                                    Division = boschLocation,
                                    Equip_code = camEncoder,
                                    Incident_desc = clipRequest.IncidentDescription,
                                    Rail_Bus_Ind = "R",
                                    Dims_Id = clipRequest.WorkOrderId,
                                    Request_Created_Date = reqdate
                                };
                                
                                var json = JsonConvert.SerializeObject(m3DvrInterface);
                                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  M3 Request Json:: " + json);
                                var postData = new StringContent(json, Encoding.UTF8, "application/json");
                                var response = httpClient.PostAsync("http://apiservices.metro.net/M3DvrInterfacedata", postData).Result;
                                try
                                {
                                    response.EnsureSuccessStatusCode();
                                    if (response.IsSuccessStatusCode)
                                    {
                                        var responseData = _clipData.BoschClipRequestInsert(clipRequest, 0, 3, 7, clipRequest.M3, boschLocation);
                                        if (responseData != 0)
                                            return new ClsClipResponse
                                            {
                                                Status = "SUCCESS",
                                                WorkOrderId = workOrdId,
                                                Description = "Clip request placed successfully"
                                            };
                                        else
                                            return new ClsClipResponse
                                            {
                                                Status = "FAILED",
                                                WorkOrderId = null,
                                                Description = "Clip request can't be placed."
                                            };
                                    }
                                }
                                catch (Exception ex)
                                {
                                    return new ClsClipResponse
                                    {
                                        Status = "FAILED",
                                        WorkOrderId = null,
                                        Description = "Unable to make M3 Clip request."
                                    };
                                }
                            }
                            return new ClsClipResponse
                            {
                                Status = "SUCCESS",
                                WorkOrderId = workOrdId,
                                Description = "Clip request placed successfully"
                            };
                        }
                        else
                        {
                            var boschServerDetails = _clipData.GetBoschServerRecord(facilitiesFixedobj.Location.ToUpper());

                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  boschServerDetails:: " + boschServerDetails);
                            if (boschServerDetails == null)
                            {
                                return new ClsClipResponse
                                {
                                    Status = "FAILED",
                                    WorkOrderId = null,
                                    Description = "Invalid Location or location not registered"
                                };
                            }
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  boschServerDetails:: " + boschServerDetails.Location + "IP:: " + boschServerDetails.LocationAddress);
                            //var existRecord 
                            string tagInfoJson = null;
                            try
                            {
                                if (clipRequest.Tags != null)
                                {
                                    // New code starts
                                    var tags = clipRequest.Tags;
                                    var tagList = tags.Information;
                                    var toBeCheckedList = new List<string> { "AREA", "BRANDNAME", "SOURCE" };
                                    var list3 = tagList.Where(x => toBeCheckedList.Contains(x.Name.ToUpper())).Select(n => n.Name.ToLower()).ToList();
                                    if (!(list3.Count == 3))
                                    {
                                        var missedTags = string.Join(",", toBeCheckedList.Where(x => !list3.Contains(x.ToLower())).ToList());

                                        return new ClsClipResponse
                                        {
                                            Status = "FAILED",
                                            WorkOrderId = null,
                                            Description = "Required Tags are missing, " + missedTags
                                        };

                                    }
                                    // New Code End

                                    var stringwriter = new System.IO.StringWriter();
                                    var serializer = new XmlSerializer(clipRequest.Tags.GetType());
                                    serializer.Serialize(stringwriter, clipRequest.Tags);
                                    string tagsInfo = stringwriter.ToString();
                                    System.Diagnostics.Debug.WriteLine("tagsInfo : " + tagsInfo);

                                    tagsInfo.Trim();
                                    if (tagsInfo.Contains("<?xml version=\"1.0\" encoding=\"utf-16\"?>"))
                                    {
                                        tagsInfo = tagsInfo.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n", "");
                                        tagsInfo.Trim();
                                    }
                                    if (tagsInfo.Contains("<Tags xmlns:xsd=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema\""))
                                    {
                                        tagsInfo = tagsInfo.Replace("<Tags xmlns:xsd=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema\">\r\n ", "<Tags>");
                                        tagsInfo.Trim();
                                    }
                                    tagInfoJson = tagsInfo;
                                }
                            }
                            catch (Exception e)
                            {
                                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  " + e.Message);
                            }
                            clipRequest.TagsJson = tagInfoJson;

                            string boschLocationAddress = "axiom@" + boschServerDetails.LocationAddress;
                            BoschVRMProcess boschVRMProcess = new BoschVRMProcess();
                            boschVRMProcess.Connect(boschLocationAddress, clipRequest.StartTime, clipRequest.EndTime);
                            System.Threading.Thread.Sleep(5000);
                            DateTime sdt = Convert.ToDateTime(clipRequest.StartTime);
                            DateTime edt = Convert.ToDateTime(clipRequest.EndTime);
                            Helper.AddtoLogFile("Send M3 : " + clipRequest.M3 + " and Location : " + facilitiesFixedobj.Location);
                            var trackId = boschVRMProcess.ValidateTrack(facilitiesFixedobj.Camera.Trim().ToUpper(), facilitiesFixedobj.Encoder.Trim().ToUpper());
                            if (trackId != null)
                            {
                                var trackData = boschVRMProcess.ValidateRecordDateTime(trackId, sdt, edt);
                                if (trackData != null)
                                {
                                    clipRequest.Division = boschServerDetails.LocationAddress;
                                    clipRequest.BusNumber = (facilitiesFixedobj.Camera.Trim() + "/" + facilitiesFixedobj.Encoder.Trim()).ToUpper();
                                    var responseData = _clipData.BoschClipRequestInsert(clipRequest, Convert.ToInt32(trackId), 3, Convert.ToInt32(Status.PENDING), clipRequest.M3, boschLocation);
                                    if (responseData != 0)
                                        return new ClsClipResponse
                                        {
                                            Status = "SUCCESS",
                                            WorkOrderId = workOrdId,
                                            Description = "Clip request placed successfully"
                                        };
                                    else
                                        return new ClsClipResponse
                                        {
                                            Status = "FAILED",
                                            WorkOrderId = null,
                                            Description = "Clip request can't be placed."
                                        };
                                }
                                return new ClsClipResponse
                                {
                                    Status = "FAILED",
                                    WorkOrderId = null,
                                    Description = "Currently no clip found with provided time"
                                };
                            }
                            return new ClsClipResponse
                            {
                                Status = "FAILED",
                                WorkOrderId = null,
                                Description = "Location not found. Please use valid location and request again."
                            };
                        }

                    }
                    else
                    {
                        return new ClsClipResponse
                        {
                            Status = "FAILED",
                            WorkOrderId = null,
                            Description = "WorkOrderId already exist."
                        };
                    }
                }
                else
                {
                    return new ClsClipResponse
                    {
                        Status = "FAILED",
                        WorkOrderId = null,
                        Description = "Invalid WorkOrderId."
                    };
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  " + ex.Message);
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = null,
                    Description = "Invalid Input"
                };
            }
        }

        public ClsClipStatus ClipStatus(string workOrderId)
        {

            try
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  Clip Status :: " + workOrderId);
                var status = ProcessClipStatus(workOrderId);
                return status;
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  " + ex.Message);
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Server error. Try with valid WorkOrderId."
                };

            }
        }

        public ClsClipMetadata ClipMetadata(string workOrderId)
        {
            var clsClipMetadata = new ClsClipMetadata();

            try
            {
                var clipId = _clipData.GetClipId(workOrderId);
                if (clipId == 0)
                {
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "Invalid workOrderId";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }

                var clipRequestObj = _clipData.GetRecord(workOrderId);
                string clipPath = clipRequestObj.clipOutputFilePath;

                if (!Directory.Exists(clipPath))
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  GetClipMeta clipPath:: " + clipPath);
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "No downloaded clips available";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }

                var length = Directory.GetFiles(clipPath, "*", SearchOption.AllDirectories).Sum(t => (new FileInfo(t).Length));

                if (length > 0)
                {
                    clsClipMetadata.Description = "Successfully generated clip metadata";
                    clsClipMetadata.Status = "AVAILABLE";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "" + workOrderId;
                    clsClipMetadata.ClipStartTime = clipRequestObj.StartTime;
                    clsClipMetadata.FileSize = length;
                    return clsClipMetadata;
                }
                else
                {
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "No downloaded clips available";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  " + ex.Message);
                clsClipMetadata.Status = "FAILED";
                clsClipMetadata.WorkOrderId = workOrderId;
                clsClipMetadata.Description = "Server Error. Try agian.";
                clsClipMetadata.ClipLengthInSeconds = 0;
                clsClipMetadata.ClipName = "";
                clsClipMetadata.ClipStartTime = "";
                clsClipMetadata.FileSize = 0;
                return clsClipMetadata;
            }
        }

        public ClsClipResponse ClipDownLoad(string workOrderId)
        {
            var clsClipResponse = new ClsClipResponse();
            try
            {
                var clipData = _clipData.GetRecord(workOrderId.ToString());

                if (clipData.MakeRequestId > 0)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  ClipDownLoad :: " + clipData.ClipStatus + " " + clipData.clipOutputFilePath);
                    var fileStatus = "FAILED";

                    if (clipData.ClipStatus == 7)
                        fileStatus = "M3REQUEST";

                    if (clipData.ClipStatus == 5)
                        fileStatus = "DOWNLOADING";

                    if ((clipData.ClipStatus == 4))
                        fileStatus = "AVAILABLE";

                    if ((clipData.ClipStatus == 2))
                        fileStatus = "PENDING";//WAITING

                    if (clipData.ClipStatus == 3)
                        fileStatus = "FAILED";

                    if (fileStatus == "AVAILABLE")
                    {
                        return new ClsClipResponse
                        {
                            Status = "AVAILABLE",
                            WorkOrderId = workOrderId,
                            Description = clipData.clipOutputFilePath
                        };
                    }
                    else if (fileStatus == "DOWNLOADING")
                    {
                        return new ClsClipResponse
                        {
                            Status = "DOWNLOADING",
                            WorkOrderId = workOrderId,
                            Description = clipData.DownloadPercentage + "%"
                        };
                    }
                    else
                    {
                        return new ClsClipResponse
                        {
                            Status = fileStatus,
                            WorkOrderId = workOrderId,
                            Description = "Unable to download the clip. Clip Status - " + fileStatus
                        };
                    }
                }

                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = workOrderId,
                    Description = "Invalid workOrderId"
                };
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  " + ex.Message);
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = workOrderId,
                    Description = "Server Error. Try again."
                };
            }
        }

        public ClsClipStatus ProcessClipStatus(string workOrderId)
        {
            var clsLstClipStatus = new NetVuMediaData { archive_media_file = new List<ArchiveMediaFile>() };

            var archive_media_file = new ArchiveMediaFile();

            try
            {
                var clipData = _clipData.GetRecord(workOrderId.ToString());
                if (clipData.MakeRequestId > 0)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  Current ClipStatus :: " + clipData.ClipStatus);
                    var fileStatus = "FAILED";

                    if (clipData.ClipStatus == 7)
                        fileStatus = "M3REQUEST";

                    if (clipData.ClipStatus == 5)
                        fileStatus = "DOWNLOADING";

                    if ((clipData.ClipStatus == 4))
                        fileStatus = "AVAILABLE";

                    if ((clipData.ClipStatus == 2))
                        fileStatus = "PENDING";

                    if (clipData.ClipStatus == 3)
                        fileStatus = "FAILED";

                    if (fileStatus == "AVAILABLE")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "AVAILABLE",
                            Description = "Download is available"
                        };
                    }
                    else if (fileStatus == "PENDING")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "PENDING",
                            Description = "Download in process"
                        };
                    }
                    else if (fileStatus == "DOWNLOADING")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "DOWNLOADING",
                            Description = clipData.DownloadPercentage + "%"
                        };
                    }
                    else if(fileStatus == "M3REQUEST")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "M3REQUEST",
                            Description = "M3 request sent"
                        };
                    }
                    else if (fileStatus == "FAILED")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "FAILED",
                            Description = "Download failed"
                        };
                    }

                }
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Details not found"
                };

            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschBusiness.cs  " + ex.Message);
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Server error. Try again."
                };
            }
        }

    }
}
