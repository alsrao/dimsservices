﻿using Bosch.VideoSDK.Device;
using Bosch.VideoSDK.MediaDatabase;
using DIMS.Data;
using DIMS.Framework;
using DIMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DIMS.Business
{
    public class BoschVRMProcess
    {
        private Bosch.VideoSDK.Device.DeviceConnectorClass deviceConnector = null;
        private Bosch.VideoSDK.Device.DeviceProxy proxy;

        private BoschMediaDatabaseBrowser mediaDb;
        private int tracksCompletelyLoaded;
        private Queue<int> trackToBeLoaded = new Queue<int>();

        public Dictionary<string, string> d_TrackList = new Dictionary<string, string>();
        public List<string> d_RecordList = new List<string>();
        public List<Bosch.VideoSDK.MediaDatabase.Track> m_tracks = new List<Bosch.VideoSDK.MediaDatabase.Track>();
        private Bosch.VideoSDK.MediaDatabase.Track m_currentTrack = null;
        private Bosch.VideoSDK.MediaSession m_currentMediaSession = null;
        private Bosch.VideoSDK.MediaDatabase.PlaybackController m_currentPlaybackController = null;

        private Bosch.VideoSDK.MediaDatabase.MediaFileWriterClass m_mediaFileWriter = null;
        private bool firstTrackflag = false;

        private VideoMode m_vmRecordVideoMode = VideoMode.vmPlayback;
        private int m_nRecordFileSizeKB = 0;
        private int m_mediaFileVideoTrackID = 0;
        private int m_mediaFileAudioTrackID = 0;

        string fileName, extn, path;

        public enum ConnectionState
        {
            Disconnected,
            Connecting,
            Connected,
            Disconnecting,
            Failed
        };

        public enum VideoMode
        {
            vmLive = 0,
            vmPlayback = 1
        };

        /// <summary>
        /// Current connection state
        /// </summary>
        public ConnectionState State { get; set; }
        string reqStTime;
        string reqEnTime;

        ClipData _clipData = new ClipData();
        public static string resultId = null;
        public void Connect(string locationAddress, string startTime, string endTime)
        {
            try
            {
                reqStTime = startTime;
                reqEnTime = endTime;
                // Create the Video SDK's device connector
                deviceConnector = new DeviceConnectorClass();
                deviceConnector.ConnectResult += OnConnectResult;
                deviceConnector.DefaultTimeout = 2000;
                
                // create media database class
                mediaDb = new BoschMediaDatabaseBrowser();
                mediaDb.Tracks.CollectionChanged += Tracks_CollectionChanged;
                mediaDb.Records.CollectionChanged += Records_CollectionChanged;
                mediaDb.OnStateChanged += mediaDb_OnStateChanged;
                mediaDb.OnProgress += mediaDb_OnProgress;

                m_mediaFileWriter = new Bosch.VideoSDK.MediaDatabase.MediaFileWriterClass();

                // it is recommended to use a ProgID string in the connect call
                deviceConnector.ConnectAsync(locationAddress, "");
            }
            catch(Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  Exception :: " + ex.Message);
            }
        }

        #region VideoSDK Event handlers

        private void OnConnectResult(ConnectResultEnum connectresult, string url, DeviceProxy deviceProxy)
        {
            proxy = deviceProxy;

            switch (connectresult)
            {
                case ConnectResultEnum.creConnected:
                    break;

                case ConnectResultEnum.creInitialized:
                    // The connection was successfully established.
                    Trace.TraceInformation("Connected to {0}.", url);

                    LoadMediaDatabase();
                    break;

                default:
                    proxy.Disconnect();
                    Trace.TraceError("Connect to {0} failed. Error: {1}", url, connectresult);
                    break;
            }
        }

        #endregion


        #region MediaDb event handlers
        private void mediaDb_OnProgress(int progress)
        {
            float totalProgress = 0;
            if (mediaDb.State == MediaDatabaseBrowserState.TracksLoading || mediaDb.State == MediaDatabaseBrowserState.TracksLoaded)
            {
                // estimate loading tracks as 10%
                totalProgress = progress / 10;
            }
            else
            {
                // "divide" rest of progress bar (90%) between all tracks
                int tracksCount = mediaDb.Tracks.Count;

                if (tracksCount > 0)
                    totalProgress = 10 + 0.9f * (progress + tracksCompletelyLoaded * 100) / tracksCount;
                else
                    totalProgress = progress;
            }
            if ((int)totalProgress == 100)
            {
                foreach(Bosch.VideoSDK.MediaDatabase.Track trackTemp in m_tracks)
                {
                    if (trackTemp.TrackID == 11671)
                        m_currentTrack = trackTemp;
                }

               // m_currentTrack = m_tracks[1];
                //m_currentMediaSession = m_currentTrack.GetMediaSession(m_currentPlaybackController);
                ///statusResult = Start_MediaFile_Writer(reqStTime, reqEnTime);
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  m_currentTrack :: " + m_currentTrack);
            }
            Trace.TraceInformation("Total progress: {0}%", (int)totalProgress);
        }

        private void mediaDb_OnStateChanged(MediaDatabaseBrowserState state, string description)
        {

            if (state == MediaDatabaseBrowserState.TracksLoaded)
            {
                // prepare list of tracks for loading records
                trackToBeLoaded.Clear();
                foreach (var track in mediaDb.Tracks)
                {
                    trackToBeLoaded.Enqueue(track.TrackID);
                }

                // start load first
                LoadNextTrackRecords();
            }

            if (state == MediaDatabaseBrowserState.RecordsLoaded)
            {
                tracksCompletelyLoaded++;
                LoadNextTrackRecords();
            }
        }

        private void Tracks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                // add track nodes
                foreach (Bosch.VideoSDK.MediaDatabase.Track track in e.NewItems)
                {
                    string nodeKey = track.TrackID.ToString();
                    string displayName = string.Format("[Track #{0}] {1}", track.TrackID, track.Name);
                    if(!d_TrackList.ContainsKey(TrackToString(track)))
                    {
                        m_tracks.Add(track);
                        d_TrackList.Add(TrackToString(track), track.TrackID.ToString());

                        Helper.AddTrackstoLogFile(DateTime.Now + " [" + track.TrackID.ToString() + "] " + track.Name);
                        Helper.AddTrackstoLogFile("SourceURL :: " + track.SourceURL + " , SourceID :: " + track.SourceID + " , MediaType :: " + track.MediaType[0]);
                    }                    
                }
            }
        }
        public string TrackToString(Bosch.VideoSDK.MediaDatabase.Track track)
        {
            if (track == null)
                return "";
            else
                return track.Name.Trim().ToUpper();
        }

         public ClsLstCameraTrackAndDateTime clsLstCameraTrackAndDateTime = new ClsLstCameraTrackAndDateTime
        {
            LstCameraTrackAndDateTime = new List<CameraTrackAndDateTime>()
        };

        private void Records_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                // add track records nodes
                foreach (Bosch.VideoSDK.MediaDatabase.SearchResult record in e.NewItems)
                {
                    var cameraTrackAndDateTime = new CameraTrackAndDateTime();
                    string parentNodeKey = record.TrackID.ToString();
                    string nodeKey = string.Format("{0}\\{1}", parentNodeKey, record.StartTime.UTC.Ticks);

                    //string displayName = string.Format("[" + parentNodeKey + "]Record . {1} - {2}  {3}",                        
                    //    record.StartTime.UTC,
                    //    record.EndTime.UTC,
                    //    record.Text);
                    string displayName = "[" + parentNodeKey + "]Record . " + record.StartTime.UTC + " - " + record.EndTime.UTC + " - " + record.Text;
                    d_RecordList.Add(displayName);
                    clsLstCameraTrackAndDateTime.LstCameraTrackAndDateTime.Add(
                        new CameraTrackAndDateTime { TrackId = parentNodeKey, StartDateTime = record.StartTime.UTC, ENdDateTime = record.EndTime.UTC });

                    Helper.AddRecordstoLogFile(DateTime.Now + " " + displayName);
                }
            }
        }

        #endregion

        #region Methods

        private void LoadMediaDatabase()
        {
            if (proxy.MediaDatabase == null)
            {
                Trace.TraceWarning("No media database found.");
                return;
            }

            // start loading 
            mediaDb.SearchTracks(proxy.MediaDatabase);//,FillTime(Convert.ToDateTime(reqStTime)), FillTime(Convert.ToDateTime(reqEnTime)));
        }

        private void LoadNextTrackRecords()
        {
            // Start loading records for found tracks from queue
            if (trackToBeLoaded.Count > 0)
            {
                int trackId = trackToBeLoaded.Dequeue();
                mediaDb.SearchTrackRecords(trackId);
            }
            else
            {
                // all tracks loaded
            }
        }
        #endregion


        private string Start_MediaFile_Writer(string trackId, string stime, string etime)
        {
            try
            {
                m_currentTrack = (from x in m_tracks where x.TrackID == Convert.ToInt32(trackId) select x).FirstOrDefault();
                m_currentMediaSession = m_currentTrack.GetMediaSession(m_currentPlaybackController);

                m_mediaFileWriter.NewFileCreated += new Bosch.VideoSDK.GCALib._IMediaFileWriterEvents_NewFileCreatedEventHandler(m_mediaFileWriter_NewFileCreated);
                m_mediaFileWriter.RecordingStopped += new Bosch.VideoSDK.GCALib._IMediaFileWriterEvents_RecordingStoppedEventHandler(m_mediaFileWriter_RecordingStopped);

                path = @"C:\\Axiom\\temp\\";
                //Start_MediaFile_Writer();
                fileName = Path.Combine(path, Path.GetFileName(m_currentTrack.Name)) + ".mp4";
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  File path : " + fileName);
                // make media file recording progress bar enabled
                m_vmRecordVideoMode = VideoMode.vmPlayback;
                m_mediaFileWriter.FileFormat = Bosch.VideoSDK.MediaDatabase.MediaFileFormatEnum.mffISOMP4;


                // Maximum size of media file(s)
                m_nRecordFileSizeKB = m_mediaFileWriter.FileSizeLimitKB * 1;


                m_mediaFileWriter.RecordingStartTime = FillTime(Convert.ToDateTime(stime));// "2017-10-27T11:35:00"));
                m_mediaFileWriter.RecordingEndTime = FillTime(Convert.ToDateTime(etime));//"2017-10-27T11:45:00"));


                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  AddStreamToMediaWriter");
                AddStreamToMediaWriter(Bosch.VideoSDK.MediaTypeEnum.mteVideo, m_currentMediaSession.GetVideoStream(), "Test");
                ///Helper.AddtoLogFile("m_mediaFileWriter.StartRecording calling..");
                m_mediaFileWriter.StartRecording(fileName, "This file is created on " + DateTime.Now);
               /// Helper.AddtoLogFile("m_mediaFileWriter.StartRecording called..");
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  Exception :: " + ex.Message);
                return ex.Message;
            }
        }

        public void AddStreamToMediaWriter(
           Bosch.VideoSDK.MediaTypeEnum mediaType,
           Bosch.VideoSDK.DataStream stream,
           string url)
        {
            try
            {
                string trackName = "";
                int trackID = 1;

                if (mediaType == Bosch.VideoSDK.MediaTypeEnum.mteVideo)
                    trackID = ++m_mediaFileVideoTrackID;
                else
                    trackID = ++m_mediaFileAudioTrackID;

                trackName = "Sample Track " + trackID.ToString();

                m_mediaFileWriter.AddStream(
                    stream,
                    mediaType,
                    trackID,
                    trackName,
                    url,
                    0);


            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  Exception :: " + ex.Message);
                //Common.CheckException(ex, "Error calling AddStream");
            }
        }

        // This delegate enables asynchronous calls for updating
        // a messages ListBox control.
         delegate void RecordingStoppedThreadSafe(Bosch.VideoSDK.MediaDatabase.RecordingStoppedEnum Reason);

         public System.Windows.Forms.ListBox m_lbMessages = new System.Windows.Forms.ListBox();
          private void m_mediaFileWriter_RecordingStopped(Bosch.VideoSDK.MediaDatabase.RecordingStoppedEnum Reason)
          {
            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  m_mediaFileWriter_RecordingStopped event raised... and reason : " + Reason.ToString());
              // InvokeRequired required compares the thread ID of the
              // calling thread to the thread ID of the creating thread.
              // If these threads are different, it returns true.
              if (m_lbMessages.InvokeRequired)
              {
                  RecordingStoppedThreadSafe wrapper = new RecordingStoppedThreadSafe(m_mediaFileWriter_RecordingStopped);
                  System.Windows.Forms.Control c = new System.Windows.Forms.Control();
                  c.Invoke(wrapper, new object[] { Reason });
              }
              else
              {
                  string msg = "Media file recording stopped, Reason: " + Reason.ToString();

                  ///LogMessage("MainForm", "m_mediaFileWriter_RecordingStopped", msg);

                  m_lbMessages.Items.Add(DateTime.Now.ToString() + " " + msg);
                  if (m_lbMessages.Items.Count > 100)
                      m_lbMessages.Items.RemoveAt(0);
                  m_lbMessages.SelectedIndex = m_lbMessages.Items.Count - 1;

                  // Reset the menu item's text and state and also the media file track IDs.
                  /// m_startRecordingMenuItem.Text = "Start MediaFileWriter Recording";
                  ///m_startRecordingMenuItem.Enabled = false;

                  // unadvise from the IMediaFileWriterEvents events
                  ///m_mediaFileWriter.Progress -= new Bosch.VideoSDK.GCALib._IMediaFileWriterEvents_ProgressEventHandler(OnMFWProgress);
                  // hide media file recording progress bar
                  ///m_panelRecordStatusBar.Visible = false;
                  ///ArrangeControls();

                  m_mediaFileVideoTrackID = 0;
                  m_mediaFileAudioTrackID = 0;
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  File recording stopped...");
                  Helper.AddtoLogFile("=============================================================================");
              }
          }

          // This delegate enables asynchronous calls for updating
          // a messages ListBox control.
          delegate void UpdateMessagesThreadSafe(string strFilename);

          private void m_mediaFileWriter_NewFileCreated(string strFilename)
          {
              Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  m_mediaFileWriter_NewFileCreated event raised...");
              // InvokeRequired required compares the thread ID of the
              // calling thread to the thread ID of the creating thread.
              // If these threads are different, it returns true.
              if (m_lbMessages.InvokeRequired)
              {
                  System.Windows.Forms.Control c = new System.Windows.Forms.Control();
                  UpdateMessagesThreadSafe wrapper = new UpdateMessagesThreadSafe(m_mediaFileWriter_NewFileCreated);
                  c.Invoke(wrapper, new object[] { strFilename });
              }
              else
              {
                  string msg = "New media file has been created: " + strFilename.ToString();

                  ///LogMessage("MainForm", "m_mediaFileWriter_NewFileCreated", msg);

                  m_lbMessages.Items.Add(DateTime.Now.ToString() + " " + msg);
                  if (m_lbMessages.Items.Count > 100)
                      m_lbMessages.Items.RemoveAt(0);
                  m_lbMessages.SelectedIndex = m_lbMessages.Items.Count - 1;
                  Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  File creation started...");
              }
          }

        /*public string ValidateRecordDateTime(string startTime, string endTime)
        {
            Helper.AddtoLogFile("StartTime :: " + startTime + " and endTime :: " + endTime+" recordObj :: "+ d_RecordList.Count);
            return "";
        }*/

        public string ValidateTrack(string cameraName, string encoderName)
        {
            try
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  cameraName :: " + cameraName + " and encoderName :: " + encoderName + " trackObj :: " + d_TrackList.Count);
                //string requestAttrEC = cameraName.Trim() + "/" + encoderName.Trim();
                //string vrmTrackAttr = string.Empty;
                var trackId = (from x in d_TrackList where x.Key.Contains(cameraName) select x.Value).FirstOrDefault();
                if (trackId != null)
                    return trackId;
                else
                {
                    trackId = (from x in d_TrackList where x.Key.Contains(encoderName) select x.Value).FirstOrDefault();
                    if (trackId != null)
                        return trackId;
                    else
                        return null;
                }
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  TrackID :: " + trackId);
                return trackId;
            }
            catch(Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  Exception :: " + ex.Message);
                return null;
            }
        }

        public CameraTrackAndDateTime ValidateRecordDateTime(string trackId, DateTime clipstartDate, DateTime clipEndDate)
        {
            try
            {
                //Please verify this 
                Time64 st = FillTime(clipstartDate);
                Time64 et = FillTime(clipEndDate);

                Thread.Sleep(3000);
                var data = (from x in clsLstCameraTrackAndDateTime.LstCameraTrackAndDateTime
                            where x.TrackId == trackId && st.UTC >= x.StartDateTime && st.UTC <= x.ENdDateTime
                            && et.UTC > x.StartDateTime && et.UTC <= x.ENdDateTime
                            select x).FirstOrDefault();
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  Record Date and time with given time range. StartDateTime :: " + data.StartDateTime + " ENdDateTime :: " + data.ENdDateTime);
                return data;
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in BoschVRMProcess.cs  Exception :: " + ex.Message);
                return null;
            }
        }
        private Bosch.VideoSDK.MediaDatabase.Time64 FillTime(DateTime date)
        {
            Bosch.VideoSDK.MediaDatabase.Time64 t64 = new Bosch.VideoSDK.MediaDatabase.Time64();
            DateTime dt = new DateTime(
                date.Year,
                date.Month,
                date.Day,
                date.Hour,
                date.Minute,
                date.Second);

            t64.UTC = dt;

            return t64;
        }
    }
}
