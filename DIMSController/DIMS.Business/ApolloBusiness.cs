﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml.Serialization;
using DIMS.Model;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using DIMS.Framework;
using DIMS.Data;
using System.IO;
using Insight.Shared.MetaContainer;

namespace DIMS.Business
{
    public class ApolloBusiness
    {
        public enum Status { M3REQUEST = 7, CANCELLED = 6, DOWNLOADING = 5, AVAILABLE = 4, PENDING = 2, FAILED = 3 };

        ClipData _clipData = new ClipData();
        public ClsClipResponse ClipRequest(ClsClipRequest clipRequest)
        {

            try
            {
                string workOrdId = clipRequest.WorkOrderId.ToString().Trim();
                if (!String.IsNullOrEmpty(workOrdId))
                {
                    var workOrderId = _clipData.GetRecord(workOrdId);
                    if (workOrderId == null)
                    {
                        if (clipRequest.M3 == true)
                        {
                            using (var httpClient = new HttpClient())
                            {
                                httpClient.Timeout = TimeSpan.FromSeconds(60);
                                httpClient.DefaultRequestHeaders.Accept.Clear();
                                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                var reqdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                var m3DvrInterface = new M3_Dvr_Interface()
                                {
                                    record_Status = "R",
                                    Transaction_type = "NIN",
                                    Incident_Type_code = "DVR",
                                    Problem_code = "DVR8",
                                    Equip_code = clipRequest.RailCarNumber,
                                    Division = clipRequest.Division,
                                    Incident_desc = clipRequest.IncidentDescription,
                                    Rail_Bus_Ind = "R",
                                    Dims_Id = clipRequest.WorkOrderId,
                                    Request_Created_Date = reqdate
                                };

                                var json = JsonConvert.SerializeObject(m3DvrInterface);
                                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ApolloBusiness.cs  M3 Request Json:: " + json);
                                var postData = new StringContent(json, Encoding.UTF8, "application/json");
                                var response = httpClient.PostAsync("http://apiservices.metro.net/M3DvrInterfacedata", postData).Result;
                                try
                                {
                                    response.EnsureSuccessStatusCode();
                                    if (response.IsSuccessStatusCode)
                                    {
                                        var responseData = _clipData.AMSClipRequestInsert(clipRequest, 0, 2, Convert.ToInt32(Status.M3REQUEST), clipRequest.M3, clipRequest.RailCarNumber);
                                        if (responseData != 0)
                                            return new ClsClipResponse
                                            {
                                                Status = "SUCCESS",
                                                WorkOrderId = workOrdId,
                                                Description = "Clip request placed successfully"
                                            };
                                        else
                                            return new ClsClipResponse
                                            {
                                                Status = "FAILED",
                                                WorkOrderId = null,
                                                Description = "Clip request can't be placed"
                                            };
                                    }
                                }
                                catch (Exception ex)
                                {
                                    return new ClsClipResponse
                                    {
                                        Status = "FAILED",
                                        WorkOrderId = null,
                                        Description = "Unable to make M3 clip request"
                                    };

                                }
                            }
                        }
                        else
                        {
                            using (var httpClient = new HttpClient())
                            {
                                httpClient.BaseAddress = new Uri(Helper.GetSetting("ApolloUrl"));
                                httpClient.Timeout = TimeSpan.FromSeconds(60);
                                httpClient.DefaultRequestHeaders.Accept.Clear();
                                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                clipRequest.Password = "";
                               // clipRequest.SegLen = "NONE";
                               // clipRequest.SiteNameList = new List<string> { clipRequest.SiteName };

                                var json = JsonConvert.SerializeObject(clipRequest);

                                var postData = new StringContent(json, Encoding.UTF8, "application/json");
                                var response = httpClient.PostAsync("ClipRequest/", postData).Result;

                                if (!response.IsSuccessStatusCode)
                                    return new ClsClipResponse
                                    {
                                        Status = "FAILED",
                                        WorkOrderId = null,
                                        Description = "Unable to send clip request to server"
                                    };

                                var clsClipResponse =
                                    JsonConvert.DeserializeObject<List<ClipResponse>>(response.Content.ReadAsStringAsync().Result);
                                var responseData = _clipData.ClipRequestInsert(clipRequest, clsClipResponse[0].ClipRequestID, 1, Convert.ToInt32(Status.PENDING), false, null);

                                return new ClsClipResponse
                                {
                                    Status = "SUCCESS",
                                    WorkOrderId = workOrdId,
                                    Description = "Clip request placed successfully"
                                };
                               
                            }
                        }
                    }
                    else
                    {
                        return new ClsClipResponse
                        {
                            Status = "FAILED",
                            WorkOrderId = null,
                            Description = "WorkOrderId already exist."
                        };
                    }
                }
                else
                {
                    return new ClsClipResponse
                    {
                        Status = "FAILED",
                        WorkOrderId = null,
                        Description = "Invalid WorkOrderId."
                    };
                }
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = null,
                    Description = "Invalid Input."
                };
            }
            catch (Exception ex)
            {
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = null,
                    Description = "Currently Service Unavailable"
                };
            }
        }


        public ClsClipStatus ClipStatus(string workOrderId)
        {
            try
            {
                var status = ProcessClipStatus(workOrderId);
                return status;
            }
            catch (Exception ex)
            {
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Server error. Try with valid WorkOrderId."
                };

            }
        }


        public ClsClipMetadata ClipMetadata(string workOrderId)
        {
            var clsClipMetadata = new ClsClipMetadata();

            clsClipMetadata.Status = "FAILED";
            clsClipMetadata.WorkOrderId = workOrderId;
            clsClipMetadata.Description = "Service Unavailable";
            return clsClipMetadata;

            //try
            //{
            //    var clipId = _clipData.GetClipId(workOrderId);
            //    if (clipId == 0)
            //    {
            //        clsClipMetadata.Status = "FAILED";
            //        clsClipMetadata.WorkOrderId = workOrderId;
            //        clsClipMetadata.Description = "Invalid workOrderId";
            //        return clsClipMetadata;
            //    }

            //    using (var httpClient = new HttpClient())
            //    {
            //        httpClient.BaseAddress = new Uri(Helper.GetSetting("ApolloUrl"));
            //        httpClient.Timeout = TimeSpan.FromSeconds(60);
            //        httpClient.DefaultRequestHeaders.Accept.Clear();
            //        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //        var response = httpClient.GetAsync("ClipMetadata/" + clipId).Result;


            //        if (!response.IsSuccessStatusCode)
            //        {
            //            clsClipMetadata.Status = "FAILED";
            //            clsClipMetadata.WorkOrderId = workOrderId;
            //            clsClipMetadata.Description = "Clip download is in process";
            //            return clsClipMetadata;
            //        }

            //        var clsClipMetadataInternal =
            //            JsonConvert.DeserializeObject<ClsClipMetadataInternal>(response.Content.ReadAsStringAsync().Result);

            //        clsClipMetadata.Status = "SUCCESS";
            //        clsClipMetadata.WorkOrderId = workOrderId;
            //        clsClipMetadata.ClipLengthInSeconds = clsClipMetadataInternal.ClipLengthInSeconds;
            //        clsClipMetadata.ClipName = clsClipMetadataInternal.ClipName;
            //        clsClipMetadata.ClipStartTime = clsClipMetadataInternal.ClipStartTime;
            //        clsClipMetadata.FileSize = clsClipMetadataInternal.FileSize;
            //        return clsClipMetadata;

            //    }
            //}
            //catch (Exception ex)
            //{
            //    clsClipMetadata.Status = "FAILED";
            //    clsClipMetadata.WorkOrderId = workOrderId;
            //    clsClipMetadata.Description = "Server Error. Try agian.";
            //    return clsClipMetadata;

            //}
        }

        public ClsClipResponse ClipDownLoad(string workOrderId)
        {
            var clsClipResponse = new ClsClipResponse();

            var clipCurrentStatus = ProcessClipStatus(workOrderId);

            return new ClsClipResponse
            {
                Status = "FAILED",
                WorkOrderId = workOrderId,
                Description = "Clip Status " +clipCurrentStatus.Description
            };

            string clipPath = @"\\MTAAXIOMS01\Users\kumarp\ClipDatabase\\" + workOrderId + "\\";
            var clipId = _clipData.GetClipId(workOrderId);
            if (clipId == 0)
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = workOrderId,
                    Description = "Invalid workOrderId"
                };
            try
            {
                //var clipCurrentStatus = ProcessClipStatus(workOrderId);

                if (clipCurrentStatus.Status == "AVAILABLE")
                {
                    using (var httpClient = new HttpClient())
                    {
                        httpClient.BaseAddress = new Uri(Helper.GetSetting("ApolloUrl"));
                        httpClient.Timeout = TimeSpan.FromSeconds(60);
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        var tcs = new System.Threading.CancellationTokenSource();
                        httpClient.DefaultRequestHeaders.Accept.Add(
                            new MediaTypeWithQualityHeaderValue("application/octet-stream"));


                        var response = httpClient.GetAsync("Clip/" + clipId, HttpCompletionOption.ResponseHeadersRead, tcs.Token).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            
                            response.Content.ReadAsStringAsync().ContinueWith(
                                (readTask) =>
                                {
                                    if (!Directory.Exists(clipPath))
                                    {
                                        Directory.CreateDirectory(clipPath);
                                    }
                                    var systemPath = clipPath + Convert.ToString(clipId) + ".exe";//Create and replace IMD file in palce of 1.exe
                                    System.IO.File.WriteAllBytes(systemPath, Convert.FromBase64String(readTask.Result));
                                }, tcs.Token);

                            var filepathId = _clipData.UpdateFilePath(workOrderId,clipPath);

#region Imd File creation
                            FileInfo myMovieFile = new FileInfo(@clipPath + Convert.ToString(1) + ".exe");
                            DateTime fileStart = new DateTime(2017, 3, 21, 7, 20, 0, DateTimeKind.Utc);
                            TimeSpan fileDuration = TimeSpan.FromSeconds(6);
                            // (etc.)

                            ImdFile imd = new ImdFile();
                            Multimedium mm = imd.Multimedia.AddNew();
                            //Insight.Shared.Crypto.PlainHash raja123445;

                            // Here we set up some information about the piece of media we're ingesting.
                            mm.Owners.AddNew("Owner");
                            mm.Title = "Speeding ticket on the raod";
                            mm.Description = "Pulled over a 2017 car for speeding.";
                            mm.BrandName = "Apollo"; // Your brand name should go here instead.
                            mm.Area = "Downtown";
                            mm.Source = "Vehicle 1234";
                            mm.StartAt = fileStart;
                            mm.Duration = fileDuration;
                            mm.AssetMedium = Medium.Video;
                            mm.StartMethod = StartMethod.Known;
                            mm.SystemRelation = SystemRelation.DirectTransfer;

                            // This describes the file itself.
                            MultimediumFile mf = mm.Files.AddNew(myMovieFile, true);
                            mf.Importance = FileImportance.Primary;
                            mf.StartTime = fileStart;
                            mf.Duration = fileDuration;
                            mf.FileSize = myMovieFile.Length;
                            mf.FileMedium = Medium.Video;
                            mf.OriginalName = "Sample_512kb.mp4";
                            mf.CurrentName = "Sample_512kb.mp4";

                            // This adds a time zone to the file, so that local times can be determined.
                            mm.TimeZones.AddCurrentTimeZone(TimeSource.DirectHandler, TimeRelation.Same, fileStart);

                            Metadata md = mm.Metadata;

                            // This adds a bookmark 26 seconds into the video.
                            // The Event Type provides a common way for users to search for videos with certain events in them.
                            md.Bookmarks.AddNew(fileStart.AddSeconds(11), null, "Speeding", "Began chasing a speeding car with license plate 1234567.");

                            // This adds a GPS coordinate to the video.
                            md.GpsList.AddNew(fileStart.AddSeconds(5), 34.077154, -117.552316, 35, null);

                            // This adds a radar reading to the video.
                            md.RadarList.AddNew(fileStart.AddSeconds(5), 35, 90, null);

                            // This adds a siren trigger. Notie that its default state is open,
                            // and fires when closed, because the siren only starts blaring once the
                            // electrical circuit is closed.
                            md.TriggerInfo.Triggers.AddNew(1, "Siren", TriggerState.Open, TriggerState.Open, true, FireState.Closed);

                            // This adds a shotgun trigger. Notice that its default state is closed,
                            // and fires when opened, because the shotgun is generally attached to the car,
                            // meaning the circuit is closed until the gun is removed, which then opens
                            // the circuit.
                            md.TriggerInfo.Triggers.AddNew(2, "Shotgun", TriggerState.Closed, TriggerState.Closed, true, FireState.Opened);

                            // This will turn the siren trigger "on" 30 seconds into the video.
                            md.TriggerInfo.Changes.AddNew(fileStart.AddSeconds(5), 1, TriggerState.Closed);

                            // This will turn the siren trigger back "off" 50 seconds into the video.
                            md.TriggerInfo.Changes.AddNew(fileStart.AddSeconds(6), 1, TriggerState.Open);

                            // This adds a few tags to the file.
                            md.Tags.AddNew("Violation", "Speeding");
                            md.Tags.AddNew("Case ID", "1234");

                            // This adds some audits.
                            mm.TextAudits.AddNew(fileStart, "Owner", "Pre-recording started", null);
                            mm.TextAudits.AddNew(fileStart.AddSeconds(7), "raja", "Siren triggered a recording", null);


                            // This creates the actual imd file that can be put into the ingestion
                            // directory along with the video.
                            //imd.SaveToFile(new FileInfo(Path.ChangeExtension(myMovieFile.FullName, ".imd")), "Producer goes here", "Producer code goes here");
                            imd.SaveToFile(new FileInfo(Path.ChangeExtension(myMovieFile.FullName, ".imd")), "Axiom xCell, Inc.", "7C31149EFA4260A73DE8F9A955FEF438ACF14ACA4528A4423338C070F9213F4D77DC4CFD754B19BA4854AA3D087F796944135AAE149E1212BBDB3612504064C2");
#endregion
                  
                       
                            //
                            return new ClsClipResponse
                            {
                                Status = "SUCCESS",
                                WorkOrderId = workOrderId,
                                Description = "C:\\\\AxonClips_DataBase\\"+ workOrderId
                            };
                        }

                        return new ClsClipResponse
                        {
                            Status = "FAILED",
                            WorkOrderId = workOrderId,
                            Description = "Unable to download"
                        };

                    }
                }
                else
                {
                    return new ClsClipResponse
                    {
                        Status = clipCurrentStatus.Status,
                        WorkOrderId = workOrderId,
                        Description = "Unable to download the clip. Clip Status - " + clipCurrentStatus.Description
                    };
                }
            }
            catch (Exception ex)
            {
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = workOrderId,
                    Description = "Server error. Try again."
                };
            }
        }

        public ClsClipStatus ProcessClipStatus(string workOrderId)
        {
            var clsLstClipStatus = new ClsLstClipStatus { LstClipStatus = new List<ClipStatus>() };
            var clipStatus = new ClsClipStatus();
            try
            {
                var clipData = _clipData.GetRecord(workOrderId.ToString());
                if (clipData.MakeRequestId > 0)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ApolloBusiness.cs  Current ClipStatus :: " + clipData.ClipStatus);
                    var fileStatus = "FAILED";

                    if (clipData.ClipStatus == 7)
                        fileStatus = "M3REQUEST";

                    if (clipData.ClipStatus == 5)
                        fileStatus = "DOWNLOADING";

                    if ((clipData.ClipStatus == 4))
                        fileStatus = "AVAILABLE";

                    if ((clipData.ClipStatus == 2))
                        fileStatus = "PENDING";

                    if (clipData.ClipStatus == 3)
                        fileStatus = "FAILED";

                    if (fileStatus == "AVAILABLE")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "AVAILABLE",
                            Description = "Download is available"
                        };
                    }
                    else if (fileStatus == "PENDING")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "PENDING",
                            Description = "Download in process"
                        };
                    }
                    else if (fileStatus == "DOWNLOADING")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "DOWNLOADING",
                            Description = clipData.DownloadPercentage + "%"
                        };
                    }
                    else if (fileStatus == "M3REQUEST")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "M3REQUEST",
                            Description = "M3 request sent"
                        };
                    }
                    else if (fileStatus == "FAILED")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "FAILED",
                            Description = "Download failed"
                        };
                    }
                }
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Details not found"
                };

            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ApolloBusiness.cs  " + ex.Message);
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Server error. Try again."
                };
            }

            
            //using (var httpClient = new HttpClient())
            //{
            //    httpClient.BaseAddress = new Uri(Helper.GetSetting("ApolloUrl"));
            //    httpClient.Timeout = TimeSpan.FromSeconds(60);
            //    httpClient.DefaultRequestHeaders.Accept.Clear();
            //    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    var response = httpClient.GetAsync("ClipStatus").Result;


            //    if (!response.IsSuccessStatusCode)
            //    {
            //        return new ClsClipStatus
            //        {
            //            WorkOrderId = workOrderId,
            //            Status = "FAILED",
            //            Description = "Unable to get the status"
            //        };
            //    }

            //    var apolloClipId = _clipData.GetClipId(workOrderId);
            //    var clsClipStatus =
            //        JsonConvert.DeserializeObject<List<ClipStatus>>(response.Content.ReadAsStringAsync().Result);

            //    clsClipStatus = clsClipStatus.Where(x => x.ClipRequestID == apolloClipId).ToList();

            //    if (clsClipStatus.Count > 0)
            //    {
            //        clipStatus.WorkOrderId = workOrderId;
            //        clipStatus.Status = clsClipStatus[0].Status.ToUpper();                    
            //        if (clipStatus.Status == "AVAILABLE") clipStatus.Description = "Download available";
            //        else if (clipStatus.Status == "PENDING") clipStatus.Description = "Download in process";
            //        else clipStatus.Description = "Download failed";
            //        return clipStatus;

            //    }

            //    return new ClsClipStatus
            //    {
            //        WorkOrderId = workOrderId,
            //        Status = "FAILED",
            //        Description = "Details not found"
            //    };
            //}
        }
    }
}
