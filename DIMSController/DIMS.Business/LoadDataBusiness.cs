﻿using DIMS.Data;
using DIMS.Framework;
using DIMS.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DIMS.Business
{
    public class LoadDataBusiness
    {
        ClipData _clipData = new ClipData();

        public void LoadServers()
        {
            try
            {
                var clsLstServers = new NetVuAssets { assetServer = new List<AssetServer>() };
                var assetServer = new AssetServer();

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(Helper.GetSetting("AMSUrl"));
                    httpClient.Timeout = TimeSpan.FromSeconds(60);
                    httpClient.DefaultRequestHeaders.Accept.Clear();

                    var locationsResponse = httpClient.GetAsync("manage-assets.php?func=list-server").Result;

                    var locationResponseData = XDocument.Parse(locationsResponse.Content.ReadAsStringAsync().Result);

                    if (locationResponseData.Root != null)
                    {
                        var result =
                           locationResponseData.Root.Descendants("asset_server")
                               .Select(row => new
                               {
                                   Server_id = row.Element("server_id"),
                                   Server_name = row.Element("server_name"),
                                   Server_address = row.Element("server_address"),
                                   Server_port = row.Element("server_port"),
                                   Server_dynamicIp = row.Element("server_dynamic_ip"),
                                   System_Id = row.Element("system_id"),
                                   Sys_Id = row.Element("_sys_id"),
                               }).ToList();

                        for (int i = 0; i < result.Count; i++)
                        {
                            long serverid = Convert.ToInt64(result[i].Server_id.Value);
                            string servername = result[i].Server_name.Value;
                            string serverAddress = result[i].Server_address.Value;
                            long serverPort = Convert.ToInt64(result[i].Server_port.Value);
                            string serverDynamicip = result[i].Server_dynamicIp.Value;
                            string serverSystemid = result[i].System_Id.Value;
                            string serverSysId = result[i].Sys_Id.Value;

                            var updatedData = _clipData.UpdateServerRecord(serverid, servername, serverAddress, serverPort, serverDynamicip, serverSystemid, serverSysId);
                            if (updatedData == 0)
                            {
                                var data = _clipData.InsServers(serverid, servername, serverAddress, serverPort, serverDynamicip, serverSystemid, serverSysId);
                            }                            
                        }
                        
                    }
                }
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in LoadDataBusiness.cs  All servers updated/inserted in LoadServers() in LoadDataBusiness ");
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in LoadDataBusiness.cs  Exception occured in LoadServers() in LoadDataBusiness :: " + ex.Message);
            }


        }

        public void LoadMediaLocations()
        {
            try
            {
                var clsLstLocationStatus = new NetVuArchiveLocations { archiveLocation = new List<ArchiveLocation>() };
                var archive_Location = new ArchiveLocation();

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(Helper.GetSetting("AMSUrl"));
                    httpClient.Timeout = TimeSpan.FromSeconds(60);
                    httpClient.DefaultRequestHeaders.Accept.Clear();

                    var locationsResponse = httpClient.GetAsync("manage-archive-locations.php?func=list").Result;

                    var locationResponseData = XDocument.Parse(locationsResponse.Content.ReadAsStringAsync().Result);

                    if (locationResponseData.Root != null)
                    {
                        var result =
                           locationResponseData.Root.Descendants("archiveLocation")
                               .Select(row => new
                               {
                                   Media_location_id = row.Element("media_location_id"),
                                   Media_location_sys_id = row.Element("media_location_sys_id"),
                                   Distributed_info = row.Element("distributed_info"),
                               }).ToList();


                        for (int i = 0; i < result.Count; i++)
                        {
                            long locationId = Convert.ToInt64(result[i].Media_location_id.Value);
                            string locationSystemId = result[i].Media_location_sys_id.Value.ToString();
                            string distributed_config_name;
                            if (result[i].Distributed_info.Element("distributed_config_name").IsEmpty)
                                distributed_config_name = null;
                            else
                                distributed_config_name = result[i].Distributed_info.Element("distributed_config_name").Value;
                            string distributed_config_address;
                            if (result[i].Distributed_info.Element("distributed_config_address").IsEmpty)
                                distributed_config_address = null;
                            else
                                distributed_config_address = result[i].Distributed_info.Element("distributed_config_address").Value;

                            var updatedData = _clipData.UpdateLocationData(locationId, locationSystemId, distributed_config_name, distributed_config_address);
                            if (updatedData == 0)
                            {
                                var res = _clipData.InsGetDivisionIp(locationId, locationSystemId, distributed_config_name, distributed_config_address);
                            }
                        }


                    }
                }
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in LoadDataBusiness.cs  All MediaLocations updated/inserted in LoadMediaLocations() in LoadDataBusiness ");
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in LoadDataBusiness.cs  Exception occured in LoadMediaLocations() in LoadDataBusiness :: " + ex.Message);
            }


        }


        public void LoadSystems()
        {
            try
            {
                var clsLstSystems = new NetVuAssets { assetSystem = new List<AssetSystem>() };
                var assetSystem = new AssetSystem();

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(Helper.GetSetting("AMSUrl"));
                    httpClient.Timeout = TimeSpan.FromSeconds(60);
                    httpClient.DefaultRequestHeaders.Accept.Clear();

                    var locationsResponse = httpClient.GetAsync("manage-assets.php?func=list-system").Result;

                    var locationResponseData = XDocument.Parse(locationsResponse.Content.ReadAsStringAsync().Result);

                    if (locationResponseData.Root != null)
                    {
                        var result =
                           locationResponseData.Root.Descendants("asset_system")
                               .Select(row => new
                               {
                                   System_id = row.Element("system_id"),
                                   System_name = row.Element("system_name"),
                                   Sys_Id = row.Element("_sys_id"),
                               }).ToList();

                        for (int i = 0; i < result.Count; i++)
                        {
                            string systemid = result[i].System_id.Value;
                            string systemname = result[i].System_name.Value;
                            string systemSysId = result[i].Sys_Id.Value;
                            var updatedData = _clipData.UpdateSystemRecord(systemid, systemname, systemSysId);
                            if (updatedData == 0)
                            {
                                var data = _clipData.InsSystems(systemid, systemname, systemSysId);
                            }
                        }

                        
                    }
                }
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in LoadDataBusiness.cs  All Systems updated/inserted in LoadSystems() in LoadDataBusiness ");
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in LoadDataBusiness.cs  Exception occured in LoadSystems() in LoadDataBusiness :: " + ex.Message);
            }


        }
    }
}
