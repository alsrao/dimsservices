﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml.Serialization;
using DIMS.Model;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using DIMS.Framework;
using DIMS.Data;
using System.IO;
using System.Xml.Linq;
using System.Xml;

namespace DIMS.Business
{
    public class AMSBusiness
    {

        ClipData _clipData = new ClipData();

        public enum Status { M3REQUEST = 7, CANCELLED = 6, DOWNLOADING = 5, AVAILABLE = 4, PENDING = 2, FAILED = 3 };

        public ClsClipResponse ClipRequest(ClsClipRequest clipRequest)
        {
            try
            {
                string workOrdId = clipRequest.WorkOrderId.ToString().Trim();
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  workOrderId:: " + workOrdId);
                if (!String.IsNullOrEmpty(workOrdId))
                {
                    var workOrderId = _clipData.GetRecord(workOrdId);
                    if (workOrderId == null)
                    {
                        string systemId, locationId, locationSystemId;

                        if (clipRequest.M3 == true)
                        {
                            using (var httpClient = new HttpClient())
                            {
                                httpClient.Timeout = TimeSpan.FromSeconds(60);
                                httpClient.DefaultRequestHeaders.Accept.Clear();
                                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                var reqdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                var m3DvrInterface = new M3_Dvr_Interface()
                                {
                                    record_Status = "R",
                                    Transaction_type = "NIN",
                                    Incident_Type_code = "DVR",
                                    Problem_code = "DVR6",
                                    Equip_code = clipRequest.BusNumber,
                                    Division = clipRequest.Division,
                                    Incident_desc = clipRequest.IncidentDescription,
                                    Rail_Bus_Ind = "B",
                                    Dims_Id = clipRequest.WorkOrderId,
                                    Request_Created_Date = reqdate
                                };

                                var json = JsonConvert.SerializeObject(m3DvrInterface);
                                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  M3 Request Json:: " + json);
                                var postData = new StringContent(json, Encoding.UTF8, "application/json");
                                var response = httpClient.PostAsync("http://apiservices.metro.net/M3DvrInterfacedata", postData).Result;
                                try
                                {
                                    response.EnsureSuccessStatusCode();
                                    if (response.IsSuccessStatusCode)
                                    {
                                        var responseData = _clipData.AMSClipRequestInsert(clipRequest, 0, 2, Convert.ToInt32(Status.M3REQUEST), clipRequest.M3, null);
                                        if (responseData != 0)
                                            return new ClsClipResponse
                                            {
                                                Status = "SUCCESS",
                                                WorkOrderId = workOrdId,
                                                Description = "Clip request placed successfully"
                                            };
                                        else
                                            return new ClsClipResponse
                                            {
                                                Status = "FAILED",
                                                WorkOrderId = null,
                                                Description = "Clip request can't be placed"
                                            };
                                    }
                                }
                                catch (Exception ex)
                                {
                                    return new ClsClipResponse
                                    {
                                        Status = "FAILED",
                                        WorkOrderId = null,
                                        Description = "Unable to make M3 clip request"
                                    };
                                    
                                }
                            }
                        }
                        else
                        {
                            string serverName = "LACMTA-" + clipRequest.BusNumber;
                            var serverDetails = _clipData.GetServerRecord(serverName);
                            Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  serverDetails:: " + serverDetails + " serverName - Bus Name:: " + serverName);
                            if (serverDetails == null)
                            {
                                return new ClsClipResponse
                                {
                                    Status = "FAILED",
                                    WorkOrderId = null,
                                    Description = "Invalid BusNumber. Please use valid BusNumbers and request again."
                                };
                            }
                            
                            using (var httpClient = new HttpClient())
                            {
                                httpClient.BaseAddress = new Uri(Helper.GetSetting("AMSUrl"));
                                httpClient.Timeout = TimeSpan.FromSeconds(60);
                                httpClient.DefaultRequestHeaders.Accept.Clear();

                                var serversResponse = httpClient.GetAsync("manage-assets.php?func=details-server&server_id=" + serverDetails.serverId).Result;

                                var serversResponseData = XDocument.Parse(serversResponse.Content.ReadAsStringAsync().Result);
                                if (serversResponseData.Root != null)
                                {
                                    var result =
                                       serversResponseData.Root.Descendants("asset_server")
                                           .Select(row => new
                                           {
                                               Server_id = row.Element("server_id"),
                                               Server_Name = row.Element("server_name"),
                                               System_id = row.Element("system_id"),
                                               Location_id = row.Element("media_location_id"),
                                               Location_Sys_Id = row.Element("media_location_sys_id"),
                                           }).FirstOrDefault();

                                    locationId = result.Location_id.Value.ToString();
                                    locationSystemId = result.Location_Sys_Id.Value;

                                    clipRequest.Location_Id = Convert.ToInt32(locationId);
                                    clipRequest.System_Id = locationSystemId;
                                    clipRequest.Server_Id = Convert.ToInt32(serverDetails.serverId);

                                    string division = "Division " + clipRequest.Division.ToUpper();

                                    clipRequest.Username = Helper.GetSetting("AMSUsername");// "axiomxcell";
                                    clipRequest.Password = Helper.GetSetting("AMSPassword"); // "Metro!123";


                                    DateTime pastDate = new DateTime(1970, 1, 1);
                                    TimeSpan startTimeSpan = Convert.ToDateTime(clipRequest.StartTime) - pastDate;
                                    TimeSpan endTimeSpan = Convert.ToDateTime(clipRequest.EndTime) - pastDate;

                                    int startTimeStamp = (int)startTimeSpan.TotalSeconds;
                                    int endTimeStamp = (int)endTimeSpan.TotalSeconds;

                                    StringBuilder stringBuilder = new StringBuilder();
                                    stringBuilder.Append("server_id=" + clipRequest.Server_Id + "&");
                                    stringBuilder.Append("location_id=" + clipRequest.Location_Id + "&");
                                    stringBuilder.Append("start_time=" + startTimeStamp + "&");
                                    stringBuilder.Append("end_time=" + endTimeStamp + "&");
                                    stringBuilder.Append("_sys_id=" + clipRequest.System_Id + "&");
                                    stringBuilder.Append("user_username=" + clipRequest.Username + "&");
                                    stringBuilder.Append("user_password=" + clipRequest.Password);

                                    var response = httpClient.GetAsync("manage-archive-tasks.php?func=import-auto&" + stringBuilder).Result;
                                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  AMS clip request:: " + stringBuilder.ToString());
                                    Helper.AddtoLogFile("Response : " + response + " response result :" + response.Content.ReadAsStringAsync().Result + " and status : " + response.StatusCode);

                                    if (!response.IsSuccessStatusCode)
                                        return new ClsClipResponse
                                        {
                                            Status = "FAILED",
                                            WorkOrderId = null,
                                            Description = "Unable to send clip request to server"
                                        };

                                    String resXmlUnfor = response.Content.ReadAsStringAsync().Result;
                                    resXmlUnfor.Trim();
                                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  AMS clip request response:: " + resXmlUnfor);

                                    if (!resXmlUnfor.Contains("<error>"))
                                    {
                                        if (resXmlUnfor.Contains("<?xml version=\"1.0\"?>"))
                                        {
                                            resXmlUnfor = resXmlUnfor.Replace("<?xml version=\"1.0\"?>\n\n", "");
                                            resXmlUnfor.Trim();
                                        }
                                        if (resXmlUnfor.Contains("<NetVuArchiveTasks>\n"))
                                        {
                                            resXmlUnfor = resXmlUnfor.Replace("<NetVuArchiveTasks>\n", "");
                                            resXmlUnfor.Trim();
                                        }
                                        if (resXmlUnfor.Contains("</NetVuArchiveTasks>"))
                                        {
                                            resXmlUnfor = resXmlUnfor.Replace("\n</NetVuArchiveTasks>", "");
                                            resXmlUnfor.Trim();
                                        }
                                        if (resXmlUnfor.Contains("<result>"))
                                        {
                                            resXmlUnfor = resXmlUnfor.Replace("<result>\n", "<NetVuArchiveTasks>\n<result>\n");
                                            resXmlUnfor.Trim();
                                        }
                                        if (resXmlUnfor.Contains("</result>"))
                                        {
                                            resXmlUnfor = resXmlUnfor.Replace("</result>", "</result>\n");
                                            resXmlUnfor.Trim();
                                        }

                                        XmlSerializer ser = new XmlSerializer(typeof(NetVuArchiveTasks));
                                        var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(resXmlUnfor));
                                        NetVuArchiveTasks amsReponseObj = (NetVuArchiveTasks)ser.Deserialize(memoryStream);

                                        if (amsReponseObj.result.resultMessage != null)
                                        {
                                            System.Diagnostics.Debug.WriteLine(amsReponseObj + " " + amsReponseObj.result.resultMessage);
                                            string resultMessage = amsReponseObj.result.resultMessage.ToUpper();
                                            int resultDetails = Convert.ToInt32(amsReponseObj.result.resultDetails);

                                            if ((resultMessage == "AUTOMATIC DOWNLOAD VIDEO IMPORT SCHEDULED") && resultDetails > 0)
                                            {
                                                System.Diagnostics.Debug.WriteLine("Tags : " + clipRequest.Tags.ToString());
                                                string tagInfoJson = null;
                                                try
                                                {
                                                    if (clipRequest.Tags != null)
                                                    {
                                                        // New code starts
                                                        var tags = clipRequest.Tags;
                                                        var tagList = tags.Information;
                                                        var toBeCheckedList = new List<string> { "AREA", "BRANDNAME", "SOURCE" };
                                                        var list3 = tagList.Where(x => toBeCheckedList.Contains(x.Name.ToUpper())).Select(n => n.Name.ToLower()).ToList();
                                                        if (!(list3.Count == 3))
                                                        {
                                                            var missedTags = string.Join(",", toBeCheckedList.Where(x => !list3.Contains(x.ToLower())).ToList());

                                                            return new ClsClipResponse
                                                            {
                                                                Status = "FAILED",
                                                                WorkOrderId = null,
                                                                Description = "Required Tags are missing, " + missedTags
                                                            };

                                                        }
                                                        // New Code End

                                                        var stringwriter = new System.IO.StringWriter();
                                                        var serializer = new XmlSerializer(clipRequest.Tags.GetType());
                                                        serializer.Serialize(stringwriter, clipRequest.Tags);
                                                        string tagsInfo = stringwriter.ToString();
                                                        System.Diagnostics.Debug.WriteLine("tagsInfo : " + tagsInfo);

                                                        tagsInfo.Trim();
                                                        if (tagsInfo.Contains("<?xml version=\"1.0\" encoding=\"utf-16\"?>"))
                                                        {
                                                            tagsInfo = tagsInfo.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n", "");
                                                            tagsInfo.Trim();
                                                        }
                                                        if (tagsInfo.Contains("<Tags xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\""))
                                                        {
                                                            tagsInfo = tagsInfo.Replace("<Tags xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n ", "<Tags>");
                                                            tagsInfo.Trim();
                                                        }
                                                        tagInfoJson = tagsInfo;
                                                    }
                                                }
                                                catch (Exception e)
                                                {
                                                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  " + e.Message);
                                                }
                                                clipRequest.TagsJson = tagInfoJson;
                                                if (String.IsNullOrEmpty(clipRequest.IncidentDescription) || String.IsNullOrEmpty(clipRequest.IncidentDescription.Trim()))
                                                    clipRequest.IncidentDescription = "No description available";
                                                var responseData = _clipData.AMSClipRequestInsert(clipRequest, resultDetails, 2, Convert.ToInt32(Status.PENDING), false, null);
                                                if (responseData != 0)
                                                    return new ClsClipResponse
                                                    {
                                                        Status = "SUCCESS",
                                                        WorkOrderId = workOrdId,
                                                        Description = "Clip request placed successfully"
                                                    };
                                                else
                                                    return new ClsClipResponse
                                                    {
                                                        Status = "FAILED",
                                                        WorkOrderId = null,
                                                        Description = "Clip request can't be placed."
                                                    };
                                            }
                                        }
                                        return new ClsClipResponse
                                        {
                                            Status = "FAILED",
                                            WorkOrderId = null,
                                            Description = "Unable to send clip request to server with provided input values. Try agian."
                                        };
                                    }
                                    else if (resXmlUnfor.Contains("<error>"))
                                    {
                                        XmlSerializer ser = new XmlSerializer(typeof(NetVuArchiveTasks));
                                        var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(resXmlUnfor));
                                        NetVuArchiveTasks amsReponseObj = (NetVuArchiveTasks)ser.Deserialize(memoryStream);
                                        Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  Response Error:: " + amsReponseObj.error.errorMessage);
                                        return new ClsClipResponse
                                        {
                                            Status = "FAILED",
                                            WorkOrderId = null,
                                            Description = "Currently request cannot be made with provided input"
                                        };
                                    }
                                    else
                                    {
                                        return new ClsClipResponse
                                        {
                                            Status = "FAILED",
                                            WorkOrderId = null,
                                            Description = "Unable to send clip request to server with provided input values. Try agian."
                                        };
                                    }
                                }
                                else
                                {
                                    return new ClsClipResponse
                                    {
                                        Status = "FAILED",
                                        WorkOrderId = null,
                                        Description = "Unable to process. Try agian."
                                    };
                                }
                            }
                        }                       
                        
                    }
                    else
                    {
                        return new ClsClipResponse
                        {
                            Status = "FAILED",
                            WorkOrderId = null,
                            Description = "WorkOrderId already exist."
                        };
                    }
                }
                else
                {
                    return new ClsClipResponse
                    {
                        Status = "FAILED",
                        WorkOrderId = null,
                        Description = "Invalid WorkOrderId."
                    };
                }
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = null,
                    Description = "Invalid Input."
                };
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  " + ex.Message);
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = null,
                    Description = "Invalid Input"
                };
            }
        }

        public ClsClipStatus ClipStatus(string workOrderId)
        {
            try
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  Clip Status :: " + workOrderId);
               var status = ProcessClipStatus(workOrderId);
                return status;
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  " + ex.Message);
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Server error. Try with valid WorkOrderId."
                };
            }
        }

        public ClsClipMetadata ClipMetadata(string workOrderId)
        {
            var clsClipMetadata = new ClsClipMetadata();

            try
            {
                var clipId = _clipData.GetClipId(workOrderId);
                if (clipId == 0)
                {
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "Invalid workOrderId";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }

                var clipRequestObj = _clipData.GetRecord(workOrderId);
                string clipPath = clipRequestObj.clipOutputFilePath;

                if (!Directory.Exists(clipPath))
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  GetClipMeta clipPath:: " + clipPath);
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "No downloaded clips available";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }

                var length = Directory.GetFiles(clipPath, "*", SearchOption.AllDirectories).Sum(t => (new FileInfo(t).Length));

                if (length > 0)
                {
                    clsClipMetadata.Description = "Successfully generated clip metadata";
                    clsClipMetadata.Status = "AVAILABLE";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "" + workOrderId;
                    clsClipMetadata.ClipStartTime = clipRequestObj.StartTime;
                    clsClipMetadata.FileSize = length;
                    return clsClipMetadata;
                }
                else
                {
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "No downloaded clips available";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  " + ex.Message);
                clsClipMetadata.Status = "FAILED";
                clsClipMetadata.WorkOrderId = workOrderId;
                clsClipMetadata.Description = "Server Error. Try agian.";
                clsClipMetadata.ClipLengthInSeconds = 0;
                clsClipMetadata.ClipName = "";
                clsClipMetadata.ClipStartTime = "";
                clsClipMetadata.FileSize = 0;
                return clsClipMetadata;
            }
        }
        
        public ClsClipResponse ClipDownLoad(string workOrderId)
        {
            var clsClipResponse = new ClsClipResponse();
            try
            {
                var clipData = _clipData.GetRecord(workOrderId.ToString());

                if (clipData.MakeRequestId > 0)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  ClipDownLoad :: " + clipData.ClipStatus+" "+ clipData.clipOutputFilePath);
                    var fileStatus = "FAILED";

                    if (clipData.ClipStatus == 7)
                        fileStatus = "M3REQUEST";

                    if (clipData.ClipStatus == 5)
                        fileStatus = "DOWNLOADING";

                    if ((clipData.ClipStatus == 4))
                        fileStatus = "AVAILABLE";

                    if ((clipData.ClipStatus == 2))
                        fileStatus = "PENDING";//WAITING

                    if (clipData.ClipStatus == 3)
                        fileStatus = "FAILED";

                    if (fileStatus == "AVAILABLE")
                    {
                        return new ClsClipResponse
                        {
                            Status = "AVAILABLE",
                            WorkOrderId = workOrderId,
                            Description = clipData.clipOutputFilePath
                        };
                    }
                    else if (fileStatus == "DOWNLOADING")
                    {
                        return new ClsClipResponse
                        {
                            WorkOrderId = workOrderId,
                            Status = "DOWNLOADING",
                            Description = clipData.DownloadPercentage + "%"
                        };
                    }
                    else
                    {
                        return new ClsClipResponse
                        {
                            Status = fileStatus,
                            WorkOrderId = workOrderId,
                            Description = "Unable to download the clip. Clip Status - " + fileStatus
                        };
                    }
                }

                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = workOrderId,
                    Description = "Invalid workOrderId"
                };
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  " + ex.Message);
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = workOrderId,
                    Description = "Server Error. Try again."
                };
            }
        }

        public ClsClipStatus ProcessClipStatus(string workOrderId)
        {
            var clsLstClipStatus = new NetVuMediaData { archive_media_file = new List<ArchiveMediaFile>() };

            var archive_media_file = new ArchiveMediaFile();

            try
            {
                var clipData = _clipData.GetRecord(workOrderId.ToString());
                if (clipData.MakeRequestId > 0)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  Current ClipStatus :: " + clipData.ClipStatus);
                    var fileStatus = "FAILED";

                    if (clipData.ClipStatus == 7)
                        fileStatus = "M3REQUEST";

                    if (clipData.ClipStatus == 5)
                        fileStatus = "DOWNLOADING";

                    if ((clipData.ClipStatus == 4))
                        fileStatus = "AVAILABLE";

                    if ((clipData.ClipStatus == 2))
                        fileStatus = "PENDING";

                    if (clipData.ClipStatus == 3)
                        fileStatus = "FAILED";

                    if (fileStatus == "AVAILABLE")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "AVAILABLE",
                            Description = "Download is available"
                        };
                    }
                    else if (fileStatus == "DOWNLOADING")
                    {
                        return new ClsClipStatus
                        {
                            Status = "DOWNLOADING",
                            WorkOrderId = workOrderId,
                            Description = clipData.DownloadPercentage + "%"
                        };
                    }
                    else if (fileStatus == "M3REQUEST")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "M3REQUEST",
                            Description = "M3 request sent"
                        };
                    }
                    else if (fileStatus == "PENDING")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "PENDING",
                            Description = "Download in process"
                        };
                    }
                    else if (fileStatus == "FAILED")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "FAILED",
                            Description = "Download failed"
                        };
                    }

                }
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Details not found"
                };

            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  " + ex.Message);
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Server error. Try again."
                };
            }
        }
    }
}