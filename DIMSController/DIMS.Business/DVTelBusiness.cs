﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml.Serialization;
using DIMS.Model;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using DIMS.Framework;
using DIMS.Data;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using DIMS.DVTelLibrary;
namespace DIMS.Business
{
    public class DVTelBusiness
    {
        ClipData _clipData = new ClipData();
        public enum Status { M3REQUEST = 7, CANCELLED = 6, DOWNLOADING = 5, AVAILABLE = 4, PENDING = 2, FAILED = 3 };
        public ClsClipResponse ClipRequest(ClsClipRequest clipRequest)
        {
            try
            {
                string workOrdId = clipRequest.WorkOrderId.ToString().Trim();
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  workOrderId:: " + workOrdId);

                if (!String.IsNullOrEmpty(workOrdId))
                {
                    if(String.IsNullOrEmpty(clipRequest.IncidentDescription) || String.IsNullOrEmpty(clipRequest.IncidentDescription.Trim()))
                        clipRequest.IncidentDescription = "No description available";

                    var workOrderId = _clipData.GetRecord(workOrdId);
                    if (workOrderId == null)
                    {
                        String dvTelCamera = null;

                        var facilitiesFixedData = Helper.SerializeToXml(clipRequest.FacilitiesFixed);

                        XmlSerializer xs = new XmlSerializer(typeof(FACILITIESFIXED));
                        FACILITIESFIXED facilitiesFixedobj = (FACILITIESFIXED)xs.Deserialize(new StringReader(facilitiesFixedData));

                        if(facilitiesFixedobj != null && facilitiesFixedobj.Camera != null)
                            dvTelCamera = facilitiesFixedobj.Camera.Trim();

                        string tagInfoJson = null;
                        try
                          {
                                if (clipRequest.Tags != null)
                                {
                                    var tags = clipRequest.Tags;
                                    var tagList = tags.Information;
                                    var toBeCheckedList = new List<string> { "AREA", "BRANDNAME", "SOURCE" };
                                    var list3 = tagList.Where(x => toBeCheckedList.Contains(x.Name.ToUpper())).Select(n => n.Name.ToLower()).ToList();
                                    if (!(list3.Count == 3))
                                    {
                                        var missedTags = string.Join(",", toBeCheckedList.Where(x => !list3.Contains(x.ToLower())).ToList());
                                        return new ClsClipResponse
                                        {
                                            Status = "FAILED",
                                            WorkOrderId = null,
                                            Description = "Required Tags are missing, " + missedTags
                                        };
                                    }
                                    var stringwriter = new System.IO.StringWriter();
                                    var serializer = new XmlSerializer(clipRequest.Tags.GetType());
                                    serializer.Serialize(stringwriter, clipRequest.Tags);
                                    string tagsInfo = stringwriter.ToString();
                                    System.Diagnostics.Debug.WriteLine("tagsInfo : " + tagsInfo);
                                    tagsInfo.Trim();
                                    if (tagsInfo.Contains("<?xml version=\"1.0\" encoding=\"utf-16\"?>"))
                                    {
                                        tagsInfo = tagsInfo.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n", "");
                                        tagsInfo.Trim();
                                    }
                                    if (tagsInfo.Contains("<Tags xmlns:xsd=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema\""))
                                    {
                                        tagsInfo = tagsInfo.Replace("<Tags xmlns:xsd=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema\">\r\n ", "<Tags>");
                                        tagsInfo.Trim();
                                    }
                                    tagInfoJson = tagsInfo;
                                }
                            }
                            catch (Exception e)
                            {
                                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  " + e.Message);
                            }
                            clipRequest.TagsJson = tagInfoJson;

                        DVTelProcess dvtProcessObj = new DVTelProcess(GetHostName, GetUserName, GetPassword, dvTelCamera);
                        List <object> listDVTelObj = dvtProcessObj.ConnectAndValidate(clipRequest.StartTime, clipRequest.EndTime);
                            System.Threading.Thread.Sleep(5000);
                            DateTime sdt = Convert.ToDateTime(clipRequest.StartTime);
                            DateTime edt = Convert.ToDateTime(clipRequest.EndTime);

                            foreach (object objValue in listDVTelObj)
                            {
                                Type t = objValue.GetType();
                                if (t.Equals(typeof(Exception)))
                                {
                                    Exception ex = (Exception)objValue;
                                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  " + ex.Message);
                                    return new ClsClipResponse
                                    {
                                        Status = "FAILED",
                                        WorkOrderId = null,
                                        Description = "Server error. Try again."
                                    };
                                }
                                if (t.Equals(typeof(bool)))
                                {
                                    bool flag = (bool)objValue;
                                    if (!flag)
                                    {
                                        return new ClsClipResponse
                                        {
                                            Status = "FAILED",
                                            WorkOrderId = null,
                                            Description = "Invalid Camera. Please use valid Camera and request again."
                                        };
                                    }
                                    if (flag)
                                    {
                                        clipRequest.BusNumber = dvTelCamera;
                                        var responseData = _clipData.DVTelClipRequestInsert(clipRequest, 0, 4, Convert.ToInt32(Status.PENDING), false, null);
                                        if (responseData != 0)
                                        {
                                            return new ClsClipResponse
                                            {
                                                Status = "SUCCESS",
                                                WorkOrderId = workOrdId,
                                                Description = "Clip request placed successfully"
                                            };
                                        }
                                        else
                                        {
                                            return new ClsClipResponse
                                            {
                                                Status = "FAILED",
                                                WorkOrderId = null,
                                                Description = "Clip request can't be placed."
                                            };
                                        }
                                    }
                                    //return new ClsClipResponse
                                    //{
                                    //    Status = "FAILED",
                                    //    WorkOrderId = null,
                                    //    Description = "Currently no clip found with provided time"
                                    //};
                                }
                            }
                    }
                    else
                    {
                        return new ClsClipResponse
                        {
                            Status = "FAILED",
                            WorkOrderId = null,
                            Description = "WorkOrderId already exist."
                        };
                    }
                }
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = null,
                    Description = "Invalid WorkOrderId."
                };
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  " + ex.Message);
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = null,
                    Description = "Invalid Input"
                };
            }
        }

        public ClsClipStatus ClipStatus(string workOrderId)
        {
            try
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  Clip Status :: " + workOrderId);
                var status = ProcessClipStatus(workOrderId);
                return status;
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  " + ex.Message);
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Server error. Try with valid WorkOrderId."
                };
            }
        }

        public ClsClipMetadata ClipMetadata(string workOrderId)
        {
            var clsClipMetadata = new ClsClipMetadata();

            try
            {
                var clipId = _clipData.GetClipId(workOrderId);
                if (clipId == 0)
                {
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "Invalid workOrderId";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }

                var clipRequestObj = _clipData.GetRecord(workOrderId);
                string clipPath = clipRequestObj.clipOutputFilePath;

                if (!Directory.Exists(clipPath))
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  GetClipMeta clipPath:: " + clipPath);
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "No downloaded clips available";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }

                var length = Directory.GetFiles(clipPath, "*", SearchOption.AllDirectories).Sum(t => (new FileInfo(t).Length));

                if (length > 0)
                {
                    clsClipMetadata.Description = "Successfully generated clip metadata";
                    clsClipMetadata.Status = "AVAILABLE";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "" + workOrderId;
                    clsClipMetadata.ClipStartTime = clipRequestObj.StartTime;
                    clsClipMetadata.FileSize = length;
                    return clsClipMetadata;
                }
                else
                {
                    clsClipMetadata.Status = "FAILED";
                    clsClipMetadata.WorkOrderId = workOrderId;
                    clsClipMetadata.Description = "No downloaded clips available";
                    clsClipMetadata.ClipLengthInSeconds = 0;
                    clsClipMetadata.ClipName = "";
                    clsClipMetadata.ClipStartTime = "";
                    clsClipMetadata.FileSize = 0;
                    return clsClipMetadata;
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  " + ex.Message);
                clsClipMetadata.Status = "FAILED";
                clsClipMetadata.WorkOrderId = workOrderId;
                clsClipMetadata.Description = "Server Error. Try agian.";
                clsClipMetadata.ClipLengthInSeconds = 0;
                clsClipMetadata.ClipName = "";
                clsClipMetadata.ClipStartTime = "";
                clsClipMetadata.FileSize = 0;
                return clsClipMetadata;
            }
        }

        public ClsClipResponse ClipDownLoad(string workOrderId)
        {
            var clsClipResponse = new ClsClipResponse();
            try
            {
                var clipData = _clipData.GetRecord(workOrderId.ToString());

                if (clipData.MakeRequestId > 0)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  ClipDownLoad :: " + clipData.ClipStatus + " " + clipData.clipOutputFilePath);
                    var fileStatus = "FAILED";

                    if (clipData.ClipStatus == 7)
                        fileStatus = "M3REQUEST";

                    if (clipData.ClipStatus == 5)
                        fileStatus = "DOWNLOADING";

                    if ((clipData.ClipStatus == 4))
                        fileStatus = "AVAILABLE";

                    if ((clipData.ClipStatus == 2))
                        fileStatus = "PENDING";//WAITING

                    if (clipData.ClipStatus == 3)
                        fileStatus = "FAILED";

                    if (fileStatus == "AVAILABLE")
                    {
                        return new ClsClipResponse
                        {
                            Status = "AVAILABLE",
                            WorkOrderId = workOrderId,
                            Description = clipData.clipOutputFilePath
                        };
                    }
                    else if (fileStatus == "DOWNLOADING")
                    {
                        return new ClsClipResponse
                        {
                            WorkOrderId = workOrderId,
                            Status = "DOWNLOADING",
                            Description = clipData.DownloadPercentage + "%"
                        };
                    }
                    else
                    {
                        return new ClsClipResponse
                        {
                            Status = fileStatus,
                            WorkOrderId = workOrderId,
                            Description = "Unable to download the clip. Clip Status - " + fileStatus
                        };
                    }
                }

                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = workOrderId,
                    Description = "Invalid workOrderId"
                };
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in AMSBusiness.cs  " + ex.Message);
                return new ClsClipResponse
                {
                    Status = "FAILED",
                    WorkOrderId = workOrderId,
                    Description = "Server Error. Try again."
                };
            }
        }

        public ClsClipStatus ProcessClipStatus(string workOrderId)
        {
            var clsLstClipStatus = new NetVuMediaData { archive_media_file = new List<ArchiveMediaFile>() };

            var archive_media_file = new ArchiveMediaFile();

            try
            {
                var clipData = _clipData.GetRecord(workOrderId.ToString());
                if (clipData.MakeRequestId > 0)
                {
                    Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  Current ClipStatus :: " + clipData.ClipStatus);
                    var fileStatus = "FAILED";

                    if (clipData.ClipStatus == 7)
                        fileStatus = "M3REQUEST";

                    if (clipData.ClipStatus == 5)
                        fileStatus = "DOWNLOADING";

                    if ((clipData.ClipStatus == 4))
                        fileStatus = "AVAILABLE";

                    if ((clipData.ClipStatus == 2))
                        fileStatus = "PENDING";

                    if (clipData.ClipStatus == 3)
                        fileStatus = "FAILED";

                    if (fileStatus == "AVAILABLE")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "AVAILABLE",
                            Description = "Download is available"
                        };
                    }
                    else if (fileStatus == "DOWNLOADING")
                    {
                        return new ClsClipStatus
                        {
                            Status = "DOWNLOADING",
                            WorkOrderId = workOrderId,
                            Description = clipData.DownloadPercentage + "%"
                        };
                    }
                    else if (fileStatus == "M3REQUEST")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "M3REQUEST",
                            Description = "M3 request sent"
                        };
                    }
                    else if (fileStatus == "PENDING")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "PENDING",
                            Description = "Download in process"
                        };
                    }
                    else if (fileStatus == "FAILED")
                    {
                        return new ClsClipStatus
                        {
                            WorkOrderId = workOrderId,
                            Status = "FAILED",
                            Description = "Download failed"
                        };
                    }

                }
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Details not found"
                };

            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in DVTelBusiness.cs  " + ex.Message);
                return new ClsClipStatus
                {
                    WorkOrderId = workOrderId,
                    Status = "FAILED",
                    Description = "Server error. Try again."
                };
            }
        }
        public string GetHostName
        {
            get
            {
                string hostName = Helper.GetSetting("DVTelHostName");
                return hostName;
            }
        }
        public string GetUserName
        {
            get
            {
                string userName = Helper.GetSetting("DVTelUserName");
                return userName;
            }
        }
        public string GetPassword
        {
            get
            { 
                string password = Helper.GetSetting("DVTelPassword");
                return password;
            }
        }

    }
}
