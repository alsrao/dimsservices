﻿using System;
using System.Linq;
using DIMS.Model;
using DIMS.Framework;

namespace DIMS.Data
{
    public class ClipData
    {
        public decimal? ClipRequestInsert(ClsClipRequest clipRequest, int clipRequestId, int providerId, int clipStatus, bool m3, string railNumber)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpInsMakeClip(clipRequest.WorkOrderId,clipRequest.Type, clipRequestId, clipRequest.Location_Id, null,
                                  clipRequest.StartTime
                                  , clipRequest.EndTime, null, null, null,
                                  "1",
                                  clipRequest.PreTime, clipRequest.PostTime,
                                  false,
                                  null, clipRequest.Server_Id, clipRequest.System_Id, providerId, clipStatus, null,clipRequest.BusNumber, clipRequest.Division,clipRequest.TagsJson, null, 0, clipRequest.IncidentDescription, m3, railNumber).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Clip Request can'be placed. Exception :: " + ex.Message);
                return 0;
            }
        }


        public decimal? AMSClipRequestInsert(ClsClipRequest clipRequest, int clipRequestId, int providerId, int clipStatus, bool m3, string railNumber)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpInsMakeClip(clipRequest.WorkOrderId, clipRequest.Type, clipRequestId, clipRequest.Location_Id, null,
                              clipRequest.StartTime
                              , clipRequest.EndTime, null, null, null,
                              "1",
                              clipRequest.PreTime, clipRequest.PostTime,
                              false,
                              null, clipRequest.Server_Id, clipRequest.System_Id, providerId, clipStatus, null, clipRequest.BusNumber, clipRequest.Division, clipRequest.TagsJson, null, 0, clipRequest.IncidentDescription, m3, railNumber).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Clip Request can'be placed. Exception :: " + ex.Message);
                return 0;
            }
        }

        public decimal? BoschClipRequestInsert(ClsClipRequest clipRequest, int clipRequestId, int providerId, int clipStatus,bool m3, string railNumber)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpInsMakeClip(clipRequest.WorkOrderId, clipRequest.Type, clipRequestId, clipRequest.Location_Id, null,
                              clipRequest.StartTime
                              , clipRequest.EndTime, null, null, null,
                              "1",
                              clipRequest.PreTime, clipRequest.PostTime,
                              false,
                              null, clipRequest.Server_Id, clipRequest.System_Id, providerId, clipStatus, null, clipRequest.BusNumber, clipRequest.Division, clipRequest.TagsJson, null, 0, clipRequest.IncidentDescription,m3,railNumber).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Clip Request can'be placed. Exception :: " + ex.Message);
                return 0;
            }
        }
        public decimal? DVTelClipRequestInsert(ClsClipRequest clipRequest, int clipRequestId, int providerId, int clipStatus, bool m3, string railNumber)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpInsMakeClip(clipRequest.WorkOrderId, clipRequest.Type, clipRequestId, clipRequest.Location_Id, null,
                              clipRequest.StartTime
                              , clipRequest.EndTime, null, null, null,
                              "1",
                              clipRequest.PreTime, clipRequest.PostTime,
                              false,
                              null, clipRequest.Server_Id, clipRequest.System_Id, providerId, clipStatus, null, clipRequest.BusNumber, clipRequest.Division, clipRequest.TagsJson, null, 0, clipRequest.IncidentDescription, m3, railNumber).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Clip Request can'be placed. Exception :: " + ex.Message);
                return 0;
            }
        }
        public int GetClipId(string workOrderId)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpGetClipRequestId(workOrderId).FirstOrDefault() ?? 0;
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in GetClipId() in ClipData.cs");
                return 0;
            }
        }

        public long GetLocationId(string workOrderId)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpGetLocationId(workOrderId).FirstOrDefault() ?? 0;
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in GetLocationId() in ClipData.cs");
                return 0;
            }
        }

        public int UpdateFilePath(string workOrderId, string clipFilePath)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpUpdateFilePath(workOrderId, clipFilePath);
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in UpdateFilePath() in ClipData.cs");
                return 0;
            }
        }

        public SpGetRecord_Result GetRecord(string workOrderId)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpGetRecord(workOrderId).FirstOrDefault();
                }
            }
            catch (Exception ex)//error here
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in GetRecord() in ClipData.cs");
                return null;
            }
        }
        public string InsGetDivisionIp(long locationId, string systemId, string divisionName, string divisionIp)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpInsLocations(locationId, systemId, divisionName, divisionIp).ToString();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in InsGetDivisionIp() in ClipData.cs");
                return null;
            }
        }

        public SpGetLocationsRecord_Result GetLocationsRecord(string divisionIp)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpGetLocationsRecord(divisionIp).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in GetLocationsRecord() in ClipData.cs");
                return null;
            }
        }

        public int UpdateLocationData(long locationId, string systemId, string divisionName, string divisionIp)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpUpdateLocationData(locationId, systemId, divisionName, divisionIp);
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in UpdateLocationData() in ClipData.cs");
                return 0;
            }
        }

        public string InsServers(long serverId,string serverName,string serverAddress,long serverPort,string serverDynamicIp,string systemId,string sysId)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpInsSevers(serverId, serverName, serverAddress, serverPort, serverDynamicIp,systemId,sysId).ToString();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in InsServers() in ClipData.cs");
                return null;
            }
        }

        public SpGetServerRecord_Result GetServerRecord(string serverName)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpGetServerRecord(serverName).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in GetServerRecord() in ClipData.cs");
                return null;
            }
        }

        public string InsSystems(string systemId, string systemName, string sysId)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpInsSystems(systemId, systemName, sysId).ToString();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in InsSystems() in ClipData.cs");
                return null;
            }
        }

        public int UpdateServerRecord(long serverId, string serverName, string serverAddress, long serverPort, string serverDynamicIp, string systemId, string sysId)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpUpdateServerRecord(serverId, serverName, serverAddress, serverPort, serverDynamicIp, systemId, sysId);
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in UpdateServerRecord() in ClipData.cs");
                return 0;
            }
        }

        public int UpdateSystemRecord(string systemId, string systemName, string sysId)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpUpdateSystemRecord(systemId, systemName, sysId);
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in UpdateSystemRecord() in ClipData.cs");
                return 0;
            }
        }

        public SpExistClip_Result CheckExistRecord(string busNumber, string division, string startTime, string endTime)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpExistClip(busNumber, division, startTime, endTime).FirstOrDefault();
                }
            }
            catch (Exception ex)//error here
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in CheckExistRecord() in ClipData.cs");
                return null;
            }
        }

        public SpGetBoschServer_Result GetBoschServerRecord(string serverName)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    return db.SpGetBoschServer(serverName).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Helper.AddtoLogFile(DateTime.Now + " :: " + (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber() + " in ClipData.cs  Exception :: " + ex.Message + "  occured in GetBoschServerRecord() in ClipData.cs");
                return null;
            }
        }


    }
}
